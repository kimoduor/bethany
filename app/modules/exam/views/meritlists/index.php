<style>
    .error{
        border: solid 1px #ff6633;
    }
    .hidec{
        display: none;
    }
</style>
<?php
$model = new Marks();
?>
<span id="yearx" class="hidec"></span><span id="classx"  class="hidec"></span>
<span id="streamx"  class="hidec"></span><span  class="hidec" id="subjectx"></span><span  class="hidec" id="termx"></span>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-list-ul"></i> Select to Display the Merit Lists</div>
                <div class="tools">
                    <a href="javascript:;" class="expand" id="widget1">
                    </a>
                </div>
            </div>
            <div class="portlet-body form" id="body2">
                <form action="#/entermarks" id="form-username" class="form-horizontal form-bordered" method="post">
                    <div class="form-group">
                        <div class="col-md-1">Year *</div>
                        <div class="col-md-2">
                            <?php echo CHtml::activeDropDownList($model, 'year_id', Years::model()->getListData("year_id", "year", true, '', array(), "year_id ASC"), array('class' => 'select2 form-control', 'id' => 'year_id', 'required' => true)); ?>
                        </div>
                        <div class="col-md-1">Class *</div>
                        <div class="col-md-2">

                            <?php echo CHtml::activeDropDownList($model, 'class_id', Classes::model()->getListData("class_id", "class", true, '', array(), "class_id ASC"), array('class' => 'select2 form-control', 'id' => 'class_id', 'required' => true)); ?>
                        </div>

                        <div class="col-md-1">Exam *</div>
                        <div class="col-md-2">
                            <?php echo CHtml::activeDropDownList($model, 'examinationtype_id', Examinationtypes::model()->getListData("examinationtype_id", "examinationtype", true, '', array(), "examinationtype_id ASC"), array('class' => 'select2 form-control', 'id' => 'examinationtype_id')); ?>
                        </div>

                        <div class="col-md-1">Term *</div>
                        <div class="col-md-2">
                            <?php echo CHtml::activeDropDownList($model, 'term_id', Terms::model()->getListData("term_id", "term", true, '', array(), "term_id ASC"), array('class' => 'select2 form-control', 'id' => 'term_id', 'required' => true)); ?>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button name="sub1" id="searchbtn" type="submit" class="btn green"><i class="fa fa-search"></i> Search</button>
                                <button name="sub2" id="refreshbtn" type="submit" class="btn btn-default"><i class="fa fa-refresh"></i> Refresh</button>
                            </div>
                        </div>
                    </div>
                </form>
                <div style="margin-left:20px">
                    <table id="jqGrid"></table>
                    <div id="jqGridPager"></div>
                </div> 
                <br />
                <!--                Subject Position Analysis-->
                <div style="margin-left:20px">
                    <table id="jqGrid2"></table>
                    <div id="jqGridPager2"></div>
                </div> 
            </div>
        </div>

    </div>
</div>
<?php
$allsubjects = Subjects::model()->findAll();
$myarray = array();
foreach ($allsubjects as $subject) {
    $myarray[] = array('label' => "$subject->subject", "search" => false, "name" => "marks$subject->subject_id", "editable" => true, "width" => "245");
    $myarray[] = array('label' => "Grade", "search" => false, "name" => "grade$subject->subject_id", "editable" => "false", "width" => "95");
}
$subjectstring = json_encode($myarray);
$subject1 = str_replace("[", "", $subjectstring);
$subjects = str_replace("]", "", $subject1);
?>

<?php
//---------------------------------------------now start for subject analysis--------------------------------

$subjectarray = array();
foreach ($allsubjects as $subject) {
    $subjectarray[] = array('label' => "$subject->subject", "search" => false, "name" => "subject$subject->subject_id", "editable" => true, "width" => "245");
}
$subjectstringanalysis = json_encode($subjectarray);
$subject2 = str_replace("[", "", $subjectstringanalysis);
$subjectsanalysis = str_replace("]", "", $subject2);
//---------------------------------------------------------finished subject analysis---------------------------
?>




<script>
    $.jgrid.defaults.width = 1200;
    $.jgrid.defaults.responsive = true;
    $.jgrid.defaults.styleUI = 'Bootstrap';</script>

<script type="text/javascript">
    $(document).ready(function() {
        $.jgrid.styleUI.Bootstrap.base.rowTable = "table table-bordered table-striped";
        var lastSel;
        var year;
        var class_id;
        var stream_id;
        var examinationtype_id;
        var term_id;
        var template = "<div style='margin-left:15px;'>";
        template += "<div> First Name: </div><div>{fname} </div>";
        template += "<hr style='width:100%;'/>";
        template += "<div> {sData} {cData}  </div></div>";
        $("#jqGrid").jqGrid({
            url: 'data/meritlists',
            datatype: "local",
            rownumbers: true,
            toppager: true, cloneToTop: true,
            colModel: [
                {label: 'Admission No',
                    name: 'adm',
                    editable: false,
                    width: 245,
                },
                {label: 'Name',
                    name: 'name',
                    editable: false,
                    width: 305,
                },
                {label: 'Stream',
                    name: 'stream_id',
                    editable: true,
                    edittype: 'select',
                    formatter: 'select',
                    editoptions: {value:<?php echo Details::streamjson(); ?>},
                    stype: 'select',
                    searchoptions: {value:<?php echo Details::streamjson(); ?>},
                    width: 245,
                },
<?php
echo $subjects;
?>,
                {label: 'Total Marks',
                    name: 'tmarks',
                    search: false,
                    formatter: "number",
                    editable: false,
                    width: 245,
                },
                {label: 'Stream Pos',
                    name: 'stream_position',
                    editable: false,
                    search: false,
                    width: 100,
                },
                {label: 'Class Pos',
                    name: 'class_position',
                    editable: false,
                    search: false,
                    width: 100,
                },
            ],
            sortname: 'tmarks',
            sortorder: 'desc',
            loadonce: true,
            viewrecords: true,
            width: 1000,
            height: 400,
            rowNum: 20000000000,
            beforeSubmitCell: function(rowid, celname, value, iRow, iCol) {
                year = $('#yearx').text();
                class_id = $('#classx').text();
                stream_id = $('#streamx').text();
                examinationtype_id = $('#examinationtypex').text();
                term_id = $('#termx').text();
                return {year_id: year, pupil_id: rowid, class_id: class_id, stream_id: stream_id, examinationtype_id: examinationtype_id,
                    value: value, examination_type: celname, term_id: term_id};
            },
            afterSubmitCell: function(data, id, cellname, value, iRow, iCol) {
                $("#jqGrid").setGridParam({datatype: 'json'}).trigger('reloadGrid');
            },
            pager: "#jqGridPager", navOptions: {reloadGridOptions: {fromServer: true}},
        }).setGridParam({datatype: 'json'}).trigger('reloadGrid');
        $('#jqGrid').navGrid('#jqGridPager',
                {edit: false, add: false, del: false, search: true, refresh: true, view: true, position: "left", cloneToTop: true},
        {// reload
            reloadAfterSubmit: true
        },
        // options for the Update Dailog
        {
            errorTextFormat: function(data) {
                return 'Error: ' + data.responseText
            },
        });
        $('#jqGrid').jqGrid('filterToolbar', {
            // JSON stringify all data from search, including search toolbar operators
            stringResult: true,
            // instuct the grid toolbar to show the search options
            searchOperators: false,
        });
        //  -------------------more for subject Analysis ---------------------------------------------------------
        $("#jqGrid2").jqGrid({
            url: 'data/subjectanalysis',
            datatype: "local",
            rownumbers: false,
            toppager: true, cloneToTop: true,
            colModel: [
                {label: 'Analysis',
                    name: 'analysis',
                    search: false,
                    editable: false,
                    width: 245,
                },
<?php
echo $subjectsanalysis;
?>,
                {label: 'Total Marks',
                    name: 'ttmarks',
                    search: false,
                    editable: false,
                    width: 245,
                },
                {label: 'Mean Score',
                    name: 'mscore',
                    search: false,
                    editable: false,
                    width: 245,
                },
            ],
            sortname: 'tmarks',
            caption: " Subject Analysis",
            sortorder: 'desc',
            loadonce: true,
            viewrecords: true,
            width: 1000,
            height: 150,
            rowNum: 20000000000,
            beforeSubmitCell: function(rowid, celname, value, iRow, iCol) {
                year = $('#yearx').text();
                class_id = $('#classx').text();
                stream_id = $('#streamx').text();
                examinationtype_id = $('#examinationtypex').text();
                term_id = $('#termx').text();
                return {year_id: year, pupil_id: rowid, class_id: class_id, stream_id: stream_id, examinationtype_id: examinationtype_id,
                    value: value, examination_type: celname, term_id: term_id};
            },
            afterSubmitCell: function(data, id, cellname, value, iRow, iCol) {
                $("#jqGrid2").setGridParam({datatype: 'json'}).trigger('reloadGrid');
            },
            pager: "#jqGridPager2", navOptions: {reloadGridOptions: {fromServer: true}},
        }).setGridParam({datatype: 'json'}).trigger('reloadGrid');
        $('#jqGrid2').navGrid('#jqGridPager2',
                {edit: false, add: false, del: false, search: false, refresh: false, view: false, position: "left", cloneToTop: true},
        {// reload
            reloadAfterSubmit: true
        },
        // options for the Update Dailog
        {
            errorTextFormat: function(data) {
                return 'Error: ' + data.responseText
            },
        });
        $('#jqGrid2').jqGrid('filterToolbar', {
            // JSON stringify all data from search, including search toolbar operators
            stringResult: true,
            // instuct the grid toolbar to show the search options making...
            searchOperators: false,
        });
        //--------------------------------------------------------------------------------------

        $('#searchbtn').click(function(e) {
            e.preventDefault();
            year = $('#year_id').val();
            class_id = $('#class_id').val();
            examinationtype_id = $('#examinationtype_id').val();
            term_id = $('#term_id').val();
            var error = 0;
            if (year == '') {
                $('#year_id').addClass('error');
                error = 1;
            } else {
                $('#year_id').removeClass('error');
            }
            if (term_id == '') {
                $('#term_id').addClass('error');
                error = 1;
            } else {
                $('#term_id').removeClass('error');
            }
            if (class_id == '') {
                $('#class_id').addClass('error');
                error = 1;
            } else {
                $('#class_id').removeClass('error');
            }
            if (examinationtype_id == '') {
                $('#examinationtype_id').addClass('error');
                error = 1;
            } else {
                $('#examinationtype_id').removeClass('error');
            }
            if (error == 0) {
                $("#jqGrid").setGridParam({datatype: 'json',
                    postData: {year_id: year, class_id: class_id, examinationtype_id: examinationtype_id,
                        term_id: term_id}}).trigger('reloadGrid');
                $("#jqGrid2").setGridParam({datatype: 'json',
                    postData: {year_id: year, class_id: class_id, examinationtype_id: examinationtype_id,
                        term_id: term_id}}).trigger('reloadGrid');
                $('#yearx').text(year);
                $('#classx').text(class_id);
                $('#examinationtypex').text(examinationtype_id);
                $('#termx').text(term_id);
                $('#year_id').prop('disabled', true);
                $('#class_id').prop('disabled', true);
                $('#examinationtype_id').prop('disabled', true);
                // $('#subject_id').prop('disabled', true);
                $('#term_id').prop('disabled', true);
            }
        });
        $('#refreshbtn').click(function(e) {
            e.preventDefault();
            $('.form-control').prop('disabled', false);
        });
        $("#jqGrid").jqGrid('navButtonAdd',
                '#jqGrid_toppager_left',
                {caption: " Export Merit List",
                    buttonicon: 'glyphicon glyphicon-list',
                    id: "btnAddNewItem",
                    onClickButton: function() {
                        exportData("jqGrid", "Merit List " + new Date());
                    }
                });
        $("#jqGrid2").jqGrid('navButtonAdd',
                '#jqGrid2_toppager_left',
                {caption: " Export Subject Analysis",
                    buttonicon: 'glyphicon glyphicon-list',
                    id: "btnAddNewItem",
                    onClickButton: function() {
                        exportData("jqGrid2", "Merit List " + new Date());
                    }
                });






    });

</script>

