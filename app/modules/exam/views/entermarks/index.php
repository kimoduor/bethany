<style>
    .error{
        border: solid 1px #ff6633;
    }
    .hidec{
        display: none;
    }
</style>
<?php
$model = new Marks();
?>
<span id="yearx" class="hidec"></span><span id="classx"  class="hidec"></span>
<span id="streamx"  class="hidec"></span><span  class="hidec" id="subjectx"></span><span  class="hidec" id="termx"></span>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-tencent-weibo"></i> Select Marks to modify</div>
                <div class="tools">
                    <a href="javascript:;" class="expand" id="widget1">
                    </a>
                </div>
            </div>
            <div class="portlet-body form" id="body2">
                <form action="#/entermarks" id="form-username" class="form-horizontal form-bordered" method="post">
                    <div class="form-group">
                        <div class="col-md-1">Year *</div>
                        <div class="col-md-2">
                            <?php echo CHtml::activeDropDownList($model, 'year_id', Years::model()->getListData("year_id", "year", true, '', array(), "year_id ASC"), array('class' => 'select2 form-control', 'id' => 'year_id', 'required' => true)); ?>
                        </div>
                        <div class="col-md-1">Class *</div>
                        <div class="col-md-2">

                            <?php echo CHtml::activeDropDownList($model, 'class_id', Classes::model()->getListData("class_id", "class", true, '', array(), "class_id ASC"), array('class' => 'select2 form-control', 'id' => 'class_id', 'required' => true)); ?>
                        </div>

                        <div class="col-md-1">Stream *</div>
                        <div class="col-md-2">
                            <?php echo CHtml::activeDropDownList($model, 'stream_id', Streams::model()->getListData("stream_id", "stream", true, '', array(), "stream_id ASC"), array('class' => 'select2 form-control', 'id' => 'stream_id', 'required' => true)); ?>
                        </div>
                        <div class="col-md-1">Subject *</div>
                        <div class="col-md-2">

                            <?php echo CHtml::activeDropDownList($model, 'subject_id', Subjects::model()->getListData("subject_id", "subject", true, '', array(), "subject_id ASC"), array('class' => 'select2 form-control', 'id' => 'subject_id', 'required' => true)); ?>
                        </div>
                        <div class="col-md-1">Term *</div>
                        <div class="col-md-2">
                            <?php echo CHtml::activeDropDownList($model, 'term_id', Terms::model()->getListData("term_id", "term", true, '', array(), "term_id ASC"), array('class' => 'select2 form-control', 'id' => 'term_id', 'required' => true)); ?>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button name="sub1" id="searchbtn" type="submit" class="btn green"><i class="fa fa-search"></i> Search</button>
                                <button name="sub2" id="refreshbtn" type="submit" class="btn btn-default"><i class="fa fa-refresh"></i> Refresh</button>
                            </div>
                        </div>
                    </div>
                </form>
                <div style="margin-left:20px">
                    <table id="jqGrid"></table>
                    <div id="jqGridPager"></div>
                </div> 
            </div>
        </div>

    </div>
</div>
<?php
$allexams = Examinationtypes::model()->findAll();
$myarray = array();
foreach ($allexams as $exam) {
    $myarray[] = array('label' => "$exam->examinationtype", "search" => false, "name" => "marks$exam->examinationtype_id", "editable" => true, "width" => "245");
    $myarray[] = array('label' => "Grade", "search" => false, "name" => "grade$exam->examinationtype_id", "editable" => "false", "width" => "95");
}
$examsstring = json_encode($myarray);
$exams1 = str_replace("[", "", $examsstring);
$exams = str_replace("]", "", $exams1);
?><script>
    $.jgrid.defaults.width = 1200;
    $.jgrid.defaults.responsive = true;
    $.jgrid.defaults.styleUI = 'Bootstrap';</script>

<script type="text/javascript">
    $(document).ready(function() {
        $.jgrid.styleUI.Bootstrap.base.rowTable = "table table-bordered table-striped";
        var lastSel;
        var year;
        var class_id;
        var stream_id;
        var subject_id;
        var term_id;
        var template = "<div style='margin-left:15px;'>";
        template += "<div> First Name: </div><div>{fname} </div>";
        template += "<hr style='width:100%;'/>";
        template += "<div> {sData} {cData}  </div></div>";
        $("#jqGrid").jqGrid({
            url: 'data/entermarks',
            datatype: "local",
            rownumbers: true,
            toppager: true, cloneToTop: true,
            colModel: [
                {
                    label: '#',
                    name: 'pupil_id',
                    hidden: true,
                    width: 8,
                    key: true,
                    search: false,
                },
                {label: 'Class',
                    name: 'class_id',
                    hidden: true,
                    editable: true,
                    edittype: 'select',
                    formatter: 'select',
                    editoptions: {value:<?php echo Details::classjson(); ?>},
                    stype: 'select',
                    searchoptions: {value:<?php echo Details::classjson(); ?>},
                    width: 200,
                },
                {label: 'Stream',
                    name: 'stream_id',
                    editable: true,
                    edittype: 'select',
                    hidden: true,
                    formatter: 'select',
                    editoptions: {value:<?php echo Details::streamjson(); ?>},
                    stype: 'select',
                    searchoptions: {value:<?php echo Details::streamjson(); ?>},
                    width: 200,
                },
                {label: 'Admission No',
                    name: 'adm',
                    editable: false,
                    width: 245,
                },
                {label: 'Name',
                    name: 'name',
                    editable: false,
                    width: 305,
                },
<?php
echo $exams;
?>,
                {label: 'Average',
                    name: 'average',
                    search: false,
                    editable: false,
                    width: 245,
                },
                {label: 'Grade',
                    name: 'avgrade',
                    editable: false,
                    search: false,
                    width: 100,
                },
            ],
            sortname: 'pupil_id',
            sortorder: 'asc',
            loadonce: true,
            viewrecords: true,
            width: 1000,
            height: 400,
            rowNum: 20,
            'cellEdit': true,
            'cellsubmit': 'remote',
            'cellurl': 'exam/entermarks/create',
            beforeSubmitCell: function(rowid, celname, value, iRow, iCol) {
                year = $('#yearx').text();
                class_id = $('#classx').text();
                stream_id = $('#streamx').text();
                subject_id = $('#subjectx').text();
                term_id = $('#termx').text();
                return {year_id: year, pupil_id: rowid, class_id: class_id, stream_id: stream_id, subject_id: subject_id,
                    value: value, examination_type: celname, term_id: term_id};
            },
            afterSubmitCell: function(data, id, cellname, value, iRow, iCol) {
                $("#jqGrid").setGridParam({datatype: 'json'}).trigger('reloadGrid');
            },
            pager: "#jqGridPager", navOptions: {reloadGridOptions: {fromServer: true}},
        }).setGridParam({datatype: 'json'}).trigger('reloadGrid');
        $('#jqGrid').navGrid('#jqGridPager',
                {edit: false, add: false, del: false, search: true, refresh: true, view: true, position: "left", cloneToTop: true},
        {// reload
            reloadAfterSubmit: true
        },
        // options for the Update Dailog
        {
            errorTextFormat: function(data) {
                return 'Error: ' + data.responseText
            },
        });
        $('#jqGrid').jqGrid('filterToolbar', {
            // JSON stringify all data from search, including search toolbar operators
            stringResult: true,
            // instuct the grid toolbar to show the search options
            searchOperators: false,
        });

        $('#searchbtn').click(function(e) {
            e.preventDefault();
            year = $('#year_id').val();
            class_id = $('#class_id').val();
            stream_id = $('#stream_id').val();
            subject_id = $('#subject_id').val();
            term_id = $('#term_id').val();
            var error = 0;
            if (year == '') {
                $('#year_id').addClass('error');
                error = 1;
            } else {
                $('#year_id').removeClass('error');
            }
            if (term_id == '') {
                $('#term_id').addClass('error');
                error = 1;
            } else {
                $('#term_id').removeClass('error');
            }
            if (class_id == '') {
                $('#class_id').addClass('error');
                error = 1;
            } else {
                $('#class_id').removeClass('error');
            }
            if (stream_id == '') {
                $('#stream_id').addClass('error');
                error = 1;
            } else {
                $('#stream_id').removeClass('error');
            }
            if (subject_id == '') {
                $('#subject_id').addClass('error');
                error = 1;
            } else {
                $('#subject_id').removeClass('error');
            }
            if (error == 0) {
                $("#jqGrid").setGridParam({datatype: 'json',
                    postData: {year_id: year, class_id: class_id, stream_id: stream_id, subject_id: subject_id,
                        term_id: term_id}}).trigger('reloadGrid');
                $('#yearx').text(year);
                $('#classx').text(class_id);
                $('#streamx').text(stream_id);
                $('#subjectx').text(subject_id);
                $('#termx').text(term_id);
                $('#year_id').prop('disabled', true);
                $('#class_id').prop('disabled', true);
                $('#stream_id').prop('disabled', true);
                $('#subject_id').prop('disabled', true);
                $('#term_id').prop('disabled', true);
            }
        });
        $('#refreshbtn').click(function(e) {
            e.preventDefault();
            $('.form-control').prop('disabled', false);
        });
    });

</script>

