<script>
    $.jgrid.defaults.width = 1200;
            $.jgrid.defaults.responsive = true;
            $.jgrid.defaults.styleUI = 'Bootstrap';</script>
<div style="margin-left:20px">
    <table id="jqGrid"></table>
    <div id="jqGridPager"></div>
</div>

<script type="text/javascript">

            $(document).ready(function() {
    $.jgrid.styleUI.Bootstrap.base.rowTable = "table table-bordered table-striped";
            var template = "<div style='margin-left:15px;'>";
            template += "<div>Subject: </div><div>{subject} </div>";
            template += "<hr style='width:100%;'/>";
            template += "<div> {sData} {cData}  </div></div>";
            $("#jqGrid").jqGrid({
    url: 'data/subjects',
            // we set the changes to be made at client side using predefined word clientArray
            //editurl: '',
            datatype: "local",
            rownumbers: true,
            toppager: true,
            colModel: [
            {
            label: '#',
                    name: 'subject_id',
                    hidden: true,
                    width: 8,
                    key: true,
                    search: false,
                    editable: false,
            },
            {
            label: 'Subject ',
                    name: 'subject',
                    width: 200,
                    editable: true
            },
            ],
            sortname: 'subject',
            loadonce: true,
            toolbar: true,
            viewrecords: true,
            width: 1000,
            subGrid: true,
            height: 400,
            rowNum: 20,
            pager: "#jqGridPager",
            subGridRowExpanded: function(subgrid_id, row_id) {
            var subgrid_table_id, pager_id;
                    subgrid_table_id = subgrid_id + "_t";
                    pager_id = "p_" + subgrid_table_id;
                    var rowData = $("#jqGrid").getRowData(row_id);
                    var subject_id = rowData['subject_id'];
                    $("#" + subgrid_id).html("<table id='" + subgrid_table_id + "' class='scroll'></table><div id='" + pager_id + "' class='scroll'></div>");
                    jQuery("#" + subgrid_table_id).jqGrid({
            url: 'data/subjectgrades/?subject_id=' + subject_id,
                    datatype: "local",
                    colModel: [
                    {label: '#',
                            name: 'subjectgrade_id',
                            hidden: true,
                            width: 8,
                            key: true,
                            search: false,
                            editable: false,
                    },
                    {label: 'From Mark',
                            name: 'frommark',
                            editable: true,
                            width: 245,
                    },
                    {label: 'To Mark',
                            name: 'tomark',
                            editable: true,
                            width: 245,
                    },
                    {
                    label: 'Grade',
                            editable: true,
                            name: 'grade',
                            width: 200,
                    },
                    {
                    label: 'Comment',
                            editable: true,
                            name: 'comment',
                            width: 200,
                    }
                    ],
                    pager: pager_id,
                    caption: "Subject Grading System",
                    width: '900',
                    sortname: 'frommark',
                    sortorder: 'desc',
                    rowNum: 20,
                    loadonce: true,
                    height: '100%'
            }).setGridParam({datatype: 'json'}).trigger('reloadGrid');
                    $("#" + subgrid_table_id).jqGrid('navGrid', "#" + pager_id, {edit: true, add: true, del: false},
            {url: "exam/subjectgrades/update?subject_id=" + subject_id, afterSubmit: processAddEdit, closeAfterEdit: true},
            {url: "exam/subjectgrades/create?subject_id=" + subject_id, afterSubmit: processAddEdit, closeAfterAdd: true},
            {url: "exam/subjectgrades/delete"})
            }
    }).setGridParam({datatype: 'json'}).trigger('reloadGrid');
            $('#jqGrid').navGrid('#jqGridPager',
            // the buttons to appear on the toolbar of the grid
            {edit: true, add: true, del: true, search: true, refresh: true, view: true, position: "left", cloneToTop: true},
    {url: "exam/subjects/update", afterSubmit: processAddEdit, closeAfterEdit: true},
    {url: "exam/subjects/create", afterSubmit: processAddEdit, closeAfterAdd: true},
    {url: "exam/subjects/delete"},
    {// reload
    reloadAfterSubmit: true
    },
            // options for the Edit Dialog
            {
            editCaption: "The Edit Dialog",
                    template: template,
                    errorTextFormat: function(data) {
                    return 'Error: ' + data.responseText
                    }
            },
            // options for the Add Dialog
            {
            template: template,
                    errorTextFormat: function(data) {
                    return 'Error: ' + data.responseText
                    }
            },
            // options for the Delete Dailog
            {
            errorTextFormat: function(data) {
            return 'Error: ' + data.responseText
            }

            });
            $('#jqGrid').jqGrid('filterToolbar', {
    // JSON stringify all data from search, including search toolbar operators
    stringResult: true,
            // instuct the grid toolbar to show the search options
            searchOperators: false,
    });
    });

</script>

