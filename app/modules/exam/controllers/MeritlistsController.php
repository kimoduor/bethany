<?php

/**
 * Settings default controller
 * @author Joakim <kimoduor@gmail.com>
 */
class MeritlistsController extends FeesModuleController {

    public function init() {
        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index', 'create', 'update', 'delete', 'data'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
        $this->render('index', array('model' => Marks::model()->searchModel(array(), 10)));
    }

    public function actionCreate() {

        if (isset($_POST['id']) && isset($_POST['value']) && $_POST['value'] > 0&& $_POST['value'] < 100) {
            $model = new Marks();
            $subject_id = $_POST['subject_id'];
            $examination_type = $_POST['examination_type'];
            $pupil_id = $_POST['pupil_id'];
            $term_id = $_POST['term_id'];
            $year_id = $_POST['year_id'];
            $class_id = $_POST['class_id'];
            $stream_id = $_POST['stream_id'];
            $marks = $_POST['value'];
            $examinationtype_id = substr($examination_type, strpos($examination_type, "s") + 1);

            //check if the mark is already the student has thw thing
            $checkexammarks_id = Marks::model()->getScalar("exammarks_id", "subject_id=$subject_id AND "
                    . "examinationtype_id=$examinationtype_id AND pupil_id=$pupil_id AND term_id=$term_id "
                    . "AND year_id=$year_id AND class_id=$class_id");
            if ($checkexammarks_id > 0) {//meaning there is stuff
                $model = Marks::model()->loadModel($checkexammarks_id);
            }
            $model->subject_id = $subject_id;
            $model->examinationtype_id = $examinationtype_id;
            $model->pupil_id = $pupil_id;
            $model->term_id = $term_id;
            $model->year_id = $year_id;
            $model->class_id = $class_id;
            $model->marks = $marks;
            //now get grade
            $model->grade = Details::getgrade($subject_id, $marks);
            $error = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error);
            if (empty($error_message_decoded)) {
                $model->save();
                echo json_encode(true);
            } else {
                echo $error;
            }
        }
    }

    public function actionUpdate() {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];
            $model = Subjects::model()->loadModel($id);
            $model->subject = $_POST['subject'];
            $error = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error);
            if (empty($error_message_decoded)) {
                $model->save();
                echo json_encode(true);
            } else {
                echo $error;
            }
        }
    }

    public function actionDelete() {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];

            try {
                Subjects::model()->loadModel($id)->delete();
                Yii::app()->user->setFlash('success', Lang::t("Successfully Deleted"));
            } catch (Exception $e) {
                Yii::app()->user->setFlash('error', Lang::t("Sorry, This record cannot be deleted, it is being used elsewhere "));
            }
        }
    }

}
