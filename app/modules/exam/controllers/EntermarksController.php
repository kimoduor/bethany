<?php

/**
 * Settings default controller
 * @author Joakim <kimoduor@gmail.com>
 */
class EntermarksController extends FeesModuleController {

    public function init() {
        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index', 'create', 'update', 'delete', 'data'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
        $this->pageTitle = Lang::t('Subjects');

        $model = new Subjects();
        $model_class_name = get_class($model);
        $this->render('index', array('model' => Subjects::model()->searchModel(array(), 10, 'marks_id')));
    }

    public function actionCreate() {

        if (isset($_POST['id']) && isset($_POST['value']) && $_POST['value'] > 0 && $_POST['value'] < 100) {
            $model = new Marks();
            $subject_id = $_POST['subject_id'];
            $examination_type = $_POST['examination_type'];
            $pupil_id = $_POST['pupil_id'];
            $term_id = $_POST['term_id'];
            $year_id = $_POST['year_id'];
            $class_id = $_POST['class_id'];
            $stream_id = $_POST['stream_id'];
            $marks = $_POST['value'];

            $examinationtype_id = substr($examination_type, strpos($examination_type, "s") + 1);

            //check if the mark is already the student has thw thing
            $checkexammarks_id = Marks::model()->getScalar("exammarks_id", "subject_id=$subject_id AND "
                    . "examinationtype_id=$examinationtype_id AND pupil_id=$pupil_id AND term_id=$term_id "
                    . "AND year_id=$year_id AND class_id=$class_id");
            if ($checkexammarks_id > 0) {//meaning there is stuff
                $model = Marks::model()->loadModel($checkexammarks_id);
            }
            $model->subject_id = $subject_id;
            $model->examinationtype_id = $examinationtype_id;
            $model->pupil_id = $pupil_id;
            $model->term_id = $term_id;
            $model->year_id = $year_id;
            $model->class_id = $class_id;
            $model->marks = $marks;

            $model->grade = Details::getgrade($subject_id, $marks);
            $error = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error);
            if (empty($error_message_decoded)) {
                $model->save();

                //update the average

                $average = Marks::model()->getAverage("marks", "subject_id='$subject_id' AND "
                        . " pupil_id=$model->pupil_id AND term_id='$term_id' "
                        . " AND year_id='$year_id' AND class_id='$class_id'");
                //echo $average;
                //   exit;
                if ($average > 0) {
                    $avgrade = Details::getgrade($subject_id, $average);
                    //enter in the average table
                    //check if update or new record 
                    $averagemodel = new Averagemarks();
                    $avexammarks_id = Averagemarks::model()->getScalar("avexammarks_id", "subject_id='$subject_id' AND "
                            . " pupil_id=$model->pupil_id AND term_id='$term_id' AND year_id='$year_id' AND class_id='$class_id'");
                    if ($avexammarks_id > 0) {
                        $averagemodel = Averagemarks::model()->loadModel($avexammarks_id);
                    }
                    $averagemodel->subject_id = $subject_id;
                    $model->examinationtype_id = $examinationtype_id;
                    $averagemodel->pupil_id = $pupil_id;
                    $averagemodel->term_id = $term_id;
                    $averagemodel->year_id = $year_id;
                    $averagemodel->class_id = $class_id;
                    $averagemodel->marks = $average;
                    $averagemodel->grade = $avgrade;
                    $averagemodel->save();
                    //---------------look for subject position--------------
                    //get all marks of the students and put them in array
                    $conditionx = "subject_id='$subject_id' AND term_id='$term_id' AND year_id='$year_id' AND class_id='$class_id'";
                    $allsubjectmarks = Averagemarks::model()->findAll($conditionx);
                    $submarks = array();
                    foreach ($allsubjectmarks as $allsubjectmark) {
                        $submarks[$allsubjectmark->pupil_id] = $allsubjectmark->marks;
                    }
                    arsort($submarks);
                  
                    $prevposition2 = 0;
                    $prevmark2 = -1;
                    $mypos = -1;
                    $pos2 = 1;
                    foreach ($submarks as $currentpupil_id => $mymarks) {
                        $currentmark2 = $mymarks;
                        if ($currentmark2 == $prevmark2) {
                            $mypos = $prevposition2;
                        } else {
                            $mypos = $pos2;
                            $prevposition2 = $pos2;
                        }
                        $prevmark2 = $currentmark2;
                        $pos2 = $pos2 + 1;                        
                          
                        
                        $avexammarks_idx = Averagemarks::model()->getScalar("avexammarks_id", "$conditionx  AND pupil_id=$currentpupil_id ");
                        if ($avexammarks_idx > 0) {
                                 $averagemodelx = Averagemarks::model()->loadModel($avexammarks_idx);
                            $averagemodelx->subjposition = $mypos;
                            $averagemodelx->save();
                        }
                    }
                    //---------------End looking for Subject position-------
                }
                //
                //now start looking at the termly positions and total marks
                // $termlyexammodel = new Termlyexam();
                ////===============================get the total marks and Update====================
                $conditionz = "pupil_id=$model->pupil_id AND term_id='$term_id' AND year_id='$year_id' AND class_id='$class_id'";
                $totalmarks = Averagemarks::model()->getSum("marks", $conditionz);
                $checktermlyexam = Termlyexam::model()->getScalar("termlyexam_id", "$conditionz");
                if ($checktermlyexam > 0) {
                    $termlyexammodel = Termlyexam::model()->loadModel($checktermlyexam);
                } else {
                    $termlyexammodel = new Termlyexam();
                }
                $termlyexammodel->year_id = $year_id;
                $termlyexammodel->class_id = $class_id;
                $termlyexammodel->pupil_id = $pupil_id;
                $termlyexammodel->term_id = $term_id;
                $termlyexammodel->totalmarks = $totalmarks;
                $termlyexammodel->totalmarks = $totalmarks;
                $termlyexammodel->save();
                ////===============================End Marks update====================
                //===========================get class positions===========================>>>>>
                $conditionclass = "term_id='$term_id' AND year_id='$year_id' AND class_id='$class_id'";
                $conditionzstream = "term_id='$term_id' AND year_id='$year_id' AND class_id='$class_id' AND pupil_id IN (SELECT pupil_id FROM pupil_id WHERE stream_id=$stream_id)";
                $averagemarkmodels = Averagemarks::model()->getDistinct("pupil_id", "$conditionclass");
                $array = array();
                foreach ($averagemarkmodels as $averagemarkmodel) {
                    $conditiony = "pupil_id=" . $averagemarkmodel['pupil_id'] . " AND term_id='$term_id' AND year_id='$year_id' AND class_id='$class_id'";
                    //get the total marks of each student and put them in an indexed array
                    $array[$averagemarkmodel['pupil_id']] = $exammarks_id = Averagemarks::model()->getSum("marks", $conditiony);
                }


                if ($array) {

                    arsort($array);
                    //  var_dump($array);
                    //  exit;

                    $prevposition = 0;
                    $prevmark = -1;
                    $theposition = "";
                    $pos = 1;
                    $number = count($array);

                    foreach ($array as $key => $currentmark) {
                        if ($currentmark == $prevmark) {
                            $theposition = $prevposition;
                        } else {
                            $theposition = $pos;
                            $prevposition = $pos;
                        }
                        //update class position termly marks and number of students who sat for that exam 

                        $conditionyy = "pupil_id=" . $key . " AND term_id='$term_id' AND year_id='$year_id' AND class_id='$class_id'";
                        $numbersubjects = Subjects::model()->count();
                        $checktermlyexam = Termlyexam::model()->getScalar("termlyexam_id", "$conditionyy");

                        //getCount("subject_id", "$conditionyy");
                        if ($checktermlyexam > 0) {
                            $termlymodel = Termlyexam::model()->loadModel($checktermlyexam);
                        } else {
                            $termlymodel = new Termlyexam();
                        }
                        $termlymodel->year_id = $year_id;
                        $termlymodel->pupil_id = $key;
                        $termlymodel->class_id = $class_id;
                        $termlymodel->term_id = $term_id;
                        $termlymodel->numberofclasspupils = $number;
                        $termlymodel->numberofsubjects = $numbersubjects;
                        $termlymodel->class_position = $theposition;
                        $check = Pupils::model()->getScalar("pupil_id", "pupil_id=$key");
                        $termlymodel->save();
                        $pos++;
                    }
                }
                //===========================================================>>
                //===========================get stream positions===========================>>>>>
                $conditionzstream = "term_id='$term_id' AND year_id='$year_id' AND class_id='$class_id' AND pupil_id IN (SELECT pupil_id FROM pupils WHERE stream_id=$stream_id)";
                $averagemarkmodels = Averagemarks::model()->getDistinct("pupil_id", "$conditionzstream");
                $array2 = array();
                foreach ($averagemarkmodels as $averagemarkmodel) {
                    $conditiony = "pupil_id=" . $averagemarkmodel['pupil_id'] . " AND term_id='$term_id' AND year_id='$year_id' AND class_id='$class_id'";
                    //get the total marks of each student and put them in an indexed array
                    $array2[$averagemarkmodel['pupil_id']] = $exammarks_id = Averagemarks::model()->getSum("marks", $conditiony);
                }
//                var_dump($array2);
//                exit;

                if ($array2) {
                    arsort($array2);
                    $prevposition = 0;
                    $prevmark = -1;
                    $theposition = "";
                    $pos2 = 1;
                    $number = count($array2);
                    foreach ($array2 as $key => $currentmark) {
                        if ($currentmark == $prevmark) {
                            $theposition = $prevposition;
                        } else {
                            $theposition = $pos2;
                            $prevposition = $pos2;
                        }
                        //update class position termly marks and number of students who sat for that exam
                        $conditionyy = "pupil_id=" . $key . " AND term_id='$term_id' AND year_id='$year_id' AND class_id='$class_id'";
                        $checktermlyexam = Termlyexam::model()->getScalar("termlyexam_id", "$conditionyy");
                        if ($checktermlyexam > 0) {
                            $termlymodel = Termlyexam::model()->loadModel($checktermlyexam);
                            $termlymodel->stream_position = $theposition;
                            $termlymodel->numberofstreampupils = $number;
                            $termlymodel->save();
                        }

                        $pos2++;
                    }
                }
                //===========================================================>>
                echo json_encode(true);
            } else {
                echo $error;
            }
        }
    }

}
