<?php

/**
 * Settings default controller
 * @author Joakim <kimoduor@gmail.com>
 */
class ReportcardsController extends ExamModuleController {

    public function init() {

        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index', 'printreportcard', 'tester'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
        // echo "joakim";
        $this->render('index', array('model' => FeesTransactions::model()->searchModel()));
    }

    public static function calculate_average($arr) {
        $total = 0;
        $count = count($arr); //total numbers in array
        foreach ($arr as $value) {
            $total = $total + $value; // total value of array numbers
        }
        $average = ($total / $count); // get average value
        return $average;
    }

    public function actionPrintreportcard() {

        ob_start();
        //$receipt_id = $_GET['receipt_id'];          
        $pdf = Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'P', 'cm', 'A4', true, 'UTF-8');
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor(Yii::app()->user->name);
        $pdf->SetTitle("Report Cards");
        $pdf->SetSubject("Report Cards");
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        if (isset($_GET['pupil_id'])) {
            $pupilstring = $_GET['pupil_id'];
            $pupilsarray = explode(",", $pupilstring);
            foreach ($pupilsarray as $pupil_id) {

                $term_id = $_GET['term_id'];
                $year_id = $_GET['year_id'];
               //$stream_id = $_GET['stream_id'];
                $class_id = $_GET['class_id'];

                $maincondition = "pupil_id='$pupil_id' AND term_id=$term_id AND year_id=$year_id AND class_id=$class_id ";
                $reportdetails = Averagemarks::model()->findAll($maincondition);

                $pupilmodel = Pupils::model()->loadModel($pupil_id);
                $pdf->AddPage();
                $pdf->Image(Yii::app()->baseUrl . '/malt/img/newlogo.jpeg', 2, 1, 2, 2, '', '', 'T', false, 100, '', false, false, 0, false, false, false);
                $pdf->SetFont('', '', 20);
                $pdf->Cell(14, 1, "Bethany Brains Academy", 0, 0, "C");
                //   $pdf->Cell(8, 1, 'St. Abigail Academy', 1, 1, 'L', 0, '', 0);
                $pdf->SetTextColor(255, 0, 0);
                $pdf->SetFont('', '', 16);

                $passport = $pupilmodel->passport?$pupilmodel->passport:"default.jpg";
                $pdf->Image(Yii::app()->baseUrl . '/public/pupilsphotos/' . $passport, 16, 1, 2, 2, '', '', 'T', false, 100, '', false, false, 1, false, false, false);
                $pdf->Ln(2);
                $pdf->SetFont('', '', 10);
                $pdf->SetTextColor(0, 0, 0);
                $pdf->Cell(18, 1, ucwords(Terms::model()->get($term_id, 'term')) . " ACADEMIC REPORT FORM -YEAR " . Years::model()->get($year_id, "year"), 0, 0, "C");
                $pdf->Ln();
                $pdf->Cell(8, 0, "Name: " . $pupilmodel->fname . "  " . $pupilmodel->lname . " " . $pupilmodel->othername, 1, 0, "L");
                $pdf->Cell(4, 0, "ADM No: " . $pupilmodel->adm, 1, 0, "L");
                $pdf->Cell(7, 0, "Class : " . Classes::model()->get($pupilmodel->class_id, "class") . "   " . Streams::model()->get($pupilmodel->stream_id, "stream"), 1, 0, "L");
                $pdf->Ln();
                $markscondition = " pupil_id='$pupil_id' AND term_id='$term_id' AND year_id='$year_id' AND class_id='$class_id'";
                $pdf->Cell(4, 0, "House : " . Houses::model()->get($pupilmodel->house_id, "house"), 1, 0, "L");
                $pdf->Cell(5, 0, "Stream Position : " . Termlyexam::model()->getScalar("stream_position", $markscondition) . " Of " . Termlyexam::model()->getScalar("numberofstreampupils", $markscondition), 1, 0, "L");
                $pdf->Cell(5, 0, "Class Position : " . Termlyexam::model()->getScalar("class_position", $markscondition) . " Of " . Termlyexam::model()->getScalar("numberofclasspupils", $markscondition), 1, 0, "L");

                $pdf->Cell(5, 0, "Total Marks :" . number_format(Termlyexam::model()->getScalar("totalmarks", $markscondition)) . " Of " . (Termlyexam::model()->getScalar("numberofsubjects", $markscondition) * 100), 1, 0, "L");
                $pdf->Ln(1);
                $pdf->Cell(3, 1, "Subject", 1, 0, "L");
                $exams = Examinationtypes::model()->findAll();
                foreach ($exams as $exam) {
                    $pdf->Cell(2, 1, $exam->examinationtype, 1, 0, "L");
                }
                $pdf->Cell(2, 1, "Average %", 1, 0, "L");
                $pdf->Cell(1, 1, "GRD", 1, 0, "L");
                $pdf->Cell(1, 1, "POS", 1, 0, "L");
                $pdf->Cell(3, 1, "Remarks", 1, 0, "L");
                $pdf->Cell(1, 1, "Initial", 1, 0, "L");
                $pdf->Ln();
                $totalmarks = 0;
                $averagearray = array();
                $subjects = Subjects::model()->findAll();
                $subcounter = 0;
                foreach ($subjects as $subject) {
                    $pdf->Cell(3, 0, $subject->subject, 1, 0, "L");
                    //get marks and grades
                    foreach ($exams as $exam) {

                        $marks = Marks::model()->getScalar("marks", "$maincondition AND subject_id=$subject->subject_id"
                                . " AND examinationtype_id=$exam->examinationtype_id");
                        $grade = Marks::model()->getScalar("grade", "$maincondition AND subject_id=$subject->subject_id"
                                . " AND examinationtype_id=$exam->examinationtype_id");
                        $pdf->Cell(2, 0, number_format($marks) . "   " . $grade, 1, 0, "L");
                    }
                    $averagemarks = Averagemarks::model()->getScalar("marks", "$maincondition AND subject_id=$subject->subject_id");
                    $averagegrade = Averagemarks::model()->getScalar("grade", "$maincondition AND subject_id=$subject->subject_id");
                    $subjposition = Averagemarks::model()->getScalar("subjposition", "$maincondition AND subject_id=$subject->subject_id");
                    $pdf->Cell(2, 0, number_format($averagemarks), 1, 0, "L");
                    $pdf->Cell(1, 0, $averagegrade, 1, 0, "L");
                    $pdf->Cell(1, 0, $subjposition, 1, 0, "L");
                    $pdf->Cell(3, 0, Details::getsubjectcomment($subject->subject_id, $averagemarks), 1, 0, "L");
                    $pdf->Cell(1, 0, Users::model()->get(Subjectteachers::model()->getScalar("user_id", "subject_id=$subject->subject_id AND class_id=$class_id AND stream_id=$pupilmodel->stream_id"), "initials"), 1, 0, "L");
                    $totalmarks = $totalmarks + $averagemarks;
                    $averagearray[] = $averagemarks;
                    $pdf->Ln();
                    $subcounter++;
                }
                $pdf->SetFont('', 'B', 11);
                $pdf->Cell(7, 1, "Total Marks: " . number_format($totalmarks) . " Of " . ($subcounter * 100), 1, 0, "L");
                $pdf->Cell(6, 1, "Subject Entry: " . $subcounter, 1, 0, "L");
                $pdf->Cell(6, 1, "Mean Score:   " . number_format(self::calculate_average($averagearray), 1), 1, 0, "L");
                $pdf->Ln();
                $pdf->Cell(19, 1, " Progress Report", 0, 0, "C");
                $pdf->SetFont('', '', 11);
                $pdf->Ln();
                $pdf->Cell(1, 1, "Term", 1, 0, "C");
                $classes = Classes::model()->findAll("class_id<> 9 ORDER BY class_id ");
                foreach ($classes as $class) {
                    $pdf->Cell(2.2, 1, $class->class, 1, 0, "C");
                }
                $pdf->Ln();

                $pdf->SetFont('', '', 8);
                $pdf->Cell(1, 0, "", 1, 0, "C"); //subheaders of meanscore/po=sition
                foreach ($classes as $class) {
                    $pdf->Cell(1.1, 0, "MKS", 1, 0, "C");
                    $pdf->Cell(1.1, 0, "POS", 1, 0, "C");
                }
                $pdf->Ln();

                $terms = Terms::model()->findAll();
                $values = array();

                foreach ($terms as $termx) {
                    $pdf->Cell(1, 0, $termx->term_id, 1, 0, "C");
                    foreach ($classes as $classx) {

                        //get the latest year the pupil ever sat for exam in a specific class
                        $year_idx = Averagemarks::model()->getScalar("year_id", "pupil_id=$pupil_id AND class_id=$classx->class_id ORDER By year_id DESC");
                        $mainconditiontable = " pupil_id=$pupil_id AND class_id=$classx->class_id AND term_id=$termx->term_id AND year_id='$year_idx'";
                        $totalmarksx = Averagemarks::model()->getSum("marks", $mainconditiontable);
                        $values["C" . $classx->class_id . "T" . $termx->term_id] = $totalmarksx;
                        $pdf->Cell(1.1, 0, number_format($totalmarksx), 1, 0, "C");
                        $position = Termlyexam::model()->getScalar("class_position", $mainconditiontable);
                        $pdf->Cell(1.1, 0, $position, 1, 0, "C");
                    }
                    $pdf->Ln();
                }

                $settings = array(
                    'graph_title' => "The Graph of " . $pupilmodel->fname . " " . $pupilmodel->lname . " Marks Scored from Class 1 Through 8",
                    "label_v" => "Total Marks Scored",
                    "label_h" => "Class and Term",
                    'axis_text_angle_h' => -90
                );
                ksort($values);
                $pdf->SetFont('', '', 11);
                $graph = new SVGGraph(640, 280, $settings);
                $graph->colours = array('#E6EFF2');
                $graph->Values($values);
                $output = $graph->fetch('BarGraph');
                $pdf->ImageSVG('@' . $output, $x = 1, $y = 14, $w = '250', $h = '100', $link = '', $align = 'L', $palign = '', $border = 0, $fitonpage = true);
                $pdf->Ln(8);
                $pdf->Cell(19, 2, "Class Teacher's Remarks....", 1, 0, "");
                $pdf->Ln();
                $pdf->Cell(19, 2, "Head Teacher's Remarks.....", 1, 0, "");
                $pdf->Ln();
                $pdf->SetFont('', '', 8);
                $anypendingcharges = Studentcharges::model()->getSum("balance", "pupil_id=$pupil_id");
                $termbalance = FeesPayment::model()->getScalar("term_balance", "pupil_id=$pupil_id  ORDER BY fees_id DESC");
                $yearbalance = FeesPayment::model()->getScalar("year_balance", "pupil_id=$pupil_id  ORDER BY fees_id DESC");
                $totalterm = ($anypendingcharges + $termbalance) < 0 ? (number_format($anypendingcharges + $termbalance, 2)) . "(Prepay)" : (number_format($anypendingcharges + $termbalance, 2));
                $totalyear = ($anypendingcharges + $yearbalance) < 0 ? (number_format($anypendingcharges + $yearbalance, 2)) . "(Prepay)" : (number_format($anypendingcharges + $yearbalance, 2));
                $pdf->Cell(19, 0, "Termly Fees Balance as At NOW  Ksh. " . $totalterm . "         Yearly Fees Balance as At NOW Ksh.$totalyear  ", 1, 0, "");
                $pdf->Ln();
                $closedate = date_create(Closingopeningdate::model()->get(1, "closingdate"));
                $opneingdate = date_create(Closingopeningdate::model()->get(1, "openingdate"));
                $pdf->Cell(19, 1, "Closing Date:   " . date_format($closedate, 'F d,Y') . "    Next Term Opening Date: " . date_format($opneingdate, 'F d,Y') . "                                        "
                        . "                 Parent's Sign........", 1, 0, "");
            }
        }
        ob_end_clean();
        $pdf->Output("Report Cards.pdf", "I");
    }

}
