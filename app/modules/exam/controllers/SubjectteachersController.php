<?php

/**
 * Settings default controller
 * @author Joakim <kimoduor@gmail.com>
 */
class SubjectteachersController extends ExamModuleController {

    public function init() {
        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index', 'create', 'update', 'delete', 'data'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
        $this->render('index', array('model' => Subjectteachers::model()->searchModel(array(), 10, 'subjectteacher_id')));
    }

    public function actionCreate() {
        if (isset($_POST)) {
            $myerror = "";
            $model = new Subjectteachers();
            $model->subject_id = $_POST['subject_id'];
            $model->user_id = $_POST['user_id'];
            $model->class_id = $_POST['class_id'];
            $model->stream_id = $_POST['stream_id'];
            //test if the minimum or the maximum has an overlap

            $duplicatetest = Subjectteachers::model()->getScalar("subjectteacher_id", "subject_id= $model->subject_id AND "
                    . " user_id=$model->user_id AND class_id=$model->class_id AND stream_id=$model->stream_id");

            if ($duplicatetest > 0) {
                $myerror = "Sorry " . Users::model()->get($model->user_id, "fname") . " Cannot be registered for this subject in this stream,"
                        . " Another teacher is already Registered for this Subject in this Class and Stream, Kindly Verify.";
            }

            $error = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error);
            if (empty($error_message_decoded)) {
                if ($myerror != "") {
                    echo json_encode(array("error" => $myerror));
                } else {
                    $model->save();
                    echo json_encode(true);
                }
            } else {
                echo $error;
            }
        }
    }

    public function actionUpdate() {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];
             $myerror = "";
            $model = Subjectteachers::model()->loadModel($id);
            $model->subject_id = $_POST['subject_id'];
            $model->user_id = $_POST['user_id'];
            $model->class_id = $_POST['class_id'];
            $model->stream_id = $_POST['stream_id'];
            //test if the minimum or the maximum has an overlap

            $duplicatetest = Subjectteachers::model()->getScalar("subjectteacher_id", "subject_id= $model->subject_id AND"
                    . " user_id=$model->user_id AND class_id=$model->class_id AND stream_id=$model->stream_id AND"
                    . " subjectteacher_id <>$model->subjectteacher_id");

            if ($duplicatetest > 0) {
               $myerror = "Sorry " . Users::model()->get($model->user_id, "fname") . " Cannot be registered for this subject in this stream,"
                        . " Another teacher is already Registered for this Subject in this Class and Stream, Kindly Verify.";
            }

            $error = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error);
            if (empty($error_message_decoded)) {
                if ($myerror != "") {
                    echo json_encode(array("error" => $myerror));
                } else {
                    $model->save();
                    echo json_encode(true);
                }
            } else {
                echo $error;
            }
        }
    }

    public function actionDelete() {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];

            try {
                Subjectteachers::model()->loadModel($id)->delete();
                Yii::app()->user->setFlash('success', Lang::t("Successfully Deleted"));
            } catch (Exception $e) {
                Yii::app()->user->setFlash('error', Lang::t("Sorry, This record cannot be deleted, it is being used elsewhere "));
            }
        }
    }

}
