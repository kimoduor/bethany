<?php

/**
 * Settings default controller
 * @author Joakim <kimoduor@gmail.com>
 */
class SubjectgradesController extends FeesModuleController {

    public function init() {
        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index', 'create', 'update', 'delete', 'data'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
        $model = new Subjectgrades();
        $model_class_name = get_class($model);
        $this->render('index', array('model' => Subjectgrades::model()->searchModel(array(), 10, 'subjectgrade_id')));
    }

    public function actionCreate() {
        if (isset($_POST)) {
            $myerror = "";
            $model = new Subjectgrades();
            $model->subject_id = @$_GET['subject_id'];
            $model->frommark = $_POST['frommark'];
            $model->tomark = $_POST['tomark'];
            $model->grade = $_POST['grade'];
            $model->comment = $_POST['comment'];
            //test if the minimum or the maximum has an overlap
            $overlaptest = Subjectgrades::model()->findAll("subject_id= $model->subject_id");
            foreach ($overlaptest as $overlaptes) {
                $from = $overlaptes->frommark;
                $to = $overlaptes->tomark;
                if (($model->frommark >= $from && $model->frommark <= $to) || ($model->tomark >= $from && $model->tomark <= $to)) {
                    $myerror = "Marks for this Grade are overlapping with another Grade, Kindly Verify";
                }
            }
            $error = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error);
            if (empty($error_message_decoded)) {
                if ($myerror != "") {
                    echo json_encode(array("error" => $myerror));
                } else {
                    $model->save();
                    echo json_encode(true);
                }
            } else {
                echo $error;
            }
        }
    }

    public function actionUpdate() {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];
             $myerror = "";
            $model = Subjectgrades::model()->loadModel($id);
            $model->subject_id = @$_GET['subject_id'];
            $model->frommark = $_POST['frommark'];
            $model->tomark = $_POST['tomark'];
            $model->grade = $_POST['grade'];
            $model->comment = $_POST['comment'];
              //test if the minimum or the maximum has an overlap
            $overlaptest = Subjectgrades::model()->findAll("subject_id= $model->subject_id AND subjectgrade_id <>$id");
            foreach ($overlaptest as $overlaptes) {
                $from = $overlaptes->frommark;
                $to = $overlaptes->tomark;
                if (($model->frommark >= $from && $model->frommark <= $to) || ($model->tomark >= $from && $model->tomark <= $to)) {
                    $myerror = "Marks for this Grade are overlapping with another Grade, Kindly Verify";
                }
            }
            $error = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error);
              if (empty($error_message_decoded)) {
                if ($myerror != "") {
                    echo json_encode(array("error" => $myerror));
                } else {
                    $model->save();
                    echo json_encode(true);
                }
            } else {
                echo $error;
            }
        }
    }

    public function actionDelete() {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];

            try {
                Subjectgrades::model()->loadModel($id)->delete();
                Yii::app()->user->setFlash('success', Lang::t("Successfully Deleted"));
            } catch (Exception $e) {
                Yii::app()->user->setFlash('error', Lang::t("Sorry, This record cannot be deleted, it is being used elsewhere "));
            }
        }
    }

}
