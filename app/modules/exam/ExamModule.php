<?php

/**
 * This module handles all system settings
 * @author Joakim <kimoduor@gmail.com>
 */
class ExamModule extends CWebModule
{

    public function init()
    {
        parent::init();
    }

    public function beforeControllerAction($controller, $action)
    {
        if (parent::beforeControllerAction($controller, $action)) {
            return true;
        } else
            return false;
    }

}
