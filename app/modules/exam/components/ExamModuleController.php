<?php

/**
 * @author Joakim <kimoduor@gmail.com>
 * Parent controller for the settings module
 */
class ExamModuleController extends Controller
{

    public function init()
    {
        if (empty($this->activeMenu))
            //$this->activeMenu = SettingsModuleConstants::MENU_SETTINGS;
        if (empty($this->resource))
           // $this->resource = SettingsModuleConstants::RES_SETTINGS;
      //  Yii::import('application.modules.fees.forms.*');
        parent::init();
    }

    public function setModulePackage()
    {

        $this->module_package = array(
            'baseUrl' => $this->module_assets_url,
            'js' => array(
                'js/module.js',
                        ),
            'css' => array(
            ),
        );
    }

}
