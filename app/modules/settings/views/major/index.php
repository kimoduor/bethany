<style>
    .portlet-body{
        display: block;
    }

</style>
<?php
if (isset($_GET['d'])) {
    ?>
    <div class="success">Successfully Saved</div>
    <?php
}
?>

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PORTLET-->

        <div class="portlet box red-pink">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-barcode"></i> Termly Updates</div>
                <div class="tools">
                    <a href="javascript:;" class="expand" id="widget1" onclick="widgetclicked()">
                    </a>

                </div>
            </div>
            <div class="portlet-body form" id="body2">
                <form action="settings/major/create" id="form-username" class="form-horizontal form-bordered" method="post">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Promote Pupils<font color="red"></font></label>
                                <div class="col-sm-2">
                                    <input type="checkbox" name="updateclasses" value="1" id="updateclasses"/>
                                </div>
                                <div class="col-sm-5">When this Option is Selected, 
                                    all pupils shall be promoted to the next classes. 
                                    Those who should repeat shall be individually  edited to update their current classes
                                    <br/>
                                    <label class="label label-danger">Note that:</label>  This Operation is irreversible because the system may not 'Know' the Alumni and complex class issues, therefore should be carried out under very precarious conditions.
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Update <strong>Termly</strong> Balances<font color="red"></font></label>
                                <div class="col-sm-2">
                                    <input type="checkbox" name="updatebalances" value="1"  id="updatebalances"/>
                                </div>
                                <div class="col-sm-5">When this Option is Selected, 
                                    all the balances of the pupils shall be updated with the current termly fees structure of each 
                                    class respective of the current class a pupil belongs to.<br/>
                                    <label class="label label-danger">Note that:</label>  This Operation is irreversible, therefore should be carried out under very precarious conditions.
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Update <strong>Yearly</strong> Balances<font color="red"></font></label>
                                <div class="col-sm-2">
                                    <input type="checkbox" name="updateyearbalances" value="1"  id="updateyearbalances"/>
                                </div>
                                <div class="col-sm-5">When this Option is Selected, 
                                    all the balances of the pupils shall be updated with the current yearly fees structure of each 
                                    class respective of the current class a pupil belongs to.<br/>
                                    <label class="label label-danger">Note that: THIS OPERATION IS DONE ANNUALLY</label>  This Operation is irreversible, therefore should be carried out under very precarious conditions.
                                </div>
                            </div>
                              <div class="form-group">
                                <label class="col-sm-2 control-label">Register Pupils For Examination <strong>Yearly</strong> </label>
                                <div class="col-sm-2">
                                    <input type="checkbox" name="registerexams" value="1"  id="registerexams"/>
                                </div>
                                <div class="col-sm-5">When this Option is Selected, 
                                    all the Pupils shall be registered in their current Classes for Examination they are likely to handle
                                    <br/>
                                    <label class="label label-danger">Note that: THIS OPERATION IS DONE ANNUALLY</label>  However, it can be done severally because in case the operation finds that the pupil is registered, it will continue registering Others and assume the pupil.Therefore, it has no harm if done severally
                                </div>
                            </div>


                        </div>
                    </div>

                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button name="sub1" type="submit" class="btn green"><i class="fa fa-edit"></i> Update</button>
                                <button type="button" class="btn default">Cancel</button>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
        </div>

    </div>
</div>


