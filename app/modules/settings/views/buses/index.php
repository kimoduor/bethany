<script>
    $.jgrid.defaults.width = 1200;
    $.jgrid.defaults.responsive = true;
    $.jgrid.defaults.styleUI = 'Bootstrap';</script>
<div style="margin-left:20px">
    <table id="jqGrid"></table>
    <div id="jqGridPager"></div>
</div>
<?php
$modelz = Route::model()->findAll();
$output = array();
$output[""] = "[Select One]";
foreach ($modelz as $modelyz) {
    $output[$modelyz->route_id] = $modelyz->route;
}
$theoutputz = json_encode($output);
?>
<script type="text/javascript">

    $(document).ready(function() {
        $.jgrid.styleUI.Bootstrap.base.rowTable = "table table-bordered table-striped";
        var template = "<div style='margin-left:15px;'>";
        template += "<div>Plate Number: </div><div>{plate_number} </div>";
        template += "<hr style='width:100%;'/>";
        template += "<div> {sData} {cData}  </div></div>";
        $("#jqGrid").jqGrid({
            url: 'data/buses',
            // we set the changes to be made at client side using predefined word clientArray
            //editurl: '',
            datatype: "local",
            rownumbers: true,
            toppager: true,
            colModel: [
                {
                    label: '#',
                    name: 'bus_id',
                    hidden: true,
                    width: 8,
                    key: true,
                    search: false,
                    editable: false,
                },
                {
                    label: 'Registration Number ',
                    name: 'plate_number',
                    width: 200,
                    editable: true
                },
                {
                    label: 'Tag Name ',
                    name: 'tag_name',
                    width: 200,
                    editable: true
                },
                {
                    label: 'Route ',
                    name: 'route_id',
                    width: 200,
                    editable: true,
                    edittype: 'select',
                    formatter: 'select',
                    editoptions: {value: <?php echo $theoutputz;?>},
                },
                {
                    label: 'Phone Number',
                    name: 'phone_number',
                    width: 200,
                    editable: true
                },
                {
                    label: 'Mac Address',
                    name: 'mac_address',
                    width: 200,
                    editable: true
                },
                {
                    label: 'Main Driver Name ',
                    name: 'main_driver_name',
                    width: 200,
                    editable: true
                },
                {
                    label: 'Driver Phone Number',
                    name: 'driver_phone',
                    width: 200,
                    editable: true
                }
            ],
            sortname: 'bus_id',
            sortorder: 'asc',
            loadonce: true,
            toolbar: true,
            viewrecords: true,
            width: 1000,
            height: 400,
            rowNum: 20,
            pager: "#jqGridPager"
        }).setGridParam({datatype:'json'}).trigger('reloadGrid');
        $('#jqGrid').navGrid('#jqGridPager',
                // the buttons to appear on the toolbar of the grid
                        {edit: true, add: true, del: true, search: true, refresh: true, view: true, position: "left", cloneToTop: true},
                {url: "settings/buses/update", afterSubmit: processAddEdit, closeAfterEdit: true},
                {url: "settings/buses/create", afterSubmit: processAddEdit, closeAfterAdd: true},
                {url: "settings/buses/delete"},
                {// reload
                    reloadAfterSubmit: true
                },
                // options for the Edit Dialog
                {
                    editCaption: "The Edit Dialog",
                    template: template,
                    errorTextFormat: function(data) {
                        return 'Error: ' + data.responseText
                    }
                },
                // options for the Add Dialog
                {
                    template: template,
                    errorTextFormat: function(data) {
                        return 'Error: ' + data.responseText
                    }
                },
                // options for the Delete Dailog
                {
                    errorTextFormat: function(data) {
                        return 'Error: ' + data.responseText
                    }

                });

                $('#jqGrid').jqGrid('filterToolbar', {
                    // JSON stringify all data from search, including search toolbar operators
                    stringResult: true,
                    // instuct the grid toolbar to show the search options
                    searchOperators: false,
                });
            });

</script>

