<script>
    $.jgrid.defaults.width = 1200;
    $.jgrid.defaults.responsive = true;
    $.jgrid.defaults.styleUI = 'Bootstrap';</script>
<div style="margin-left:20px">
    <table id="jqGrid"></table>
    <div id="jqGridPager"></div>
</div>

<script type="text/javascript">

    $(document).ready(function() {
        $.jgrid.styleUI.Bootstrap.base.rowTable = "table table-bordered table-striped";
        var template = "<div style='margin-left:15px;'>";
        template += "<div>Bank Name: </div><div>{bank_name} </div>";
        template += "<hr style='width:100%;'/>";
        template += "<div> {sData} {cData}  </div></div>";
        $("#jqGrid").jqGrid({
            url: 'data/closingopeningdate',
            // we set the changes to be made at client side using predefined word clientArray
            //editurl: '',
            datatype: "local",
            rownumbers: true,
            toppager: true,
            colModel: [
                {
                    label: '#',
                    name: 'closingopening_id',
                    hidden: true,
                    width: 8,
                    key: true,
                    search: false,
                    editable: false,
                },
                  {
                    label: 'Closing Date',
                    name: 'closingdate',
                    width: 100,
                    editable: true,
                    search:false,
                    editrules: {required: true},
                    editoptions: {dataInit: function(elem) {
                            $(elem).datepicker({autoclose: true,
                                changeMonth: true, changeYear: true,
                                defaultDate: new Date(),
                                format: 'yyyy-mm-dd'});
                        }}
                },
                                  {
                    label: 'Opening Date ',
                    name: 'openingdate',
                    width: 100,
                    search:false,
                    editable: true,
                    editrules: {required: true},
                    editoptions: {dataInit: function(elem) {
                            $(elem).datepicker({autoclose: true,
                                changeMonth: true, changeYear: true,
                                defaultDate: new Date(),
                                format: 'yyyy-mm-dd'});
                        }}
                },

            ],
            sortname: 'closingopening_id',
            sortorder: 'asc',
            loadonce: true,
            toolbar: true,
            viewrecords: true,
            width: 1000,
            height: 40,
            rowNum: 20,
            pager: "#jqGridPager"
        }).setGridParam({datatype: 'json'}).trigger('reloadGrid');
        $('#jqGrid').navGrid('#jqGridPager',
                // the buttons to appear on the toolbar of the grid
                        {edit: true, add: false, del: false, search: false, refresh: true, view: true, position: "left", cloneToTop: true},
                {url: "settings/closingopeningdate/update", afterSubmit: processAddEdit, closeAfterEdit: true},
                {url: "settings/closingopeningdate/create", afterSubmit: processAddEdit, closeAfterAdd: true},
                {url: "settings/closingopeningdate/delete"},
                {// reload
                    reloadAfterSubmit: true
                },
                // options for the Edit Dialog
                {
                    editCaption: "The Edit Dialog",
                    template: template,
                    errorTextFormat: function(data) {
                        return 'Error: ' + data.responseText
                    }
                },
                // options for the Add Dialog
                {
                    template: template,
                    errorTextFormat: function(data) {
                        return 'Error: ' + data.responseText
                    }
                },
                // options for the Delete Dailog
                {
                    errorTextFormat: function(data) {
                        return 'Error: ' + data.responseText
                    }

                });

                $('#jqGrid').jqGrid('filterToolbar', {
                    // JSON stringify all data from search, including search toolbar operators
                    stringResult: true,
                    // instuct the grid toolbar to show the search options
                    searchOperators: false,
                });
            });

</script>

