<script>
    $.jgrid.defaults.width = 1200;
    $.jgrid.defaults.responsive = true;
    $.jgrid.defaults.styleUI = 'Bootstrap';</script>
<div style="margin-left:20px">
    <table id="jqGrid"></table>
    <div id="jqGridPager"></div>
</div>
<script type="text/javascript">

    $(document).ready(function() {
        $.jgrid.styleUI.Bootstrap.base.rowTable = "table table-bordered table-striped";
        var template = "<div style='margin-left:15px;'>";
        template += "<div> Repayment Mode: </div><div>{loantype} </div>";
        template += "<hr style='width:100%;'/>";
        template += "<div> {sData} {cData}  </div></div>";
        $("#jqGrid").jqGrid({
            url: 'data/repaymentmodes',
            // we set the changes to be made at client side using predefined word clientArray
            //editurl: '',
            datatype: "json",
            rownumbers: true,
            toppager: true,
            colModel: [
                {
                    label: '#',
                    name: 'repaymentmode_id',
                    hidden:true,
                    width: 8,
                    key:true,
                    search:false,
                    editable: false,
                },
                {
                    label: 'Repayment Mode',
                    name: 'repaymentmode',
                    width: 200,
                    editable: true,
                    editrules: {required: true}
                }
            ],
            sortname: 'repaymentmode_id',
            sortorder: 'asc',
            loadonce: false,
            toolbar: true,
            viewrecords: true,
            width: 1000,
            height: 400,
            rowNum: 20,
            pager: "#jqGridPager"
        }).trigger('reloadGrid');
        //   $("#gridid").jqGrid('setGridParam',{datatype:'json'}).trigger('reloadGrid');
        //    $('#gridId').jqGrid('setGridWidth', '8000');
        $('#jqGrid').navGrid('#jqGridPager',
                // the buttons to appear on the toolbar of the grid
                        {edit: true, add: true, del: true, search: true, refresh: true, view: true, position: "left", cloneToTop: true},
                {url: "settings/repaymentmodes/update", closeAfterEdit: true},
                {url: "settings/repaymentmodes/create", closeAfterAdd: true},
                {url: "settings/repaymentmodes/delete"},
                {// reload
                    reloadAfterSubmit: true
                },
                // options for the Edit Dialog
                {
                    editCaption: "The Edit Dialog",
                    template: template,
                    errorTextFormat: function(data) {
                        return 'Error: ' + data.responseText
                    }
                },
                // options for the Add Dialog
                {
                    template: template,
                    errorTextFormat: function(data) {
                        return 'Error: ' + data.responseText
                    }
                },
                // options for the Delete Dailog
                {
                    errorTextFormat: function(data) {
                        return 'Error: ' + data.responseText
                    }

                });

                $('#jqGrid').jqGrid('filterToolbar', {
                    // JSON stringify all data from search, including search toolbar operators
                    stringResult: true,
                    // instuct the grid toolbar to show the search options
                    searchOperators: false,
                });
            });

</script>

