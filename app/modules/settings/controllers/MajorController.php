<?php

/**
 * Settings default controller
 * @author Joakim <kimoduor@gmail.com>
 */
class MajorController extends SettingsModuleController {

    public function init() {
        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index', 'create', 'update', 'delete', 'data'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
        $this->render('index', array('model' => Pupils::model()->searchModel(array(), 10)));
    }

    public function actionCreate() {
        $current_year = Years::model()->getScalar("year_id", "active=1");
        $current_term = Terms::model()->getScalar("term_id", "active=1");
        if (isset($_POST['updateclasses'])) {
            $pupils = Pupils::model()->findAll();
            foreach ($pupils as $pupil) {
                if ($pupil->class_id < 9) {
                    //take all fees balances to arreas and empty the balances to be zeros
                    $fees_id = FeesPayment::model()->getScalar("fees_id", "pupil_id=$pupil->pupil_id ORDER BY fees_id DESC");
                    if ($fees_id > 0) {
                        $feesmodel = FeesPayment::model()->loadModel($fees_id);
                        if ($feesmodel->year_balance > 0) {//only happens when the student has a fees balance
                            $modelstudentcharges = new Studentcharges();
                            $modelstudentcharges->charge_id = 1;
                            $modelstudentcharges->pupil_id = $pupil->pupil_id;
                            $modelstudentcharges->pupil_id = $pupil->pupil_id;
                            $modelstudentcharges->amount = $feesmodel->year_balance;
                            $modelstudentcharges->amount_charged = 0;
                            $modelstudentcharges->balance = $feesmodel->year_balance;
                            $modelstudentcharges->save();
                            $feesmodel = FeesPayment::model()->loadModel($fees_id);
                            $feesmodel->term_balance = 0;
                            $feesmodel->year_balance = 0;
                            $feesmodel->save();
                        }
                    }
                    $model = Pupils::model()->loadModel($pupil->pupil_id);
                    $model->class_id = ($pupil->class_id + 1);
                    $model->save();
                }
            }
        }
        //update termly fees balances
        if (isset($_POST['updatebalances'])) {
            $pupils = Pupils::model()->findAll();
            foreach ($pupils as $pupil) {
                if ($pupil->class_id < 9) {
                    //get the termly balance
                    $fees_id = FeesPayment::model()->getScalar("fees_id", "pupil_id=$pupil->pupil_id ORDER BY fees_id DESC");
                    if (!$fees_id) {//meaning the student has never paid anything
                        $feestransactionmodel = new FeesTransactions();
                        $feestransactionmodel->pupil_id = $pupil->pupil_id;
                        $feestransactionmodel->transaction_details = "Initial Termly Fees for Term-" . Terms::model()->get($current_term, "term") . "Year-" . Years::model()->get($current_year, "year");
                        $feestransactionmodel->amount = 0;
                        $feestransactionmodel->paymenttype_id = 1;
                        $feestransactionmodel->save();
                        $feespaymentmodel = new FeesPayment();
                        $feespaymentmodel->amount = 0;
                        $feespaymentmodel->pupil_id = $pupil->pupil_id;
                        $feespaymentmodel->transaction_id = $feestransactionmodel->transaction_id;
                        $feespaymentmodel->term_id = $current_term;
                        $feespaymentmodel->year_id = $current_year;
                        $feespaymentmodel->save();
                        $fees_id = $feespaymentmodel->fees_id;
                    }



                    $model = FeesPayment::model()->loadModel($fees_id);

                    //get current term fees structure
                    $termfees = FeesStructure::model()->getScalar("amount", "class_id=$pupil->class_id AND year_id=$current_year AND term_id=$current_term");
                    $model->term_balance = $model->term_balance + $termfees;
                    $model->save();
                }
            }
        }
        //update yearly fees balances

        if (isset($_POST['updateyearbalances'])) {
            $pupils = Pupils::model()->findAll();
            foreach ($pupils as $pupil) {
                if ($pupil->class_id < 9) {
                    //get the termly balance
                    $fees_id = FeesPayment::model()->getScalar("fees_id", "pupil_id=$pupil->pupil_id ORDER BY fees_id DESC");
                    if (!$fees_id) {//meaning the student has never paid anything
                        $feestransactionmodel = new FeesTransactions();
                        $feestransactionmodel->pupil_id = $pupil->pupil_id;
                        $feestransactionmodel->transaction_details = "Initial Termly Fees for Term-" . Terms::model()->get($current_term, "term") . "Year-" . Years::model()->get($current_year, "year");
                        $feestransactionmodel->amount = 0;
                        $feestransactionmodel->paymenttype_id = 1;
                        $feestransactionmodel->save();
                        $feespaymentmodel = new FeesPayment();
                        $feespaymentmodel->amount = 0;
                        $feespaymentmodel->pupil_id = $pupil->pupil_id;
                        $feespaymentmodel->transaction_id = $feestransactionmodel->transaction_id;
                        $feespaymentmodel->term_id = $current_term;
                        $feespaymentmodel->year_id = $current_year;
                        $feespaymentmodel->save();
                        $fees_id = $feespaymentmodel->fees_id;
                    }

                    $model = FeesPayment::model()->loadModel($fees_id);
                    //get current term fees structure
                    $yearfees = FeesStructure::model()->getSum("amount", "class_id=$pupil->class_id AND year_id=$current_year");
                    $model->year_balance = $model->year_balance + $yearfees;
                    $model->save();
                }
            }
        }
        if (isset($_POST['registerexams'])) {
            $pupils = Pupils::model()->findAll();
            foreach ($pupils as $pupil) {//loop pupils
                if ($pupil->class_id < 9) {
                    $subjects = Subjects::model()->findAll();
                    foreach ($subjects as $subject) {//loop subjects
                        $terms = Terms::model()->findAll();
                        foreach ($terms as $term) {//loop terms
                            $examinationtypes = Examinationtypes::model()->findAll();
                            foreach ($examinationtypes as $examinationtype) {
                                //test whether the record is there or not
                                $year_id = Years::model()->getScalar("year_id", "active=1");

                                $class_id = $pupil->class_id;
                                $condition = "subject_id='$subject->subject_id' AND examinationtype_id='$examinationtype->examinationtype_id' AND "
                                        . " pupil_id='$pupil->pupil_id' AND term_id='$term->term_id' AND year_id='$year_id' AND class_id='$class_id'";
                                $exammarks_id = Marks::model()->getScalar("exammarks_id", $condition);
                                $model = "";
                                if ($exammarks_id > 0) {
                                    $model = Marks::model()->loadModel($exammarks_id);
                                } else {
                                    $model = new Marks();
                                }
                                $model->subject_id = $subject->subject_id;
                                $model->examinationtype_id = $examinationtype->examinationtype_id;
                                $model->pupil_id = $pupil->pupil_id;
                                $model->term_id = $term->term_id;
                                $model->year_id = $year_id;
                                $model->class_id = $class_id;
                                if ($model->save()) {
                                   //do nothing 
                                } else {
                                    continue;
                                }
                            }
                        }
                    }
                }
            }
        }



        $this->redirect(Yii::app()->baseUrl . '/#/major?d=s');
    }

}
