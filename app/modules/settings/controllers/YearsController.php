<?php

/**
 * Settings default controller
 * @author Joakim <kimoduor@gmail.com>
 */
class YearsController extends SettingsModuleController {

    public function init() {

        $this->resourceLabel = Lang::t('Years');
        // $this->resource = DistributorsModuleConstants::RES_DISTIBUTORS;
        $this->activeTab = 'Years';
        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index', 'create', 'update', 'delete', 'data'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
        $this->pageTitle = Lang::t('Years');

        $model = new Years();
        $model_class_name = get_class($model);
        //$this->render('index', array('model' => $model));
        $condition = '';
        $this->render('index', array('model' => Years::model()->searchModel(array(), 10, 'year_id', $condition)));
    }

    public function actionCreate() {
        //$this->hasPrivilege(Acl::ACTION_UPDATE);
        $this->pageTitle = Lang::t('Years');
        if (isset($_POST)) {
            $model = new Years();
            $model->year = $_POST['year'];
            $model->active = $_POST['active'];
            $error = CActiveForm::validate($model);
            if ($model->active == 1) {//check if there is another year with 1
                $anyactiveyear = Years::model()->getScalar("year", "active=1 AND year_id<>$model->year_id");
                if ($anyactiveyear > 0) {
                    $error = json_encode(array("active" => "The Year $anyactiveyear is Active, Kindly Deactivate it before activating Year $model->year"));
                }
            }

            $error_message_decoded = CJSON::decode($error);
            if (empty($error_message_decoded)) {
                $model->save();
                echo json_encode(true);
            } else {
                echo $error;
            }
        }
    }

    public function actionUpdate() {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];
            $model = Years::model()->loadModel($id);
            $model->year = $_POST['year'];
            $model->active = $_POST['active'];
            $error = CActiveForm::validate($model);
            if ($model->active == 1) {//check if there is another year with 1
                $anyactiveyear = Years::model()->getScalar("year", "active=1 AND year_id<>$model->year_id");
                if ($anyactiveyear > 0) {
                    $error = json_encode(array("active" => "The Year $anyactiveyear is Active, Kindly Deactivate it before activating Year $model->year"));
                }
            }
            $error_message_decoded = CJSON::decode($error);
            if (empty($error_message_decoded)) {
                $model->save();
                echo json_encode(true);
            } else {
                echo $error;
            }
        }
    }

    public function actionDelete() {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];

            try {
                Years::model()->loadModel($id)->delete();
                Yii::app()->user->setFlash('success', Lang::t("Successfully Deleted"));
            } catch (Exception $e) {
                Yii::app()->user->setFlash('error', Lang::t("Sorry, This record cannot be deleted, it is being used elsewhere "));
            }
        }
    }

}
