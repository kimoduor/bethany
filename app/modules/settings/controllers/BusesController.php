<?php

/**
 * Settings default controller
 * @author Joakim <kimoduor@gmail.com>
 */
class BusesController extends SettingsModuleController {

    public function init() {

        $this->resourceLabel = Lang::t('Buses');
        // $this->resource = DistributorsModuleConstants::RES_DISTIBUTORS;
        $this->activeTab = 'Buses';
        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index', 'create', 'update', 'delete', 'data'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
        $this->pageTitle = Lang::t('Buses');

        $model = new Bus();
        $model_class_name = get_class($model);
        //$this->render('index', array('model' => $model));
        $condition = '';
        $this->render('index', array('model' => Bus::model()->searchModel(array(), 10, 'bus_id', $condition)));
    }

    public function actionCreate() {
        //$this->hasPrivilege(Acl::ACTION_UPDATE);
        $this->pageTitle = Lang::t('Bus');
        if (isset($_POST)) {
            $model = new Bus();
            $model->plate_number = $_POST['plate_number'];
            $model->tag_name = $_POST['tag_name'];
            $model->main_driver_name = $_POST['main_driver_name'];
            $model->route_id = $_POST['route_id'];
            $model->phone_number = $_POST['phone_number'];
            $model->mac_address = $_POST['mac_address'];
            $model->driver_phone = $_POST['driver_phone'];
            
            $error = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error);
            if (empty($error_message_decoded)) {
                $model->save();
                  echo json_encode(true);
            } else {
                echo $error;
            }
        }
    }

    public function actionUpdate() {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];
            $model = Bus::model()->loadModel($id);
            $model->plate_number = $_POST['plate_number'];
            $model->tag_name = $_POST['tag_name'];
            $model->main_driver_name = $_POST['main_driver_name'];
            $model->route_id = $_POST['route_id'];
            $model->phone_number = $_POST['phone_number'];
            $model->mac_address = $_POST['mac_address'];
            $model->driver_phone = $_POST['driver_phone'];
            $error = CActiveForm::validate($model);
             $error_message_decoded = CJSON::decode($error);
            if (empty($error_message_decoded)) {
                                $model->save();
                                 echo json_encode(true);
            } else {
                echo $error;
            }
        }
    }

    public function actionDelete() {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];

            try {
                Bus::model()->loadModel($id)->delete();
                Yii::app()->user->setFlash('success', Lang::t("Successfully Deleted"));
            } catch (Exception $e) {
                Yii::app()->user->setFlash('error', Lang::t("Sorry, This record cannot be deleted, it is being used elsewhere "));
            }
        }
    }

}
