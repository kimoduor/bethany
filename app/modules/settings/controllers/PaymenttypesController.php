<?php

/**
 * Settings default controller
 * @author Joakim <kimoduor@gmail.com>
 */
class PaymenttypesController extends SettingsModuleController {

    public function init() {

        $this->resourceLabel = Lang::t('Paymenttypes');
        // $this->resource = DistributorsModuleConstants::RES_DISTIBUTORS;
        $this->activeTab = 'Paymenttypes';
        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index', 'create', 'update', 'delete', 'data'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
        $this->pageTitle = Lang::t('Banks');

        $model = new Paymenttypes();
        $model_class_name = get_class($model);
        //$this->render('index', array('model' => $model));
        $condition = '';
        $this->render('index', array('model' => Paymenttypes::model()->searchModel(array(), 10, 'paymenttype_id', $condition)));
    }

    public function actionCreate() {
        //$this->hasPrivilege(Acl::ACTION_UPDATE);
        $this->pageTitle = Lang::t('Paymenttype');
        if (isset($_POST)) {
            $model = new Paymenttypes();
            $model->paymenttype = $_POST['paymenttype'];
            $error = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error);
            if (empty($error_message_decoded)) {
                $model->save();
                echo json_encode(true);
            } else {
                echo $error;
            }
        }
    }

    public function actionUpdate() {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];
            $model = Paymenttypes::model()->loadModel($id);
            $model->paymenttype = $_POST['paymenttype'];
            $error = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error);
            if (empty($error_message_decoded)) {
                $model->save();
                echo json_encode(true);
            } else {
                echo $error;
            }
        }
    }

    public function actionDelete() {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];

            try {
                Paymenttypes::model()->loadModel($id)->delete();
                Yii::app()->user->setFlash('success', Lang::t("Successfully Deleted"));
            } catch (Exception $e) {
                Yii::app()->user->setFlash('error', Lang::t("Sorry, This record cannot be deleted, it is being used elsewhere "));
            }
        }
    }

}
