<?php

/**
 * Settings default controller
 * @author Joakim <kimoduor@gmail.com>
 */
class ChargesController extends SettingsModuleController {

    public function init() {

        $this->resourceLabel = Lang::t('charges');
        // $this->resource = DistributorsModuleConstants::RES_DISTIBUTORS;
        $this->activeTab = 'charges';
        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index', 'create', 'update', 'delete', 'data'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
        $this->pageTitle = Lang::t('Years');

        $model = new Otherfeescharges();
        $model_class_name = get_class($model);
        //$this->render('index', array('model' => $model));
        $condition = '';
        $this->render('index', array('model' => Otherfeescharges::model()->searchModel(array(), 10, 'charge_id', $condition)));
    }

    public function actionCreate() {
        //$this->hasPrivilege(Acl::ACTION_UPDATE);
        $this->pageTitle = Lang::t('charges');
        if (isset($_POST)) {
            $model = new Otherfeescharges();
            $model->chargeparticulars = $_POST['chargeparticulars'];
            $error = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error);
            if (empty($error_message_decoded)) {
                $model->save();
                echo json_encode(true);
            } else {
                echo $error;
            }
        }
    }

    public function actionUpdate() {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];
            $model = Otherfeescharges::model()->loadModel($id);
 $model->chargeparticulars = $_POST['chargeparticulars'];
            $error = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error);
            if (empty($error_message_decoded)) {
                $model->save();
                echo json_encode(true);
            } else {
                echo $error;
            }
        }
    }

    public function actionDelete() {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];

            try {
                Otherfeescharges::model()->loadModel($id)->delete();
                Yii::app()->user->setFlash('success', Lang::t("Successfully Deleted"));
            } catch (Exception $e) {
                Yii::app()->user->setFlash('error', Lang::t("Sorry, This record cannot be deleted, it is being used elsewhere "));
            }
        }
    }

}
