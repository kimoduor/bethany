<?php

/**
 * This is the model class for table "sys_job_processes".
 *
 * The followings are the available columns in table 'sys_job_processes':
 * @property string $id
 * @property string $job_id
 * @property string $last_run_datetime
 * @property integer $status
 * @property string $date_created
 *
 */
class SysJobProcesses extends ActiveRecord implements IMyActiveSearch
{

    const STATUS_RUNNING = '1';
    const STATUS_SLEEPING = '2';

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'sys_job_processes';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('id, job_id', 'required'),
            array('job_id', 'length', 'max' => 30),
            array('last_run_datetime,status', 'safe'),
            array(self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => Lang::t('Process ID'),
            'job_id' => Lang::t('Job'),
            'last_run_datetime' => Lang::t('Last Run'),
            'status' => Lang::t('Status'),
            'date_created' => Lang::t('Date Started'),
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return SysJobProcesses the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function searchParams()
    {
        return array(
            'id',
            'job_id',
            'status',
        );
    }

    /**
     *
     * @param type $value
     * @return type
     */
    public static function decodeStatus($value)
    {
        $decoded = '';
        switch ($value) {
            case self::STATUS_RUNNING:
                $decoded = Lang::t('Running');
                break;
            case self::STATUS_SLEEPING:
                $decoded = Lang::t('Sleeping');
                break;
        }

        return $decoded;
    }

    /**
     * Create a new process
     * @param \SysJobs $job
     * @return string $process_id
     */
    public function createProcess($job)
    {
        $process_id = MyYiiUtils::uuid();
        $now = new CDbExpression('NOW()');

        Yii::app()->db->createCommand()
                ->insert($this->tableName(), array(
                    'id' => $process_id,
                    'job_id' => $job->id,
                    'last_run_datetime' => $now,
                    'status' => self::STATUS_RUNNING,
                    'date_created' => $now,
        ));

        $job->threads = $this->getTotalProcesses($job->id);
        $job->save(false);

        return $process_id;
    }

    /**
     * Clear processes of a job
     * @param string $job_id
     */
    public function clearProcesses($job_id)
    {
        $expiry = 3; //expire all processes which have stalled
        $conditions = '`job_id`=:job_id AND (`last_run_datetime` < DATE_SUB(NOW(), INTERVAL :expiry MINUTE))';
        $params = array(':expiry' => $expiry, 'job_id' => $job_id);

        if ($this->deleteAll($conditions, $params)) {
            Yii::app()->db->createCommand()
                    ->update(SysJobs::model()->tableName(), array('threads' => $this->getTotalProcesses($job_id)), '`id`=:id', array(':id' => $job_id));
        }
    }

    /**
     * Get total processes of a job
     * @param type $job_id
     * @return type
     */
    protected function getTotalProcesses($job_id)
    {
        return $this->getTotals('`job_id`=:t1', array(':t1' => $job_id));
    }

    /**
     *
     * @param type $job_id
     * @return type
     */
    public function getTotalRunning($job_id)
    {
        return $this->getTotals('`job_id`=:t1 AND `status`=:t2', array(':t1' => $job_id, ':t2' => self::STATUS_RUNNING));
    }

    /**
     *
     * @param type $job_id
     * @return type
     */
    public function getTotalSleeping($job_id)
    {
        return $this->getTotals('`job_id`=:t1 AND `status`=:t2', array(':t1' => $job_id, ':t2' => self::STATUS_SLEEPING));
    }

    /**
     * Retire a process
     * @param integer $job_id
     * @param integer $process_id
     * @param integer $expiry_minutes
     */
    public function retireProcess($job_id, $process_id, $expiry_minutes = 30)
    {
        $conditions = '`id`=:id AND (`date_created` < DATE_SUB(NOW(), INTERVAL :expiry MINUTE))';
        $params = array(':expiry' => $expiry_minutes, 'id' => $process_id);

        if ($this->deleteAll($conditions, $params)) {
            Yii::app()->db->createCommand()
                    ->update(SysJobs::model()->tableName(), array('threads' => $this->getTotalProcesses($job_id)), '`id`=:id', array(':id' => $job_id));

            return true;
        }

        return false;
    }

}
