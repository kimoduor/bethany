<?php

/**
 * This is the model class for table "bus".
 *
 * The followings are the available columns in table 'bus':
 * @property string $bus_id
 * @property string $plate_number
 * @property string $tag_name
 * @property string $main_driver_name
 * @property string $route_id
 * @property string $phone_number
 * @property string $mac_address
 * @property string $driver_phone
 */
class Bus extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'bus';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('plate_number', 'required'),
			array('plate_number, tag_name, main_driver_name, phone_number, driver_phone', 'length', 'max'=>255),
			array('route_id', 'length', 'max'=>11),
			array('mac_address', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('bus_id, plate_number, tag_name, main_driver_name, route_id, phone_number, mac_address, driver_phone', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'bus_id' => 'Bus',
			'plate_number' => 'Plate Number',
			'tag_name' => 'Tag Name',
			'main_driver_name' => 'Main Driver Name',
			'route_id' => 'Route',
			'phone_number' => 'Phone Number',
			'mac_address' => 'Mac Address',
			'driver_phone' => 'Driver Phone',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('bus_id',$this->bus_id,true);
		$criteria->compare('plate_number',$this->plate_number,true);
		$criteria->compare('tag_name',$this->tag_name,true);
		$criteria->compare('main_driver_name',$this->main_driver_name,true);
		$criteria->compare('route_id',$this->route_id,true);
		$criteria->compare('phone_number',$this->phone_number,true);
		$criteria->compare('mac_address',$this->mac_address,true);
		$criteria->compare('driver_phone',$this->driver_phone,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Bus the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
