<?php

/**
 * Roles management controller
 * @author Joakim <kimoduor@gmail.com>
 */
class RolesController extends UsersModuleController {

    public function init() {
        $this->resourceLabel = 'Role';
        $this->resource = UsersModuleConstants::RES_USER_ROLES;
        $this->activeMenu = UsersModuleConstants::MENU_USERS;
        $this->activeTab = UsersModuleConstants::TAB_ROLES;

        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index', 'create', 'view', 'privileges','update', 'delete'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
       // Acl::hasPrivilege($this->privileges, $this->resource, Acl::ACTION_VIEW);
        $model = UserRoles::model()->loadModel($id);
        $this->pageTitle = $model->name;

        //$forbidden_resources = Acl::getForbiddenResources(UserLevels::LEVEL_ADMIN);
     //   $resources = UserResources::model()->getResources($forbidden_resources);
        $model_class_name = UserRolesOnResources::model()->getClassName();

        if (isset($_POST[$model_class_name])) {
            if (isset($_POST['users'])) {
                // UserRoles::model()->updateRoleUsers($id, $_POST['users']);
            }
            $roles_on_resources = $_POST[$model_class_name];
            foreach ($roles_on_resources as $key => $rr) {

                UserRolesOnResources::model()->set($key, $id, $rr);
            }
            Yii::app()->user->setFlash('success', Lang::t('SUCCESS_MESSAGE'));
            $this->refresh();
        }

        $this->render('view', array(
            'model' => $model,
            'resources' => $resources,
        ));
    }

    public function actionPrivileges($id) {
        //Acl::hasPrivilege($this->privileges, $this->resource, Acl::ACTION_VIEW);
        $model = UserRoles::model()->loadModel($id);
        $this->pageTitle = $model->name;

        //$forbidden_resources = Acl::getForbiddenResources(UserLevels::LEVEL_ADMIN);
        $privileges = UserPrivileges::model()->getPrivileges();
        $model_class_name = UserRolesOnPrivileges::model()->getClassName();

        if (isset($_POST[$model_class_name])) {

            $roles_on_privileges = $_POST[$model_class_name];
            foreach ($roles_on_privileges as $key => $rr) {


                UserRolesOnPrivileges::model()->set($key, $id, $rr);
            }

            Yii::app()->user->setFlash('success', Lang::t('SUCCESS_MESSAGE'));
            $this->refresh();
        }

        $this->render('privileges', array(
            'model' => $model,
            'privileges' => $privileges,
        ));
    }




   


    public function actionInputPrivileges($id) {
        Acl::hasPrivilege($this->privileges, $this->resource, Acl::ACTION_VIEW);
        $model = UserRoles::model()->loadModel($id);
        $this->pageTitle = $model->name;

        $privileges = InputPrivileges::model()->getPrivileges();
        $model_class_name = RolesOnInputPrivileges::model()->getClassName();

        if (isset($_POST[$model_class_name])) {

            $roles_on_privileges = $_POST[$model_class_name];
            foreach ($roles_on_privileges as $key => $rr) {


                RolesOnInputPrivileges::model()->set($key, $id, $rr);
            }

            Yii::app()->user->setFlash('success', Lang::t('SUCCESS_MESSAGE'));
            $this->refresh();
        }
        // var_dump($privileges);die;

        $this->render('inputprivileges', array(
            'model' => $model,
            'privileges' => $privileges,
        ));
    }

    

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
       // $this->hasPrivilege(Acl::ACTION_CREATE);
        $this->pageTitle = Lang::t(Constants::LABEL_CREATE . ' ' . $this->resourceLabel);

        $model = new UserRoles();
        $model_class_name = $model->getClassName();

        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            $error_message = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error_message);
            
                        if (empty($error_message_decoded)) {
                $model->save(FALSE);
                Yii::app()->user->setFlash('success', "Successfully Saved");
                $this->redirect('index', array('model' => $model::model()->searchModel(array(), $this->settings[SettingsModuleConstants::SETTINGS_ITEMS_PER_PAGE], 'id')));
                //  echo CJSON::encode(array('success' => true, 'message' => Lang::t('SUCCESS_MESSAGE'), 'redirectUrl' => UrlManager::getReturnUrl($this->createUrl('index'))));
                Yii::app()->end();
            }
        }

        $this->render('_form', array(
            'model' => $model,
                ), FALSE, TRUE);
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
       // $this->hasPrivilege(Acl::ACTION_UPDATE);
        $this->pageTitle = Lang::t(Constants::LABEL_UPDATE . ' ' . $this->resourceLabel);

        $model = UserRoles::model()->loadModel($id);
        $model_class_name = $model->getClassName();

        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            $error_message = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error_message);
        
                        if (empty($error_message_decoded)) {
                $model->save(FALSE);
                Yii::app()->user->setFlash('success', "Successfully Saved");
                $this->redirect('index', array('model' => $model::model()->searchModel(array(), $this->settings[SettingsModuleConstants::SETTINGS_ITEMS_PER_PAGE], 'id')));
                //  echo CJSON::encode(array('success' => true, 'message' => Lang::t('SUCCESS_MESSAGE'), 'redirectUrl' => UrlManager::getReturnUrl($this->createUrl('index'))));
                Yii::app()->end();
            }
        }

        $this->render('_form', array(
            'model' => $model,
                ), FALSE, TRUE);
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->hasPrivilege(Acl::ACTION_DELETE);

        UserRoles::model()->loadModel($id)->delete();

        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
    }

    public function actionIndex() {
        //$this->hasPrivilege(Acl::ACTION_VIEW);
        $this->pageTitle = Lang::t($this->resourceLabel . 's');

        $this->render('index', array(
            'model' => UserRoles::model()->searchModel(array(), $this->settings[SettingsModuleConstants::SETTINGS_ITEMS_PER_PAGE], 'name'),
        ));
    }

}
