<?php

/**
 * Settings default controller
 * @author Joakim <kimoduor@gmail.com>
 */
class NewrolesController extends UsersModuleController {

    public function init() {

        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index', 'create', 'update', 'delete', 'data'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionData() {
        $models = Users::model()->findAll();
        $output = array();
        foreach ($models as $model) {
            $array = array();
            $array['id'] = $model->id;
            $array['email'] = $model->email;
            $array['username'] = $model->username;
            $array['phone'] = $model->phone;
            $array['initials'] = $model->initials;
            $array['fname'] = $model->fname;
            $array['lname'] = $model->lname;
            $array['othername'] = $model->othername;
            $array['date_created'] = $model->date_created;
            $array['last_login'] = $model->last_login;
            $array['role_id'] = $model->role_id;
            $output[] = $array;
        }
        $theoutput = json_encode($output);
        echo $theoutput;
    }

    public function actionIndex() {
        $model = new Users();
        $model_class_name = get_class($model);
        $condition = "";
        $this->render('index', array('model' => Users::model()->searchModel(array(), 10, 'id', $condition)));
        // $this->render('index', array('model' => $model));
    }

    public function actionCreate() {
        if (isset($_POST)) {
            $model = new Users();
            $model->email = $_POST['email'];
            $model->email_category = $_POST['email_category'];
            $error = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error);
            if (empty($error_message_decoded)) {
                $model->save();
                echo json_encode(true);
            } else {
                echo $error;
            }
        }
    }

    public function actionUpdate() {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];
            $model = Users::model()->loadModel($id);
            $model->role_id = $_POST['role_id'];
            $model->fname = $_POST['fname'];
             $model->initials = $_POST['initials'];
            $model->lname = $_POST['lname'];

            $error = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error);
            if (empty($error_message_decoded)) {
                $model->save();
                echo json_encode(true);
            } else {
                echo $error;
            }
        }
    }

    public function actionDelete() {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];

            try {
                // Emails::model()->loadModel($id)->delete();
                Yii::app()->user->setFlash('success', Lang::t("Successfully Deleted"));
            } catch (Exception $e) {
                Yii::app()->user->setFlash('error', Lang::t("Sorry, This record cannot be deleted, it is being used elsewhere "));
            }
        }
    }

}
