<?php

$grid_id = 'user-roles-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => Common::pluralize($this->resourceLabel),
    'titleIcon' => '<i class="fa fa-check-square-o"></i>',
    'showExportButton' => true,
    'showSearch' => true,
    'createButton' => array('visible' =>true, 'modal' => true),
    'toolbarButtons' => array(),
    'showRefreshButton' => true,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        'columns' => array(
            array(
                'name' => 'name',
                'type' => 'raw',
                'value' => '$data->name',
            ),
            'description',
            array(
                'class' => 'ButtonColumn',
                'header' => Lang::t(UsersModuleConstants::USER_PRIVILEGES),
                'template' => '{live_privileges}{video_privileges}'
                . '{update}{delete}',
                'htmlOptions' => array('style' => 'width: 500px;'),
                'buttons' => array(
                    'live_privileges' => array(
                        'imageUrl' => false,
                        'label' => '<i class="icon-key fa-2x"></i>',
                        'url' => 'Yii::app()->controller->createUrl("",array("id"=>$data->id))',
                        'visible' => 'true',
                        'options' => array(
                            'class' => 'blue',
                            'title' => 'Live Priviledges',
                        ),
                    ),

                    'video_privileges' => array(
                        'imageUrl' => false,
                        'label' => '<i class="icon-key fa-2x"></i>',
                        'url' => 'Yii::app()->controller->createUrl("",array("id"=>$data->id))',
                        'visible' => 'true',
                        'options' => array(
                            'class' => 'green',
                            'title' => Lang::t('Video Priviledges'),
                        ),
                    ),

                    'update' => array(
                        'imageUrl' => false,
                       'label' => '<i class="icon-pencil"></i>',
                        'url' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->id))',
                        'visible' => 'true',
                        'options' => array(
                            'class' => 'show_modal_form',
                            'title' => Lang::t(Constants::LABEL_UPDATE),
                        ),
                    ),
                    'delete' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa icon-trash text-danger"></i>',
                        'url' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->id))',
                        'visible' => '$this->grid->owner->showLink("' . UsersModuleConstants::RES_USER_PRIVILEGES . '", "' . Acl::ACTION_DELETE . '")?true:false',
                        'url_attribute' => 'data-ajax-url',
                        'options' => array(
                            'data-grid_id' => $grid_id,
                            'data-confirm' => Lang::t('DELETE_CONFIRM'),
                            'class' => 'delete my-update-grid',
                            'title' => Lang::t(Constants::LABEL_DELETE),
                        ),
                    ),
                )
            ),
        ),
    )
));
?>