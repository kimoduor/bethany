<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'my-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-horizontal',
        'role' => 'form',
    )
        ));
?>
<div class="col-md-12">

    <div class="widget box">
        <div class="widget-header">
            <i class="icon-reorder"></i> <h4 class="modal-title">Create Streamer</h4>

        </div>
<div class="modal-body">
    <div class="alert hidden" id="my-modal-notif"></div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'name', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeTextField($model, 'name', array('class' => 'form-control', 'maxlength' => 128)); ?>
            <?php echo $form->error($model, 'name'); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo $form->labelEx($model, 'description', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeTextArea($model, 'description', array('class' => 'form-control', 'maxlength' => 255, 'rows' => 3)); ?>
       <?php echo $form->error($model, 'description'); ?>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo Lang::t('Close') ?></button>
    <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> <?php echo Lang::t($model->isNewRecord ? 'Create' : 'Save changes') ?></button>
</div>
<?php $this->endWidget(); ?>