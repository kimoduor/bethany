<?php

/**
 * This is the model class for table "fees_transactions".
 *
 * The followings are the available columns in table 'fees_transactions':
 * @property string $transaction_id
 * @property integer $pupil_id
 * @property string $transaction_details
 * @property double $amount
 * @property string $date
 * @property string $datepicked
 * @property string $paymenttype_id
 * @property string $cheque_no
 * @property string $bank_id
 * @property string $term_id
 * @property string $year_id
 */
class FeesTransactions extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'fees_transactions';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pupil_id, transaction_details, amount,  paymenttype_id', 'required'),
			array('pupil_id', 'numerical', 'integerOnly'=>true),
			array('amount', 'numerical'),
			array('paymenttype_id, bank_id, term_id, year_id', 'length', 'max'=>11),
			array('cheque_no', 'length', 'max'=>255),
			array('datepicked', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('transaction_id, pupil_id, transaction_details, amount, date, datepicked, paymenttype_id, cheque_no, bank_id, term_id, year_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'transaction_id' => 'Transaction',
			'pupil_id' => 'Pupil',
			'transaction_details' => 'Transaction Details',
			'amount' => 'Amount',
			'date' => 'Date',
			'datepicked' => 'Datepicked',
			'paymenttype_id' => 'Paymenttype',
			'cheque_no' => 'Cheque No',
			'bank_id' => 'Bank',
			'term_id' => 'Term',
			'year_id' => 'Year',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('transaction_id',$this->transaction_id,true);
		$criteria->compare('pupil_id',$this->pupil_id);
		$criteria->compare('transaction_details',$this->transaction_details,true);
		$criteria->compare('amount',$this->amount);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('datepicked',$this->datepicked,true);
		$criteria->compare('paymenttype_id',$this->paymenttype_id,true);
		$criteria->compare('cheque_no',$this->cheque_no,true);
		$criteria->compare('bank_id',$this->bank_id,true);
		$criteria->compare('term_id',$this->term_id,true);
		$criteria->compare('year_id',$this->year_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FeesTransactions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
