<?php

/**
 * This is the model class for table "studentcharges".
 *
 * The followings are the available columns in table 'studentcharges':
 * @property string $studentcharge_id
 * @property string $charge_id
 * @property string $pupil_id
 * @property double $amount
 * @property double $amount_charged
 * @property double $balance
 * @property string $datecreated
 */
class Studentcharges extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'studentcharges';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('charge_id, pupil_id', 'required'),
			array('amount, amount_charged, balance', 'numerical'),
			array('charge_id, pupil_id', 'length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('studentcharge_id, charge_id, pupil_id, amount, amount_charged, balance, datecreated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'studentcharge_id' => 'Studentcharge',
			'charge_id' => 'Charge',
			'pupil_id' => 'Pupil',
			'amount' => 'Amount',
			'amount_charged' => 'Amount Charged',
			'balance' => 'Balance',
			'datecreated' => 'Date Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('studentcharge_id',$this->studentcharge_id,true);
		$criteria->compare('charge_id',$this->charge_id,true);
		$criteria->compare('pupil_id',$this->pupil_id,true);
		$criteria->compare('amount',$this->amount);
		$criteria->compare('amount_charged',$this->amount_charged);
		$criteria->compare('balance',$this->balance);
		$criteria->compare('datecreated',$this->datecreated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Studentcharges the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
