<?php

/**
 * This is the model class for table "feesvoteheads".
 *
 * The followings are the available columns in table 'feesvoteheads':
 * @property string $feesvoteheads_id
 * @property string $voteheadyear_id
 * @property string $fees_id
 * @property double $amount_charged
 * @property double $balance
 * @property string $datecreated
 * @property integer $repeater
 */
class Feesvoteheads extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'feesvoteheads';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('voteheadyear_id, fees_id, amount_charged', 'required'),
			array('repeater', 'numerical', 'integerOnly'=>true),
			array('amount_charged, balance', 'numerical'),
			array('voteheadyear_id, fees_id', 'length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('feesvoteheads_id, voteheadyear_id, fees_id, amount_charged, balance, datecreated, repeater', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'feesvoteheads_id' => 'Feesvoteheads',
			'voteheadyear_id' => 'Voteheadyear',
			'fees_id' => 'Fees',
			'amount_charged' => 'Amount Charged',
			'balance' => 'Balance',
			'datecreated' => 'Datecreated',
			'repeater' => '0 means the votehead was served fully;  1 means there is a balance that needs to be served',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('feesvoteheads_id',$this->feesvoteheads_id,true);
		$criteria->compare('voteheadyear_id',$this->voteheadyear_id,true);
		$criteria->compare('fees_id',$this->fees_id,true);
		$criteria->compare('amount_charged',$this->amount_charged);
		$criteria->compare('balance',$this->balance);
		$criteria->compare('datecreated',$this->datecreated,true);
		$criteria->compare('repeater',$this->repeater);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Feesvoteheads the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
