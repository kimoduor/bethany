<?php

/**
 * This is the model class for table "voteheadyear".
 *
 * The followings are the available columns in table 'voteheadyear':
 * @property string $voteheadyear_id
 * @property string $votehead_id
 * @property string $class_id
 * @property string $year_id
 * @property integer $priority
 * @property double $amount
 */
class Voteheadyear extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'voteheadyear';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('votehead_id, class_id, year_id, amount', 'required'),
			array('priority', 'numerical', 'integerOnly'=>true),
			array('amount', 'numerical'),
			array('votehead_id, class_id, year_id', 'length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('voteheadyear_id, votehead_id, class_id, year_id, priority, amount', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'voteheadyear_id' => 'Voteheadyear',
			'votehead_id' => 'Votehead',
			'class_id' => 'Class',
			'year_id' => 'Year',
			'priority' => 'Priority',
			'amount' => 'Amount',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('voteheadyear_id',$this->voteheadyear_id,true);
		$criteria->compare('votehead_id',$this->votehead_id,true);
		$criteria->compare('class_id',$this->class_id,true);
		$criteria->compare('year_id',$this->year_id,true);
		$criteria->compare('priority',$this->priority);
		$criteria->compare('amount',$this->amount);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Voteheadyear the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
