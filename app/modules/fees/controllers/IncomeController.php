<?php

/**
 * Settings default controller
 * @author Joakim <kimoduor@gmail.com>
 */
class IncomeController extends FeesModuleController {

    public function init() {

        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index', 'delete', 'create', 'update'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
        $this->render('index', array('model' => Pupils::model()->searchModel()));
    }

    public function actionCreate() {
        //$this->hasPrivilege(Acl::ACTION_UPDATE);
        $this->pageTitle = Lang::t('Banks');
        if (isset($_POST)) {
            $model = new Income();
            $model->details = $_POST['details'];
            $model->amount = $_POST['amount'];
            $model->user_id = Yii::app()->user->id;
            $model->incometype_id = $_POST['incometype_id'];
            $model->paymenttype_id = $_POST['paymenttype_id'];
            $model->bank_id = $_POST['bank_id'];
            $model->cheque_no = $_POST['cheque_no'];
            $model->fromname = $_POST['fromname'];
            $model->date = $_POST['date'];
            $error = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error);
            if (empty($error_message_decoded)) {
                $model->save();
                echo json_encode(true);
            } else {
                echo $error;
            }
        }
    }

    public function actionUpdate() {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];
            $model = Income::model()->loadModel($id);
           $model->details = $_POST['details'];
            $model->amount = $_POST['amount'];
            $model->user_id = Yii::app()->user->id;
            $model->incometype_id = $_POST['incometype_id'];
            $model->paymenttype_id = $_POST['paymenttype_id'];
            $model->bank_id = $_POST['bank_id'];
            $model->cheque_no = $_POST['cheque_no'];
            $model->fromname = $_POST['fromname'];
            $model->date = $_POST['date'];
            $error = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error);
            if (empty($error_message_decoded)) {
                $model->save();
                echo json_encode(true);
            } else {
                echo $error;
            }
        }
    }

    public function actionDelete() {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];

            try {
                Income::model()->loadModel($id)->delete();
                Yii::app()->user->setFlash('success', Lang::t("Successfully Deleted"));
            } catch (Exception $e) {
                Yii::app()->user->setFlash('error', Lang::t("Sorry, This record cannot be deleted, it is being used elsewhere "));
            }
        }
    }

}
