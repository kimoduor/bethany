<?php

/**
 * Settings default controller
 * @author Joakim <kimoduor@gmail.com>
 */
class ReceiptsController extends FeesModuleController {

    public function init() {

        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index', 'printreceipt'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
        $this->render('index', array('model' => FeesTransactions::model()->searchModel(array(), 10, 'transaction_id')));
    }

    public function actionPrintreceipt() {
        ob_start();
        $receipt_id = $_GET['receipt_id'];
        //  $receipt_id = 75;
        $receiptno = Details::receiptfromtransaction($receipt_id);


        $model = FeesTransactions::model()->loadModel($receipt_id);
        $pdf = Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'P', 'cm', 'A4', true, 'UTF-8');
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor(Yii::app()->user->name);
        $pdf->SetTitle($receiptno);
        $pdf->SetSubject($receiptno);
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        $pdf->AddPage();
        $pdf->Image(Yii::app()->baseUrl . '/malt/img/newlogo.jpeg', 2, 1, 2, 2, '', '', 'T', false, 100, '', false, false, 0, false, false, false);
        $pdf->SetFont('', '', 16);
        $pdf->Cell(10, 1, "Bethany Brains Academy ", 0, 0, "R");
        //   $pdf->Cell(8, 1, 'St. Abigail Academy', 1, 1, 'L', 0, '', 0);
        $pdf->SetTextColor(255, 0, 0);
        $pdf->SetFont('', '', 16);
        $pdf->Cell(9, 1, "Receipt No: " . $receiptno, 0, 0, "C");
        $pdf->Ln(2);
        $pdf->SetFont('', '', 10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->Cell(6, 1, "Date: " . $model->datepicked, 0, 0, "L");
        $pdf->Cell(6, 1, "Details: " . $model->transaction_details, 0, 0, "L");
        $pdf->Cell(6, 1, "Term: " . Terms::model()->get($model->term_id, "term") . ".   Accounts Year: " . Years::model()->get($model->year_id, "year"), 0, 0, "C");
        $pdf->writeHTML("<hr>", true, false, false, false, '');
        //$pdf->writeHTML("<hr>", true, false, false, false, '');
        $pdf->Cell(6, 1, "Student Name: " . Pupils::model()->get($model->pupil_id, "CONCAT(fname,' ',lname,' ',Othername)"), 0, 0, "L");
        $pdf->Cell(6, 1, "Amount Paid: " . number_format($model->amount, 2), 0, 0, "C");
        $pdf->Cell(6, 1, "Class: " . Classes::model()->get(Pupils::model()->get($model->pupil_id, "class_id"), "class") . "-" . Streams::model()->get(Pupils::model()->get($model->pupil_id, "stream_id"), "stream"), 0, 0, "L");
        $pdf->Ln();
        $pdf->Cell(6, 1, "Payment Type:  " . Paymenttypes::model()->get($model->paymenttype_id, "paymenttype"), 0, 0, "L");
        if ($model->paymenttype_id != 1) {
            $pdf->Cell(6, 1, "Payment Type:  " . Banks::model()->get($model->bank_id, "bank_name"), 0, 0, "L");
            $pdf->Cell(6, 1, "Cheque/Doc #:  " . $model->cheque_no, 0, 0, "L");
        }
        $pdf->Ln();
        $pdf->Cell(18, 1, "Amount in Words: " . Towords::convert_number_to_words($model->amount), 0, 0, "C");

        $pdf->Ln();
        $pdf->Cell(9, 0, "Voteheads ", 1, 0, "C");
        $pdf->Cell(9, 0, "Amount ", 1, 0, "C");
        $pdf->SetFont('', '', 9);
        $pdf->Ln();
        $studentcharges = Studentchargestransactions::model()->findAll(array("condition" => "transaction_id=$receipt_id"));
        foreach ($studentcharges as $studentcharge) {
            $pdf->Cell(9, 0, Details::studentchargedetails($studentcharge->studentcharge_id), 1, 0, "C");
            $pdf->Cell(9, 0, number_format($studentcharge->amount, 2), 1, 0, "C");
            $pdf->Ln();
        }
        $fees = Feesvoteheads::model()->findAll(array("condition" => "fees_id IN (SELECT fees_id FROM fees_payment WHERE transaction_id=$receipt_id)"));
        foreach ($fees as $fee) {
            $pdf->Cell(9, 0, Details::getvoteheadyear($fee->voteheadyear_id), 1, 0, "C");
            $pdf->Cell(9, 0, number_format($fee->amount_charged, 2), 1, 0, "C");
            $pdf->Ln();
        }
        $pdf->SetFont('', 'B', 12);
        $pdf->Cell(9, 1, "Total", 0, 0, "C");
        $pdf->Cell(9, 1, number_format($model->amount, 2), 0, 0, "C");
        $pdf->Ln();
        $pdf->SetFont('', '', 9);
        $term_balance = FeesPayment::model()->getScalar("term_balance", "transaction_id=$receipt_id");
        $year_balance = FeesPayment::model()->getScalar("year_balance", "transaction_id=$receipt_id");
        $prepayt = $term_balance < 0 ? "(Prepayment)" : "";
        $prepayy = $year_balance < 0 ? "(Prepayment)" : "";
        $totalcharges = Studentcharges::model()->getSum("balance", "pupil_id=$model->pupil_id");
        $pdf->Cell(9, 1, "Term Balance: Ksh." . number_format($term_balance, 2) . "  " . $prepayt, 0, 0, "C");
        $pdf->Cell(9, 1, "Year Balance: Ksh." . number_format($year_balance, 2) . "  " . $prepayy, 0, 0, "C");
        $pdf->writeHTML("<hr>", true, false, false, false, '');
        $pdf->Cell(6, 1, "Any Current Unpaid Charges: Ksh." . number_format($totalcharges, 2), 0, 0, "C");
        $pdf->Cell(12, 1, "Sign:.......................................   Date Printed: " . date('Y-m-d'), 0, 0, "C");
        ob_end_clean();
        $pdf->Output(Details::receiptfromtransaction($receipt_id), "I");
    }

}
