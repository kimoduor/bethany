<?php

/**
 * Settings default controller
 * @author Joakim <kimoduor@gmail.com>
 */
class ChargestudentsController extends FeesModuleController {

    public function init() {

        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index', 'delete', 'create', 'update'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
        $this->render('index', array('model' => Pupils::model()->searchModel(array(), 10, 'pupil_id')));
    }

    public function actionCreate() {

        if (isset($_POST['pupils_id']) && count($_POST['pupils_id']) > 0) {
            foreach ($_POST['pupils_id'] as $pupil_id) {
                $error = "";
                $amount = $_POST['amount'];
                $model = new Studentcharges();
                $model->pupil_id = $pupil_id;
                $model->charge_id = $_POST['charge_id'];
                $model->amount = $amount;
                $model->balance = $amount;

                //find if the student has a prepayment
                $term_balance = FeesPayment::model()->getScalar("term_balance", "pupil_id = $pupil_id ORDER BY fees_id DESC");
                if ($term_balance < 0) {//meaning this student has something
                    $abstermbalance = abs($term_balance);
                    if ($abstermbalance >= $amount) {//meaning all our charge shall disappear---we have money in fees
                        $model->balance = 0;
                        $model->amount_charged = $amount;
                        $error1 = CActiveForm::validate($model);
                        $error_message_decoded1 = CJSON::decode($error1);
                        // var_dump($error_message_decoded4);
                        if (empty($error_message_decoded1)) {//now save in voteheads
                            $model->save();
                        } else {
                            $error = $error1;
                        }

                        //insert in charge transactions
                        $modelchargetransactions = new Studentchargestransactions();
                        $modelchargetransactions->studentcharge_id = $model->studentcharge_id;
                        $modelchargetransactions->amount = $model->amount_charged;
                        $error2 = CActiveForm::validate($modelchargetransactions);
                        $error_message_decoded2 = CJSON::decode($error2);
                        if (empty($error_message_decoded2)) {//now save in voteheads
                            $modelchargetransactions->save();
                        } else {
                            $error = $error2;
                        }
                        //update the balance
                        $fees_id = FeesPayment::model()->getScalar("fees_id", "pupil_id = $pupil_id ORDER BY fees_id DESC");
                        $updatefees = FeesPayment::model()->loadModel($fees_id);
                        $newbalance = $term_balance + $amount;
                        $updatefees->term_balance = $newbalance;
                        $updatefees->year_balance = $updatefees->year_balance + $amount;
                        $error3 = CActiveForm::validate($updatefees);
                        $error_message_decoded3 = CJSON::decode($error3);
                        if (empty($error_message_decoded3)) {//now save in voteheads
                            $updatefees->save();
                        } else {
                            $error = $error3;
                        }
                    } else {//meaning our prepyament is little to pick all charges// we dont enough prepay
                        $model->balance = $amount - $abstermbalance;
                        $model->amount_charged = $abstermbalance;
                        $error1 = CActiveForm::validate($model);
                        $error_message_decoded1 = CJSON::decode($error1);
                        // var_dump($error_message_decoded4);
                        if (empty($error_message_decoded1)) {//now save in voteheads
                            $model->save();
                        } else {
                            $error = $error1;
                        }
                        //insert in charge transactions
                        $modelchargetransactions = new Studentchargestransactions();
                        $modelchargetransactions->studentcharge_id = $model->studentcharge_id;
                        $modelchargetransactions->amount = $model->amount_charged;
                        $error2 = CActiveForm::validate($modelchargetransactions);
                        $error_message_decoded2 = CJSON::decode($error2);
                        if (empty($error_message_decoded2)) {//now save in voteheads
                            $modelchargetransactions->save();
                        } else {
                            $error = $error2;
                        }
                        //update the balance
                        $fees_id = FeesPayment::model()->getScalar("fees_id", "pupil_id = $pupil_id ORDER BY fees_id DESC");
                        $updatefees = FeesPayment::model()->loadModel($fees_id);
                        $newbalance = $term_balance + $model->amount_charged;
                        $updatefees->term_balance = $newbalance;
                        $updatefees->year_balance = $updatefees->year_balance + $model->amount_charged;
                        $error3 = CActiveForm::validate($updatefees);
                        $error_message_decoded3 = CJSON::decode($error3);
                        if (empty($error_message_decoded3)) {//now save in voteheads
                            $updatefees->save();
                        } else {
                            $error = $error3;
                        }
                    }
                } else {
                    $error21 = CActiveForm::validate($model);
                    $error_message_decoded1 = CJSON::decode($error21);
                    if (empty($error_message_decoded21)) {//now save in voteheads
                        $model->save();
                    } else {
                        $error = $error21;
                    }
                }
            }

            $decodecheck = CJSON::decode($error);
            if (empty($decodecheck)) {
                echo json_encode(true);
            } else {
                echo $error;
            }
        } else {
            echo json_encode(array('error' => Lang::t("Sorry, Your Details have no Pupils, Make sure you select the right records")));
        }
    }

    public function actionDelete() {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];

            try {
                Studentcharges::model()->loadModel($id)->delete();
                echo json_encode(true);
            } catch (Exception $e) {
                echo json_encode(array('error' => Lang::t("Sorry, This record cannot be deleted, it is being used elsewhere ")));
            }
        }
    }

}
