<?php

/**
 * Settings default controller
 * @author Joakim <kimoduor@gmail.com>
 */
class PayController extends FeesModuleController {

    public function init() {

        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index', 'delete', 'create', 'update'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
        $this->render('index', array('model' => Pupils::model()->searchModel(array(), 10, 'pupil_id')));
    }

    public function actionCreate() {
        $error = array();

        if (isset($_POST['pupils_id']) && count($_POST['pupils_id']) > 0) {
            $pupil_id = $_POST['pupils_id'];
            $feesstructure = Details::getbalancevoteheadsandstructure($pupil_id);
            if ($feesstructure != "") {
                echo json_encode(array("error" => $feesstructure));
            } else {
                $activeyear_id = Years::model()->getScalar("year_id", "active =1");
                $activeterm_id = Terms::model()->getScalar("term_id", "active =1");
// exit;
                $date_picked = $_POST['date_picked'];
                $paymenttype_id = $_POST['paymenttype_id'];
                $amount = $_POST['amount'];
                $bank_id = $_POST['bank_id'];
                $cheque_no = $_POST['cheque_no'];
//start saving in the fees transactions
                $modelfeestransaction = new FeesTransactions();
                $modelfeestransaction->pupil_id = $pupil_id;
                $modelfeestransaction->transaction_details = "Fees Payment";
                $modelfeestransaction->amount = $amount;
                $modelfeestransaction->datepicked = $date_picked;
                $modelfeestransaction->bank_id = $bank_id;
                $modelfeestransaction->cheque_no = $cheque_no;
                $modelfeestransaction->paymenttype_id = $paymenttype_id;
                $modelfeestransaction->term_id = $activeterm_id;
                $modelfeestransaction->year_id = $activeyear_id;
                $error = CActiveForm::validate($modelfeestransaction);
                $error_message_decoded = CJSON::decode($error);
                if (empty($error_message_decoded)) {//now save in fees payment
                    $modelfeestransaction->save();

//walt in the charges and see if you have something



                    $operatingamunt = $amount;
                    $availablecharges = Studentcharges::model()->findAll(array('condition' => "balance>0 AND pupil_id=" . $pupil_id));

                    if (count($availablecharges) > 0) {//meaning there are charges
                        foreach ($availablecharges as $availablecharge) {
                            if ($operatingamunt > 0) {

                                $balance = $availablecharge->balance; // balance on the charge remaining to paid
                                $modelstudentschargestransactions = new Studentchargestransactions();
                                $modelstudentschargestransactions->transaction_id = $modelfeestransaction->transaction_id;
                                $modelstudentschargestransactions->studentcharge_id = $availablecharge->studentcharge_id;
                                $modelstudentchargesupdate = Studentcharges::model()->loadModel($availablecharge->studentcharge_id);

                                if ($operatingamunt >= $balance) {//meanign you have more money than the votehead/handle it and move next charge
                                    $modelstudentschargestransactions->amount = $balance;
                                    $modelstudentschargestransactions->save();
//update your charges
                                    $modelstudentchargesupdate->amount_charged = $modelstudentchargesupdate->amount_charged + $balance;
                                    $modelstudentchargesupdate->balance = 0;
                                    $modelstudentchargesupdate->save();
                                    $operatingamunt = $operatingamunt - $balance;
                                } else {//meaning the charges are morw than what we have at hand, deduct what you have and update the main charge
                                    $modelstudentschargestransactions->amount = $operatingamunt; //charged the whole of it
                                    $modelstudentschargestransactions->save();
//update your charges
                                    $balanceforcharge = $balance - $operatingamunt;
                                    $modelstudentchargesupdate->amount_charged = $modelstudentchargesupdate->amount_charged + $operatingamunt;
                                    $modelstudentchargesupdate->balance = $balanceforcharge;
                                    $modelstudentchargesupdate->save();
                                    $operatingamunt = 0;
                                }
                            }
                        }
                    }
//save in the mega fees table
//I am done with the charges$operatingamunt

                    if ($operatingamunt > 0) {
                        $modelfeespayment = new FeesPayment();
                        $term_balance = FeesPayment::model()->getScalar("term_balance", "pupil_id = $pupil_id ORDER BY fees_id DESC");
                        $year_balance = FeesPayment::model()->getScalar("year_balance", "pupil_id = $pupil_id ORDER BY fees_id DESC");
                        $modelfeespayment->amount = $operatingamunt;
                        $modelfeespayment->pupil_id = $pupil_id;
                        $modelfeespayment->transaction_id = $modelfeestransaction->transaction_id;
                        $modelfeespayment->term_balance = $term_balance - $operatingamunt;
                        $modelfeespayment->year_balance = $year_balance - $operatingamunt;
                        //$modelfeespayment->date_picked = $date_picked;
                        $modelfeespayment->term_id = $activeterm_id;
                        $modelfeespayment->year_id = $activeyear_id;
                        // var_dump($modelfeespayment);
                        $error2 = CActiveForm::validate($modelfeespayment);
                        $error_message_decoded2 = CJSON::decode($error2);
                        if (empty($error_message_decoded2)) {//now save in fees payment
                            $modelfeespayment->save();
                        } else {
                            $error = $error2;
                        }

//start cutting the voteheads
//get the date picked'                  
//get the fees structure
//get all  voteheads that year
//any pnding votehead for the student in the feesvoteheads
                        $feesvoteheadyears = Feesvoteheads::model()->findAll(array('condition' => "balance>0 AND repeater=1"
                            . " AND fees_id IN (SELECT fees_id FROM fees_payment WHERE pupil_id=$pupil_id)"));
                        if (count($feesvoteheadyears)) {
                            foreach ($feesvoteheadyears as $feesvoteheadyear) {
                                if ($operatingamunt > 0) {
                                    $modelfeesvoteheads = new Feesvoteheads();
                                    $modelfeesvoteheads->voteheadyear_id = $feesvoteheadyear->voteheadyear_id;
                                    $modelfeesvoteheads->fees_id = $modelfeespayment->fees_id;
                                    $modelupdatepath = Feesvoteheads::model()->loadModel($feesvoteheadyear->feesvoteheads_id);
                                    $modelupdatepath->repeater = 0;
                                    $modelupdatepath->save();
                                    if ($operatingamunt >= $feesvoteheadyear->balance) {//meaning there shall be no repeater
                                        $modelfeesvoteheads->amount_charged = $feesvoteheadyear->balance;
                                        $modelfeesvoteheads->balance = 0;
                                        $modelfeesvoteheads->repeater = 0;
                                        $operatingamunt = $operatingamunt - $feesvoteheadyear->balance;
                                    } else {//meaning the money we have cant still finish the balance
                                        $modelfeesvoteheads->amount_charged = $operatingamunt;
                                        $modelfeesvoteheads->balance = $feesvoteheadyear->balance - $operatingamunt;
                                        $modelfeesvoteheads->repeater = 1;
                                        $operatingamunt = 0;
                                    }
                                    $error3 = CActiveForm::validate($modelfeesvoteheads);
                                    $error_message_decoded3 = CJSON::decode($error3);
                                    if (empty($error_message_decoded3)) {//now save in fees payment
                                        $modelfeesvoteheads->save();
                                    } else {
                                        $error = $error3;
                                    }
                                }
                            }
                        }

                        if ($operatingamunt > 0) {//meaning we want to start to work on the voteheads
                            //get the present class of the pupil then use the voteheads
                            $modelpupils = Pupils::model()->loadModel($pupil_id);
                            $allvoteheads = Voteheadyear::model()->findAll(array("condition" => "year_id=$activeyear_id AND "
                                . " class_id=$modelpupils->class_id AND voteheadyear_id NOT IN (SELECT voteheadyear_id FROM feesvoteheads WHERE fees_id "
                                . " IN (SELECT fees_id FROM fees_payment WHERE pupil_id=$pupil_id)) ORDER BY priority ASC"));

                            foreach ($allvoteheads as $allvotehead) {
                                if ($operatingamunt > 0) {
                                    $modelfeesvoteheads = new Feesvoteheads();
                                    $modelfeesvoteheads->voteheadyear_id = $allvotehead->voteheadyear_id;
                                    $modelfeesvoteheads->fees_id = $modelfeespayment->fees_id;
                                    if ($operatingamunt >= $allvotehead->amount) {//meaning there shall be no repeat
                                        $modelfeesvoteheads->amount_charged = $allvotehead->amount;
                                        $modelfeesvoteheads->balance = 0;
                                        $modelfeesvoteheads->repeater = 0;
                                        $operatingamunt = $operatingamunt - $allvotehead->amount;
                                    } else {//there shall be a repeater
                                        $modelfeesvoteheads->amount_charged = $operatingamunt;
                                        $modelfeesvoteheads->balance = $allvotehead->amount - $operatingamunt;
                                        $modelfeesvoteheads->repeater = 1;
                                        $operatingamunt = 0;
                                    }
                                    $error4 = CActiveForm::validate($modelfeesvoteheads);
                                    $error_message_decoded4 = CJSON::decode($error4);
                                    // var_dump($error_message_decoded4);
                                    if (empty($error_message_decoded4)) {//now save in voteheads
                                        $modelfeesvoteheads->save();
                                    } else {
                                        $error = $error4;
                                    }
                                }
                            }//am done with all voteheadds make sure that any prepay is recorded
                            if ($operatingamunt > 0) {
                                $modelprepay = new Feesvoteheads();
                                $modelprepay->voteheadyear_id = 1;
                                $modelprepay->fees_id = $modelfeespayment->fees_id;
                                $modelprepay->amount_charged = $operatingamunt;
                                $modelprepay->balance = 0;
                                $modelprepay->repeater = 1;
                                $modelprepay->save();
                            }
                        }
                    }
                    $decodecheck = CJSON::decode($error);
                    if (empty($decodecheck)) {
                        echo json_encode("qq".$modelfeestransaction->transaction_id);
                    } else {
                        echo $error;
                    }
                } else {
                    echo json_encode("qq".$modelfeestransaction->transaction_id);
                }
            }
        }
    }

}
