<?php

/**
 * Settings default controller
 * @author Joakim <kimoduor@gmail.com>
 */
class FeesstructureController extends SettingsModuleController {

    public function init() {

        $this->resourceLabel = Lang::t('FeesStructure');
        // $this->resource = DistributorsModuleConstants::RES_DISTIBUTORS;
        $this->activeTab = 'FeesStructure';
        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index', 'create', 'update', 'delete', 'data'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
        $this->pageTitle = Lang::t('FeesStructure');

        $model = new FeesStructure();
        $model_class_name = get_class($model);
        //$this->render('index', array('model' => $model));
        $condition = '';
       $this->render('index', array('model' => FeesStructure::model()->searchModel(array(), 10, 'year_id', $condition)));
    }

    public function actionCreate() {
        if (isset($_POST)) {
            $model = new FeesStructure();
            $model->year_id = $_POST['year_id'];
            $model->term_id = $_POST['term_id'];
            $model->class_id = $_POST['class_id'];
            $model->amount = $_POST['amount'];
            $error = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error);
            if (empty($error_message_decoded)) {
                $model->save();
                echo json_encode(true);
            } else {
                echo $error;
            }
        }
    }

    public function actionUpdate() {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];
            $model = FeesStructure::model()->loadModel($id);
            $model->year_id = $_POST['year_id'];
            $model->term_id = $_POST['term_id'];
            $model->class_id = $_POST['class_id'];
            $model->amount = $_POST['amount'];
            $error = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error);
            if (empty($error_message_decoded)) {
                $model->save();
                echo json_encode(true);
            } else {
                echo $error;
            }
        }
    }

    public function actionDelete() {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];

            try {
                FeesStructure::model()->loadModel($id)->delete();
                Yii::app()->user->setFlash('success', Lang::t("Successfully Deleted"));
            } catch (Exception $e) {
                Yii::app()->user->setFlash('error', Lang::t("Sorry, This record cannot be deleted, it is being used elsewhere "));
            }
        }
    }

}
