<style>
    #tr_bank_id,#tr_cheque_no{
        display: none;
    }
</style>
<script>
    $.jgrid.defaults.width = 1200;
    $.jgrid.defaults.responsive = true;
    $.jgrid.defaults.styleUI = 'Bootstrap';</script>
<div style="margin-left:20px">
    <table id="jqGrid"></table>
    <div id="jqGridPager"></div>
</div>
<script type="text/javascript">

    $(document).ready(function() {
        $.jgrid.styleUI.Bootstrap.base.rowTable = "table table-bordered table-striped";
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var template = "<div style='margin-left:15px;'>";
        template += "<div> First Name: </div><div>{fname} </div>";
        template += "<hr style='width:100%;'/>";
        template += "<div> {sData} {cData}  </div></div>";
        $("#jqGrid").jqGrid({
            url: 'data/feesbalances',
            datatype: "local",
            rownumbers: true,
            toppager: true, cloneToTop: true,
            colModel: [
                {
                    label: 'pupil',
                    name: 'pupil_id',
                    width: 150,
                    editable: false,
                    hidden: true,
                    key: true,
                    search: true,
                },
                {label: 'Admission No',
                    name: 'adm',
                    editable: false,
                    width: 150,
                },
                {label: 'Name',
                    name: 'name',
                    editable: false,
                    width: 200,
                },
                {label: 'Class',
                    name: 'class_id',
                    editable: false,
                    edittype: 'select',
                    formatter: 'select',
                    editoptions: {value:<?php echo Details::classjson(); ?>},
                    stype: 'select',
                    searchoptions: {value:<?php echo Details::classjson(); ?>},
                    width: 120,
                },
                {label: 'Stream',
                    name: 'stream_id',
                    editable: false,
                    edittype: 'select',
                    formatter: 'select',
                    editoptions: {value:<?php echo Details::streamjson(); ?>},
                    stype: 'select',
                    searchoptions: {value:<?php echo Details::streamjson(); ?>},
                    width: 120,
                },
                {label: 'Last Paid',
                    name: 'lastpaid',
                    width: 200,
                    editable: false,
                    editrules: {required: true, edithidden: true},
                    editoptions: {dataInit: function(elem) {
                            $(elem).datepicker({autoclose: true,
                                changeMonth: true, changeYear: true,
                                defaultDate: new Date(),
                                format: 'yyyy-mm-dd'});
                        }},
                },
                {label: 'Actual Fees Balance ',
                    name: 'actualbalance',
                    editable: true,
                    width: 150,
                },
                {label: 'Other Charges ',
                    name: 'othercharges',
                    editable: false,
                    width: 150,
                },
                {label: 'Total Fees Balance',
                    name: 'total',
                    editable: false,
                    width: 150,
                },
            ],
            sortname: 'class_id',
            sortorder: 'asc',
            loadonce: true,
            viewrecords: true,
            width: 1000,
            height: 400,
            rowNum: 100000000000000,
            pager: "#jqGridPager",
            cellEdit: true,
            cellsubmit: 'remote',
            cellurl: 'fees/feesbalances/adjustbalance',
            subGrid: true,
             afterSubmitCell: function(data, id, cellname, value, iRow, iCol) {
                $("#jqGrid").setGridParam({datatype: 'json'}).trigger('reloadGrid');
            },
            navOptions: {reloadGridOptions: {fromServer: true}},
            // define the icons in subgrid
            subGridRowExpanded: function(subgrid_id, row_id) {
                var subgrid_table_id, pager_id;
                subgrid_table_id = subgrid_id + "_t";
                pager_id = "p_" + subgrid_table_id;
                var rowData = $("#jqGrid").getRowData(row_id);
                var pupil_id = rowData['pupil_id'];
                $("#" + subgrid_id).html("<table id='" + subgrid_table_id + "' class='scroll'></table><div id='" + pager_id + "' class='scroll'></div>");
                jQuery("#" + subgrid_table_id).jqGrid({
                    url: 'data/feesstatement/?pupil_id=' + pupil_id,
                    datatype: "json",
                    colModel: [
                        {
                            label: 'Date Paid',
                            name: 'datecreated',
                            width: 200,
                        }, {label: 'Year',
                            name: 'year_id',
                            editable: true,
                            edittype: 'select',
                            formatter: 'select',
                            editoptions: {value:<?php echo Details::yearjson(); ?>},
                            stype: 'select',
                            searchoptions: {value:<?php echo Details::yearjson(); ?>},
                            width: 200,
                        },
                        {label: 'Term',
                            name: 'term_id',
                            editable: true,
                            edittype: 'select',
                            formatter: 'select',
                            editoptions: {value:<?php echo Details::termjson(); ?>},
                            stype: 'select',
                            searchoptions: {value:<?php echo Details::termjson(); ?>},
                            width: 200,
                        },
                        {label: 'Amount Paid',
                            name: 'amount',
                            editable: true,
                            width: 245,
                        },
                        {label: 'Payment Type',
                            name: 'paymenttype_id',
                            editable: true,
                            edittype: 'select',
                            formatter: 'select',
                            editoptions: {value:<?php echo Details::paymenttypesjson(); ?>},
                            stype: 'select',
                            searchoptions: {value:<?php echo Details::paymenttypesjson(); ?>},
                            width: 200,
                        },
                        {label: 'Bank',
                            name: 'bank_id',
                            editable: true,
                            edittype: 'select',
                            formatter: 'select',
                            editoptions: {value:<?php echo Details::banksjson(); ?>},
                            stype: 'select',
                            searchoptions: {value:<?php echo Details::banksjson(); ?>},
                            width: 200,
                        },
                        {
                            label: 'Voteheads Amount',
                            name: 'voteheadsfees',
                            width: 200,
                        },
                        {
                            label: 'Amount Deducted',
                            name: 'deductions',
                            width: 200,
                        },
                        {
                            label: 'Termly Balance',
                            name: 'term_balance',
                            width: 200,
                        },
                        {
                            label: 'Year Balance',
                            name: 'year_balance',
                            width: 200,
                        }
                    ],
                    pager: pager_id,
                    caption: "Fees Statement for the Pupil",
                    sortname: 'transaction_id',
                    width: '900',
                    rowNum: 20,
                    loadonce: true,
                    sortorder: "desc",
                    height: '100%'
                });
                jQuery("#" + subgrid_table_id).jqGrid('navGrid', "#" + pager_id, {edit: false, add: false, del: false});
            },
        }).setGridParam({datatype: 'json'}).trigger('reloadGrid');
        $('#jqGrid').navGrid('#jqGridPager',
                {edit: false, add: false, del: false, search: true, refresh: true, view: true, position: "left", cloneToTop: true},
        {url: "fees/charges/update", afterSubmit: processAddEdit, closeAfterEdit: true},
        {url: "fees/charges/create", afterSubmit: processAddEdit, closeAfterAdd: true},
        {url: "fees/charges/delete"},
        {// reload
            reloadAfterSubmit: true
        },
        // options for the Edit Dialog
        {
            editCaption: "The Edit Dialog",
            template: template,
            errorTextFormat: function(data) {
                return 'Error: ' + data.responseText
            }
        },
        // options for the Add Dialog
        {
            editCaption: "Pay Fees for the Student",
            template: template,
            errorTextFormat: function(data) {
                return 'Error: ' + data.responseText
            }
        },
        // options for the Delete Dailog
        {
            errorTextFormat: function(data) {
                return 'Error: ' + data.responseText
            },
        });
        $('#jqGrid').jqGrid('filterToolbar', {
            // JSON stringify all data from search, including search toolbar operators
            stringResult: true,
            // instuct the grid toolbar to show the search options
            searchOperators: false,
        });
        $("#jqGrid").jqGrid('navButtonAdd',
                '#jqGrid_toppager_left',
                {caption: " Export Balances",
                    buttonicon: 'glyphicon glyphicon-list',
                    id: "btnAddNewItem",
                    onClickButton: function() {
                        exportData("jqGrid", "Fees Balance as at " + new Date());
                    }
                });
    });


</script>
