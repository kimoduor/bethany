<style>
    #tr_bank_id,#tr_cheque_no{
        display: none;
    }
</style>
<script>
    $.jgrid.defaults.width = 1200;
    $.jgrid.defaults.responsive = true;
    $.jgrid.defaults.styleUI = 'Bootstrap';</script>
<div style="margin-left:20px">
    <table id="jqGrid"></table>
    <div id="jqGridPager"></div>
</div>
<script type="text/javascript">

    $(document).ready(function() {
        $.jgrid.styleUI.Bootstrap.base.rowTable = "table table-bordered table-striped";
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var template = "<div style='margin-left:15px;'>";
        template += "<div> First Name: </div><div>{fname} </div>";
        template += "<hr style='width:100%;'/>";
        template += "<div> {sData} {cData}  </div></div>";
        $("#jqGrid").jqGrid({
            url: 'data/pupils',
            datatype: "local",
            rownumbers: true,
            toppager: true, cloneToTop: true,
            colModel: [
                {
                    label: '#',
                    name: 'pupil_id',
                    hidden: true,
                    width: 8,
                    key: true,
                    search: false,
                },
                {label: 'Class',
                    name: 'class_id',
                    editable: false,
                    edittype: 'select',
                    formatter: 'select',
                    editoptions: {value:<?php echo Details::classjson(); ?>},
                    stype: 'select',
                    searchoptions: {value:<?php echo Details::classjson(); ?>},
                    width: 200,
                },
                {label: 'Stream',
                    name: 'stream_id',
                    editable: false,
                    edittype: 'select',
                    formatter: 'select',
                    editoptions: {value:<?php echo Details::streamjson(); ?>},
                    stype: 'select',
                    searchoptions: {value:<?php echo Details::streamjson(); ?>},
                    width: 200,
                },
                {label: 'Admission No',
                    name: 'adm',
                    editable: false,
                    width: 245,
                },
                {label: 'Name',
                    name: 'name',
                    editable: false,
                    width: 245,
                },
                {
                    label: 'Parent Name',
                    name: 'parentname',
                    editable: false,
                    width: 245,
                },
                {
                    label: 'Uses Bus?',
                    name: 'usebus',
                    editable: false,
                    width: 170,
                    search: false,
                    edittype: 'checkbox',
                    formatter: 'checkbox',
                    editoptions: {value: "1:0"},
                },
                {
                    label: 'Date',
                    name: 'date_picked',
                    width: 100,
                    hidden:true,
                    editable: true,
                    editrules: {required: true,edithidden: true},
                    editoptions: {dataInit: function(elem) {
                            $(elem).datepicker({autoclose: true,
                                changeMonth: true, changeYear: true,
                                defaultDate: new Date(),
                                format: 'yyyy-mm-dd'});
                        }}
                },
                //start all form details
                {label: 'Payment Types',
                    name: 'paymenttype_id',
                    editable: true,
                    hidden: true,
                    edittype: 'select',
                    formatter: 'select',
                    editoptions: {value:<?php echo Details::paymenttypesjson(); ?>,
                        dataEvents: [{type: 'change', fn: function(e) {
                                    closecawhencash($(this).val());
                                }}]},
                    stype: 'select',
                    editrules: {edithidden: true, required: true},
                    searchoptions: {value:<?php echo Details::paymenttypesjson(); ?>},
                    width: 200
                },
                {label: 'Bank',
                    name: 'bank_id',
                    editable: true,
                    hidden: true,
                    edittype: 'select',
                    formatter: 'select',
                    editoptions: {value:<?php echo Details::banksjson(); ?>},
                    stype: 'select',
                    editrules: {edithidden: true},
                    searchoptions: {value:<?php echo Details::banksjson(); ?>},
                    width: 200,
                },
                {label: 'Cheque/Doc Number',
                    name: 'cheque_no',
                    hidden: true,
                    editrules: {edithidden: true},
                    editable: true,
                    width: 245,
                },
                {label: 'Amount',
                    name: 'amount',
                    hidden: true,
                    editrules: {edithidden: true, required: true},
                    editable: true,
                    width: 245,
                },
            ],
            sortname: 'pupil_id',
            sortorder: 'asc',
            loadonce: true,
            multiselect: false,
            viewrecords: true,
            width: 1000,
            height: 400,
            rowNum: 20,
            pager: "#jqGridPager",
            navOptions: {reloadGridOptions: {fromServer: true}},
        }).setGridParam({datatype: 'json'}).trigger('reloadGrid');
        $('#jqGrid').navGrid('#jqGridPager',
                {edit: false, add: false, del: false, search: true, refresh: true, view: true, position: "left", cloneToTop: true},
        {url: "fees/charges/update", afterSubmit: processAddEdit, closeAfterEdit: true},
        {url: "fees/charges/create", afterSubmit: processAddEdit, closeAfterAdd: true},
        {url: "fees/charges/delete"},
        {// reload
            reloadAfterSubmit: true
        },
        // options for the Edit Dialog
        {
            editCaption: "The Edit Dialog",
            template: template,
            errorTextFormat: function(data) {
                return 'Error: ' + data.responseText
            }
        },
        // options for the Add Dialog
        {
            editCaption: "Pay Fees for the Student",
            template: template,
            errorTextFormat: function(data) {
                return 'Error: ' + data.responseText
            }
        },
        // options for the Delete Dailog
        {
            errorTextFormat: function(data) {
                return 'Error: ' + data.responseText
            },
        });
        $('#jqGrid').jqGrid('filterToolbar', {
            // JSON stringify all data from search, including search toolbar operators
            stringResult: true,
            // instuct the grid toolbar to show the search options
            searchOperators: false,
        });
        $("#jqGrid").jqGrid('navButtonAdd',
                '#jqGrid_toppager_left',
                {caption: " Pay",
                    buttonicon: 'glyphicon glyphicon-ok',
                    id: "btnAddNewItem",
                    addCaption: "Fees Payment for Students",
                    onClickButton: function()
                    {
                        getSelectedRows();
                    }
                });


    });

    function getSelectedRows() {
        var grid = $("#jqGrid");
        var rowKey = grid.getGridParam("selrow");

        if (!rowKey) {
            //  alert("No rows are selected");
            $.jgrid.info_dialog.call(this,
                    "<i class='fa fa-exclamation-triangle'></i>  Warning", // dialog title
                    "Please, select The Students to Pay fees" // text inside of dialog
                    );
        }
        else {
           var pupil_id =$("#jqGrid").jqGrid('getCell', rowKey, 'pupil_id');
            $("#jqGrid").jqGrid('editGridRow', "new", {addCaption: "Pay Fees for the Student",
                url: "fees/pay/create",
                afterSubmit: processAddEdit,
                closeAfterAdd: true,
                closeAfterEdit: true,
                editData: {"pupils_id": pupil_id}
            });
        }
    }

    function closecawhencash(id) {
        if (id == 1) {
            $('#tr_bank_id').hide();
            $('#tr_cheque_no').hide();
        }
        else {
            $('#tr_bank_id').show();
            $('#tr_cheque_no').show();
        }
    }


</script>

