<style>
    #tr_bank_id,#tr_cheque_no{
        display: none;
    }
</style>
<script>
    $.jgrid.defaults.width = 1200;
    $.jgrid.defaults.responsive = true;
    $.jgrid.defaults.styleUI = 'Bootstrap';</script>
<div style="margin-left:20px">
    <table id="jqGrid"></table>
    <div id="jqGridPager"></div>
</div>
<script type="text/javascript">

    $(document).ready(function() {
        $.jgrid.styleUI.Bootstrap.base.rowTable = "table table-bordered table-striped";
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var template = "<div style='margin-left:15px;'>";
        template += "<div> Details: </div><div>{details} </div>";
        template += "<hr style='width:100%;'/>";
        template += "<div> {sData} {cData}  </div></div>";
        $("#jqGrid").jqGrid({
            url: 'data/expenses',
            datatype: "local",
            rownumbers: true,
            toppager: true, cloneToTop: true,
            colModel: [
                {
                    label: '#',
                    name: 'expense_id',
                    hidden: true,
                    width: 8,
                    key: true,
                    search: false,
                },
                 {label: 'Expense  Type',
                    name: 'expensetype_id',
                    editable: true,
                    edittype: 'select',
                    formatter: 'select',
                    editoptions: {value:<?php echo Details::expensetypejson(); ?>},
                    stype: 'select',
                    searchoptions: {value:<?php echo Details::expensetypejson(); ?>},
                    width: 200
                },
                {label: 'Receiver',
                    name: 'toname',
                    editable: true,
                    width: 245,
                    summaryType: 'sum',
                    summaryTpl: "<b>Total </b>",
                },
                {
                    label: 'Date',
                    name: 'date',
                    width: 250,
                    editable: true,
                    editrules: {required: true, edithidden: true},
                    editoptions: {dataInit: function(elem) {
                            $(elem).datepicker({autoclose: true,
                                changeMonth: true, changeYear: true,
                                defaultDate: new Date(),
                                format: 'yyyy-mm-dd'});
                        }}
                },
                {label: 'Payment Types',
                    name: 'paymenttype_id',
                    editable: true,
                    edittype: 'select',
                    formatter: 'select',
                    editoptions: {value:<?php echo Details::paymenttypesjson(); ?>,
                        dataEvents: [{type: 'change', fn: function(e) {
                                    closecawhencash($(this).val());
                                }}]},
                    stype: 'select',
                    searchoptions: {value:<?php echo Details::paymenttypesjson(); ?>},
                    width: 200
                },
                        
               
                {label: 'Bank',
                    name: 'bank_id',
                    editable: true,
                    edittype: 'select',
                    formatter: 'select',
                    editoptions: {value:<?php echo Details::banksjson(); ?>},
                    stype: 'select',
                    searchoptions: {value:<?php echo Details::banksjson(); ?>},
                    width: 200,
                },
                //start all form details
                {label: 'Cheque/Doc Number',
                    name: 'cheque_no',
                    editable: true,
                    width: 100,
                },
                {label: 'Amount',
                    name: 'amount',
                    summaryType: 'sum',
                    summaryTpl: "<b>{0}</b>",
                    editable: true,
                    formatter: "number",
                    width: 145,
                },
                {label: 'Details',
                    name: 'details',
                    search:false,
                    editable: true,
                    edittype: 'textarea',
                    editoptions: {rows: "2", cols: "30"},
                    width: 200,
                },
                {label: 'Served by',
                    name: 'user_id',
                    editable: false,
                    edittype: 'select',
                    formatter: 'select',
                    editoptions: {value:<?php echo Details::teachersjson(); ?>},
//                    search: false,
//                    width: 200,
                },
            ],
            sortname: 'date',
            sortorder: 'desc',
            loadonce: true,
            viewrecords: true,
            width: 1000,
            height: 400,
            rowNum: 20,
            caption: " Manage Expenses",
            pager: "#jqGridPager",
            grouping: true,
            groupingView: {
                groupField: ['user_id'],
                groupSummary: [true],
                groupColumnShow: [true],
                groupText: ['<strong>{0}</strong>'],
                groupCollapse: false,
                groupOrder: ['asc']
            },
            navOptions: {reloadGridOptions: {fromServer: true}},
            // define the icons in subgrid,
        }).setGridParam({datatype: 'json'}).trigger('reloadGrid');
        $('#jqGrid').navGrid('#jqGridPager',
                {edit: true, add: true, del: true, search: true, refresh: true, view: true, position: "left", cloneToTop: true},
        {url: "fees/expenses/update", afterSubmit: processAddEdit, closeAfterEdit: true},
        {url: "fees/expenses/create", afterSubmit: processAddEdit, closeAfterAdd: true},
        {url: "fees/expenses/delete"},
        {// reload
            reloadAfterSubmit: true
        },
        // options for the Edit Dialog
        {
            editCaption: "The Edit Dialog",
            template: template,
            errorTextFormat: function(data) {
                return 'Error: ' + data.responseText
            }
        },
        // options for the Add Dialog
        {
            addCaption: "Pay Fees for the Student",
            template: template,
            errorTextFormat: function(data) {
                return 'Error: ' + data.responseText
            }
        },
        // options for the Delete Dailog
        {
            errorTextFormat: function(data) {
                return 'Error: ' + data.responseText
            },
        });
        $('#jqGrid').jqGrid('filterToolbar', {
            // JSON stringify all data from search, including search toolbar operators
            stringResult: true,
            // instuct the grid toolbar to show the search options
            searchOperators: false,
        });

    });


    function closecawhencash(id) {
        if (id == 1) {
            $('#tr_bank_id').hide();
            $('#tr_cheque_no').hide();
        }
        else {
            $('#tr_bank_id').show();
            $('#tr_cheque_no').show();
        }
    }


</script>
