<style>
    #tr_bank_id,#tr_cheque_no{
        display: none;
    }
</style>
<script>
    $.jgrid.defaults.width = 1200;
    $.jgrid.defaults.responsive = true;
    $.jgrid.defaults.styleUI = 'Bootstrap';</script>
<div style="margin-left:20px">
    <table id="jqGrid"></table>
    <div id="jqGridPager"></div>
</div>
<script type="text/javascript">

    $(document).ready(function() {
        $.jgrid.styleUI.Bootstrap.base.rowTable = "table table-bordered table-striped";
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var template = "<div style='margin-left:15px;'>";
        template += "<div> First Name: </div><div>{fname} </div>";
        template += "<hr style='width:100%;'/>";
        template += "<div> {sData} {cData}  </div></div>";
        $("#jqGrid").jqGrid({
            url: 'data/receipts',
            datatype: "local",
            rownumbers: true,
            toppager: true, cloneToTop: true,
            colModel: [
                {
                    label: '#',
                    name: 'receipt_id',
                    width: 150,
                     editable: false,
                    hidden:true,
                    key: true,
                    search: true,
                },
                {
                    label: 'Receipt Number',
                    name: 'receipt_no',
                    width: 150,
                      summaryType: 'sum',
                    summaryTpl: "<b>Total </b>",
                    search: true,
                },
                {label: 'Admission No',
                    name: 'adm',
                    editable: false,
                    width: 150,
                },
                {label: 'Name',
                    name: 'name',
                    editable: false,
                    width: 200,
                },
                {label: 'Class',
                    name: 'class_id',
                    editable: false,
                    edittype: 'select',
                    formatter: 'select',
                    editoptions: {value:<?php echo Details::classjson(); ?>},
                    stype: 'select',
                    searchoptions: {value:<?php echo Details::classjson(); ?>},
                    width: 120,
                },
                {label: 'Stream',
                    name: 'stream_id',
                    editable: false,
                    edittype: 'select',
                    formatter: 'select',
                    editoptions: {value:<?php echo Details::streamjson(); ?>},
                    stype: 'select',
                    searchoptions: {value:<?php echo Details::streamjson(); ?>},
                    width: 120,
                },
                {
                    label: 'Date',
                    name: 'date',
                    width: 200,
                    editable: true,
                    editrules: {required: true, edithidden: true},
                    editoptions: {dataInit: function(elem) {
                            $(elem).datepicker({autoclose: true,
                                changeMonth: true, changeYear: true,
                                defaultDate: new Date(),
                                format: 'yyyy-mm-dd'});
                        }},
                    searchoptions: {sopt: ["ge", "le", "eq"]}
                },
                //start all form details
                {label: 'Payment Types',
                    name: 'paymenttype_id',
                    editable: true,
                    edittype: 'select',
                    formatter: 'select',
                    editoptions: {value:<?php echo Details::paymenttypesjson(); ?>},
                    stype: 'select',
                    searchoptions: {value:<?php echo Details::paymenttypesjson(); ?>},
                    width: 200
                },
                {label: 'Bank',
                    name: 'bank_id',
                    editable: true,
                    edittype: 'select',
                    formatter: 'select',
                    editoptions: {value:<?php echo Details::banksjson(); ?>},
                    stype: 'select',
                    editrules: {edithidden: true},
                    searchoptions: {value:<?php echo Details::banksjson(); ?>},
                    width: 200,
                },
                {label: 'Cheque/Doc Number',
                    name: 'cheque_no',
                    editable: true,
                    width: 150,
                },
                {label: 'Amount',
                    name: 'amount',
                    editable: true,
                    summaryType: 'sum',
                    formatter: "number",
                    summaryTpl: "<b>{0}</b>",
                    width: 150,
                },
            ],
            sortname: 'date',
            sortorder: 'desc',
            loadonce: true,
            multiselect: false,
            viewrecords: true,
            width: 1000,
            height: 400,
            rowNum: 20,
            pager: "#jqGridPager",
            subGrid: false,
             grouping: true,
            groupingView: {
                groupField: ['date'],
                groupSummary: [true],
                groupColumnShow: [true],
                groupText: ['<strong>{0}</strong>'],
                groupCollapse: false,
                groupOrder: ['asc']
            },
            navOptions: {reloadGridOptions: {fromServer: true}},
            // define the icons in subgrid
//            subGridRowExpanded: function(subgrid_id, row_id) {
//                var subgrid_table_id, pager_id;
//                subgrid_table_id = subgrid_id + "_t";
//                pager_id = "p_" + subgrid_table_id;
//                var rowData = $("#jqGrid").getRowData(row_id);
//                var receipt_id = rowData['receipt_id'];
//                $("#" + subgrid_id).html("<table id='" + subgrid_table_id + "' class='scroll'></table><div id='" + pager_id + "' class='scroll'></div>");
//                jQuery("#" + subgrid_table_id).jqGrid({
//                    url: 'data/receiptdetails/?subgridtransaction=' + receipt_id,
//                    datatype: "json",
//                    colModel: [
//                        {label: '#',
//                            name: 'voteheadyear_id',
//                            hidden: true,
//                            width: 8,
//                            key: true,
//                            search: false,
//                            editable: false,
//                        },
//                        {label: 'Votehead',
//                            name: 'votehead',
//                            editable: true,
//                            width: 245,
//                        },
//                        {
//                            label: 'Amount',
//                            name: 'amount_charged',
//                            width: 200,
//                        }
//                    ],
//                    rowNum: 20,
//                    pager: pager_id,
//                    caption: "Fees Payment",
//                    sortname: 'pupil_id',
//                    width: '900',
//                    loadonce: true,
//                    sortorder: "asc",
//                    height: '100%'
//                });
//                jQuery("#" + subgrid_table_id).jqGrid('navGrid', "#" + pager_id, {edit: false, add: false, del: false})
//            },
        }).setGridParam({datatype: 'json'}).trigger('reloadGrid');
        $('#jqGrid').navGrid('#jqGridPager',
                {edit: false, add: false, del: false, search: true, refresh: true, view: true, position: "left", cloneToTop: true},
        {url: "fees/charges/update", afterSubmit: processAddEdit, closeAfterEdit: true},
        {url: "fees/charges/create", afterSubmit: processAddEdit, closeAfterAdd: true},
        {url: "fees/charges/delete"},
        {// reload
            reloadAfterSubmit: true
        },
        // options for the Edit Dialog
        {
            editCaption: "The Edit Dialog",
            template: template,
            errorTextFormat: function(data) {
                return 'Error: ' + data.responseText
            }
        },
        // options for the Add Dialog
        {
            editCaption: "Pay Fees for the Student",
            template: template,
            errorTextFormat: function(data) {
                return 'Error: ' + data.responseText
            }
        },
        // options for the Delete Dailog
        {
            errorTextFormat: function(data) {
                return 'Error: ' + data.responseText
            },
        });
        $('#jqGrid').jqGrid('filterToolbar', {
            // JSON stringify all data from search, including search toolbar operators
            stringResult: true,
            // instuct the grid toolbar to show the search options
            searchOperators: false,
        });
        $("#jqGrid").jqGrid('navButtonAdd',
                '#jqGrid_toppager_left',
                {caption: " Print Receipt",
                    buttonicon: 'glyphicon glyphicon-print',
                    id: "btnAddNewItem",
                    addCaption: "Fees Payment for Students",
                    onClickButton: function()
                    {
                        getSelectedRows();
                    }
                });


    });

    function getSelectedRows() {
        var grid = $("#jqGrid");
        var rowKey = grid.getGridParam("selrow");

        if (!rowKey) {
            //  alert("No rows are selected");
            $.jgrid.info_dialog.call(this,
                    "<i class='fa fa-exclamation-triangle'></i>  Warning", // dialog title
                    "Please, select The Receipt to Print fees" // text inside of dialog
                    );
        }
        else {
            var receipt_id = $("#jqGrid").jqGrid('getCell', rowKey, 'receipt_id');
            window.open('fees/receipts/printreceipt/?receipt_id=' + receipt_id, '_blank');


        }
    }




</script>

