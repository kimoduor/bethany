<script>
    $.jgrid.defaults.width = 1200;
    $.jgrid.defaults.responsive = true;
    $.jgrid.defaults.styleUI = 'Bootstrap';</script>
<div style="margin-left:20px">
    <table id="jqGrid"></table>
    <div id="jqGridPager"></div>
</div>
<script type="text/javascript">

    $(document).ready(function() {
        $.jgrid.styleUI.Bootstrap.base.rowTable = "table table-bordered table-striped";
        var template = "<div style='margin-left:15px;'>";
        template += "<div> First Name: </div><div>{fname} </div>";
        template += "<hr style='width:100%;'/>";
        template += "<div> {sData} {cData}  </div></div>";
        var studentcharge_id;
        $("#jqGrid").jqGrid({
            url: 'data/pupils',
            datatype: "local",
            rownumbers: true,
            toppager: true, cloneToTop: true,
            colModel: [
                {
                    label: '#',
                    name: 'pupil_id',
                    hidden: true,
                    width: 8,
                    key: true,
                    search: false,
                },
                {label: 'Class',
                    name: 'class_id',
                    editable: false,
                    edittype: 'select',
                    formatter: 'select',
                    editoptions: {value:<?php echo Details::classjson(); ?>},
                    stype: 'select',
                    searchoptions: {value:<?php echo Details::classjson(); ?>},
                    width: 200,
                },
                {label: 'Stream',
                    name: 'stream_id',
                    editable: false,
                    edittype: 'select',
                    formatter: 'select',
                    editoptions: {value:<?php echo Details::streamjson(); ?>},
                    stype: 'select',
                    searchoptions: {value:<?php echo Details::streamjson(); ?>},
                    width: 200,
                },
                {
                    label: 'First Name',
                    name: 'fname',
                    editable: false,
                    hidden: true,
                    editrules: {edithidden: true, required: true},
                    width: 200,
                },
                {
                    label: 'Last Name',
                    name: 'lname',
                    hidden: true,
                    editable: false,
                    editrules: {edithidden: true, required: true},
                    width: 200,
                },
                {
                    label: 'Other Name',
                    name: 'othername',
                    hidden: true,
                    editable: false,
                    editrules: {edithidden: true},
                    width: 200,
                },
                {label: 'Admission No',
                    name: 'adm',
                    editable: false,
                    width: 245,
                },
                {label: 'Name',
                    name: 'name',
                    editable: false,
                    width: 245,
                },
                {
                    label: 'Uses Bus?',
                    name: 'usebus',
                    editable: false,
                    width: 170,
                    search: true,
                    edittype: 'checkbox',
                    formatter: 'checkbox',
                    stype: "select", searchoptions: { sopt: ["eq", "ne"], value: ":All;1:Yes;0:No" },
                    editoptions: {value: "1:0"},
                },
                {
                    label: 'Boarder?',
                    name: 'boarding',
                    editable: false,
                    width: 170,
                    search: true,
                    edittype: 'checkbox',
                     stype: "select", searchoptions: { sopt: ["eq", "ne"], value: ":All;1:Yes;0:No" },
                    formatter: 'checkbox',
                    editoptions: {value: "1:0"},
                },
                                        {
                    label: 'Takes Lunch?',
                    name: 'lunch',
                    editable: false,
                    width: 170,
                    search: true,
                     stype: "select", searchoptions: { sopt: ["eq", "ne"], value: ":All;1:Yes;0:No" },
                    edittype: 'checkbox',
                    formatter: 'checkbox',
                    editoptions: {value: "1:0"},
                },

                {label: 'Charge Details',
                    name: 'charge_id',
                    editable: true,
                    hidden: true,
                    edittype: 'select',
                    formatter: 'select',
                    editoptions: {value:<?php echo Details::chargesjson(); ?>},
                    stype: 'select',
                    editrules: {edithidden: true, required: true},
                    searchoptions: {value:<?php echo Details::chargesjson(); ?>},
                    width: 200,
                },
                {label: 'Amount',
                    name: 'amount',
                    hidden: true,
                    editrules: {edithidden: true, required: true},
                    editable: true,
                    width: 245,
                },
            ],
            sortname: 'pupil_id',
            sortorder: 'asc',
            loadonce: true,
            multiselect: true,
            viewrecords: true,
            width: 1000,
            height: 400,
            rowNum: 20,
            pager: "#jqGridPager",
            subGrid: true,
            navOptions: {reloadGridOptions: {fromServer: true}},
            // define the icons in subgrid
            subGridRowExpanded: function(subgrid_id, row_id) {
                var subgrid_table_id, pager_id;
                subgrid_table_id = subgrid_id + "_t";
                pager_id = "p_" + subgrid_table_id;
                var rowData = $("#jqGrid").getRowData(row_id);
                var pupil_id = rowData['pupil_id'];
                $("#" + subgrid_id).html("<table id='" + subgrid_table_id + "' class='scroll'></table><div id='" + pager_id + "' class='scroll'></div>");
                jQuery("#" + subgrid_table_id).jqGrid({
                    url: 'data/studentcharges?subgridpupil=' + pupil_id,
                    datatype: "local",
                    toppager: true, cloneToTop: true,
                    colModel: [
                        {
                            label: '#',
                            name: 'studentcharge_id',
                            hidden: true,
                            width: 8,
                            key: true,
                            search: false,
                            editable: true,
                        },
                        {label: 'Charge Details',
                            name: 'charge_id',
                            editable: true,
                            edittype: 'select',
                            formatter: 'select',
                            editoptions: {value:<?php echo Details::chargesjson(); ?>},
                            stype: 'select',
                            searchoptions: {value:<?php echo Details::chargesjson(); ?>},
                            width: 200
                        },
                        {label: 'Amount',
                            name: 'amount',
                            editable: true,
                            width: 245,
                        },
                        {
                            label: 'Amount Charged',
                            name: 'amount_charged',
                            width: 200,
                        }, {
                            label: 'Balance',
                            name: 'balance',
                            width: 200
                        }, {
                            label: 'Last Modified',
                            name: 'datecreated',
                            width: 200
                        },
                    ],
                    rowNum: 9000,
                    pager: pager_id,
                    caption: "Details for Other Charges",
                    sortname: 'pupil_id',
                    width: '900',
                    loadonce: true,
                    sortorder: "asc",
                    height: '100%',
                });

                //  $("#" + subgrid_table_id).setGridParam({datatype: 'json'}).trigger('reloadGrid');
                jQuery("#" + subgrid_table_id).jqGrid('navGrid', "#" + pager_id,
                        {edit: false, add: false, del: true, cloneToTop: true},
                {url: "fees/charges/update", afterSubmit: processAddEdit, closeAfterEdit: true},
                {url: "fees/charges/create", afterSubmit: processAddEdit, closeAfterAdd: true},
                {url: "fees/chargestudents/delete"});
                $("#" + subgrid_table_id).setGridParam({datatype: 'json'}).trigger('reloadGrid');
            }//subgrid function
        }).setGridParam({datatype: 'json'}).trigger('reloadGrid');
        $('#jqGrid').navGrid('#jqGridPager',
                {edit: false, add: false, del: false, search: true, refresh: true, view: true, position: "left", cloneToTop: true},
        {url: "fees/charges/update", afterSubmit: processAddEdit, closeAfterEdit: true},
        {url: "fees/charges/create", afterSubmit: processAddEdit, closeAfterAdd: true},
        {url: "fees/charges/delete"},
        {// reload
            reloadAfterSubmit: true
        },
        // options for the Edit Dialog
        {
            editCaption: "The Edit Dialog",
            template: template,
            errorTextFormat: function(data) {
                return 'Error: ' + data.responseText
            }
        },
        // options for the Add Dialog
        {
            editCaption: "Charge Students ",
            template: template,
            errorTextFormat: function(data) {
                return 'Error: ' + data.responseText
            }
        },
        // options for the Delete Dailog
        {
            errorTextFormat: function(data) {
                return 'Error: ' + data.responseText
            },
        });
        $('#jqGrid').jqGrid('filterToolbar', {
            // JSON stringify all data from search, including search toolbar operators
            stringResult: true,
            // instuct the grid toolbar to show the search options
            searchOperators: false,
        });
        $("#jqGrid").jqGrid('navButtonAdd',
                '#jqGrid_toppager_left',
                {caption: " Charge",
                    buttonicon: 'glyphicon glyphicon-tags',
                    id: "btnAddNewItem",
                    addCaption: "Charge Students",
                    onClickButton: function()
                    {
                        getSelectedRows();
                    }
                });
    });
    function getSelectedRows() {
        var grid = $("#jqGrid");
        var rowKey = grid.getGridParam("selrow");
        if (!rowKey) {
            //  alert("No rows are selected");
            $.jgrid.info_dialog.call(this,
                    "<i class='fa fa-exclamation-triangle'></i>  Warning", // dialog title
                    "Please, select The Students to Charge" // text inside of dialog
                    );
        }
        else {
            var selectedIDs = grid.getGridParam("selarrrow");
            var result = "";
            var pupil_ids = [];
            for (var i = 0; i < selectedIDs.length; i++) {
                var name = $("#jqGrid").jqGrid('getCell', selectedIDs[i], 'name');
                pupil_ids.push(selectedIDs[i]);
                result = result + name + ",";
            }
            $("#jqGrid").jqGrid('editGridRow', "new", {addCaption: "Charge Students",
                url: "fees/chargestudents/create",
                afterSubmit: processAddEdit,
                closeAfterAdd: true,
                closeAfterEdit: true,
                editData: {"pupils_id": pupil_ids}
            });
            $('.tinfo').html(result + "<hr />");
        }
    }


</script>

