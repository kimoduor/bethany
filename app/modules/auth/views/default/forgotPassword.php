<!-- Latest compiled and minified CSS -->
<script src="<?php echo Yii::app()->baseUrl.'/malt/js/jquery-2.0.2.min.js';?>"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl.'/malt/jqgrid/css/bootstrap.css';?>">
<style>
        .login{
            margin: 0;
            outline: none;
            font: 13px/1.55 'Open Sans', Helvetica, Arial, sans-serif;
            color: #666;
           width: 90%;
            margin: 0 auto;
            padding: 40px;
        }   
    </style>
<div class="row">
  
<center><img  src="<?php echo Yii::app()->baseUrl ?>/malt/img/ubunifu-logo.jpg" alt="logo" height="90" width="90" /></center>
    <br/>
    <div class="col-md-offset-4 col-md-4">
        <div class="well no-padding">
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'forgot-password-form',
                'enableClientValidation' => false,
                'focus' => array($model, 'username'),
                'clientOptions' => array(
                    'validateOnSubmit' => false,
                ),
                'htmlOptions' => array(
                    'class' => 'smart-form client-form',
                )
            ));
            ?>
            <center><strong>Password Recovery</strong></center>
            <?php echo $form->errorSummary($model, ''); ?>
            <fieldset>
               
                 Enter Your Email
 
                        <?php echo $form->textField($model, 'username', array('class' => '', 'required' => true,'class'=>'form-control')); ?>
            </fieldset>
            <footer>
                <button type="submit" class="btn btn-primary"><?php echo Lang::t('Submit'); ?></button>
                <a class="btn btn-link" href="<?php echo $this->createUrl('login') ?>"><?php echo Lang::t('Back to login') ?></a>

            </footer>
            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>