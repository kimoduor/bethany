<!-- Latest compiled and minified CSS -->
<head>
    <title>Bethany Brains|Login</title>
</head>
<script src="<?php echo Yii::app()->baseUrl.'/malt/js/jquery-2.0.2.min.js';?>"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl.'/malt/jqgrid/css/bootstrap.css';?>">
<style>
        .login{
            margin: 0;
            outline: none;
            font: 13px/1.55 'Open Sans', Helvetica, Arial, sans-serif;
            color: #666;
           width: 90%;
            margin: 0 auto;
            padding: 40px;
        }   
    </style>


    <div class="login" class="row" >
        <center><img  src="<?php echo Yii::app()->baseUrl ?>/malt/img/newlogo.jpeg" alt="logo" height="90" width="90" /></center>
        <br/>
        <div class="col-md-offset-4 col-md-4">
            <div class="well no-padding">
                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'login-form',
                    'enableClientValidation' => false,
                    'focus' => array($model, 'username'),
                    'clientOptions' => array(
                        'validateOnSubmit' => false,
                    ),
                    'htmlOptions' => array(
                        'class' => 'smart-form client-form',
                    )
                ));
                ?>
                <center><strong>Login</strong></center>
                <br />
                <fieldset>
                    <?php echo $form->errorSummary($model, ''); ?>

                    <?php echo $form->textField($model, 'username', array('style' => 'height:50px;font-size:14', 'required' => true, 'class' => 'form-control', 'placeholder' => Lang::t(' Email'))); ?>

                    <br />
                    <?php echo $form->passwordField($model, 'password', array('style' => 'height:50px;font-size:14', 'required' => true, 'class' => 'form-control', 'placeholder' => Lang::t('Password'))); ?>

                    <div class="note">
                        <a href="<?php echo $this->createUrl('forgotPassword') ?>"><?php echo Lang::t('Forgot password?') ?></a>
                    </div>
                    <br />
                    <section>
                        <div class="text-left" style="padding-left: 0px;">
                            <?php if (CCaptcha::checkRequirements()): ?>
                                <?php
                                $this->widget('CCaptcha', array(
                                    'buttonLabel' => '&nbsp;New Code',
                                    'imageOptions' => array('style' => 'width:200px;'),
                                ));
                                ?>
                                <br/>
                                <?php echo CHtml::activeTextField($model, 'verifyCode', array('style' => 'width:200px;')); ?>
                                <p class="help-block"><?php echo Lang::t('Enter the code above.') ?></p>
                            <?php endif; ?>
                        </div>
                    </section>
                </fieldset>
                <footer>
                        
                    <button type="submit" class="btn btn-default" style="width:100%"><?php echo Lang::t('Sign in'); ?></button>
                </footer>
                <br />
                <a href="<?php echo Yii::app()->baseUrl.'/default/registration';?>">Register</a>
                <?php $this->endWidget(); ?>
            </div>
        </div>
    </div>
