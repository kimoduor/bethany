<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class LoginForm extends FormModel {

    public $username;
    public $password;
    public $rememberMe = false;

    /**
     *
     * @var UserIdentity
     */
    public $_identity;

    /**
     *
     * @var type
     */
    public $verifyCode;

    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules() {
        return array(
            // username and password are required
            array('username, password', 'required'),
            // rememberMe needs to be a boolean
            array('rememberMe', 'boolean'),
            array('verifyCode', 'CaptchaExtendedValidator', 'allowEmpty' => !CCaptcha::checkRequirements()),
            // password needs to be authenticated
            array('password', 'authenticate'),
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels() {
        return array(
            'username' => Lang::t('Username'),
            'password' => Lang::t('Password'),
            'rememberMe' => Lang::t('Stay signed in'),
        );
    }
    public function authenticate($attribute, $params) {
         $level_id = Users::model()->getScalar('user_level', '`username`=:t1', array(':t1' =>$this-> username ));
//        $currentSeason = Agriseason::model()->getScalar('season_id', '`active`=:t1', array(':t1' => 1));  
//        if ($currentSeason === FALSE && ($level_id===UserLevels::LEVEL_SUPPLIER || $level_id===UserLevels::LEVEL_AGRODEALER)) {
//            $this->addErrors(array('password' => '', 'username' => Lang::t('Sorry, there is No Active Season')));
//        }
        if (!$this->hasErrors()) {
            if (!$this->_identity->authenticate()) {
                //error code 4:Account not active
                if ($this->_identity->errorCode === UserIdentity::ERROR_ACC_PENDING) {//not activated            
                    $this->addErrors('username', Lang::t('ACC_NOT_ACTIVATED_NOTICE'));
                } else if ($this->_identity->errorCode === UserIdentity::ERROR_ACC_BLOCKED) {//account blocked
                    $this->addErrors('username', Lang::t('ACC_BLOCKED_NOTICE'));
                } else if ($this->_identity->errorCode === UserIdentity::ERROR_AGRODEALER_INACTIVE) {
                    $this->addErrors(array('password' => '', 'username' => Lang::t('Sorry, your account is inactive  kindly call CASU toll free line for further assistance')));
                }
                else {
                    $this->addErrors(array('password' => '', 'username' => Lang::t('Incorrect login details given')));
                }
            }
        }   
    }

}
