<?php

/**
 * @author Joakim <kimoduor@gmail.com>
 * Parent controller for the settings module
 */
class PupilsModuleController extends Controller
{

    public function init()
    {
        if (empty($this->resource))
            $this->resource = 'Pupils';
        parent::init();
    }

    public function setModulePackage()
    {

        $this->module_package = array(
            'baseUrl' => $this->module_assets_url,
            'js' => array(
               // 'js/module.js',
                        ),
            'css' => array(
            ),
        );
    }

}
