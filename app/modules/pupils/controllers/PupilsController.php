<?php

/**
 * Settings default controller
 * @author Joakim <kimoduor@gmail.com>
 */
class PupilsController extends PupilsModuleController {

    public function init() {

        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index', 'delete', 'create', 'update', 'barcode', 'updateexternal', 'testbarcode'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
        $this->render('index', array('model' => Pupils::model()->searchModel(array(), 10, 'pupil_id')));
    }

    public function actionBarcode() {
        $this->render('barcode', array('model' => Pupils::model()->searchModel(array(), 10, 'pupil_id')));
    }

    public function actionTestbarcode() {
        $this->render('testbarcode', array('model' => Pupils::model()->searchModel(array(), 10, 'pupil_id')));
    }

    public function actionCreate() {
        if (isset($_POST['fname'])) {
            $model = new Pupils();
            $model->class_id = $_POST['class_id'];
            $model->stream_id = $_POST['stream_id'];
            $model->fname = $_POST['fname'];
            $model->lname = $_POST['lname'];
            $model->adm = $_POST['adm'];
            $model->othername = $_POST['othername'];
            $model->parentname = $_POST['parentname'];
            $model->parentmobile = $_POST['parentmobile'];
         //   $model->parent_email = $_POST['parent_email'];
            $model->usebus = $_POST['usebus'];
            
            $model->boarding = $_POST['boarding'];
            $model->house_id = $_POST['house_id'];
            $model->bednumber = $_POST['bednumber'];
            $model->lunch = $_POST['lunch'];
//            $model->sms_parent = $_POST['sms_parent'];
//            $model->email_parent = $_POST['email_parent'];
            $error_message = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error_message);
            if (!empty($error_message_decoded)) {
                echo $error_message;
            } else {
                $model->save();
                $model->barcode = "1000" . $model->pupil_id;
                $model->save();
                echo json_encode(true);
            }
        }
    }

    public function actionUpdate() {
        if (isset($_POST['id'])) {
            $model = Pupils::model()->loadModel($_POST['id']);
            $model->class_id = $_POST['class_id'];
            $model->fname = $_POST['fname'];
            $model->lname = $_POST['lname'];
            $model->stream_id = $_POST['stream_id'];
            $model->adm = $_POST['adm'];
            $model->othername = $_POST['othername'];
            $model->parentname = $_POST['parentname'];
            $model->parentmobile = $_POST['parentmobile'];
            //$model->parent_email = $_POST['parent_email'];
            $model->usebus = $_POST['usebus'];
             $model->boarding = $_POST['boarding'];
            $model->house_id = $_POST['house_id'];
            $model->bednumber = $_POST['bednumber'];
            $model->lunch = $_POST['lunch'];
//            $model->sms_parent = $_POST['sms_parent'];
//            $model->email_parent = $_POST['email_parent'];
            $error_message = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error_message);
            if (!empty($error_message_decoded)) {
                echo $error_message;
            } else {
                $model->save();
                echo json_encode(true);
            }
        }
    }

    public function actionUpdateexternal() {
        // if (isset($_POST['pupil_id'])) {
        $model = Pupils::model()->loadModel($_POST['Pupils']['pupil_id']);
        $model_class_name = get_class($model);

        if (isset($_POST[$model_class_name]) && isset($_POST['Pupils']['lat'])) {
            $model->lat = $_POST['Pupils']['lat'];
            $model->long = $_POST['Pupils']['long'];
        } elseif ((isset($_POST[$model_class_name]) && isset($_POST['Pupils']['barcode']))) {
            $model->barcode = $_POST['Pupils']['barcode'];
        }
        //--------------------------------upload passport
        $target_dir = getcwd() . "/public/pupilsphotos/";
        if (isset($_FILES["uploadImage"])) {
            $img = rand(0, 1000000) . "-" . basename($_FILES["uploadImage"]["name"]);
            $target_file = $target_dir . $img;
            $uploadOk = 1;
            $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image
            if (isset($_POST) && !empty($_FILES["uploadImage"]["tmp_name"])) {
                $check = getimagesize($_FILES["uploadImage"]["tmp_name"]);
                if ($check !== false) {
                    $uploadOk = 1;
                } else {
                    Yii::app()->session['error'] = "No file was Uploaded OR File is Not an Image";
                    $this->redirect("../../#/barcode", array('model' => $model));
                    $uploadOk = 0;
                }
            }
// Check file size
            if ($_FILES["uploadImage"]["size"] > 1500000) {
                Yii::app()->session['error'] = "Sorry, the Image is too large";
                $this->redirect("../../#/barcode", array('model' => $model));
                $uploadOk = 0;
            }
// Allow certain file formats
            if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
                Yii::app()->session['error'] = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                $this->redirect("../../#/barcode", array('model' => $model));
                $uploadOk = 0;
            }
// Check if $uploadOk is set to 0 by an error
            if ($uploadOk == 0) {
                Yii::app()->session['error'] = "Sorry, your file was not uploaded.";
                $this->redirect("../../#/barcode", array('model' => $model));
            } else {//now upload
                if (move_uploaded_file($_FILES["uploadImage"]["tmp_name"], $target_file)) {
                    $model->passport = $img;
                }
            }
        }
        //---------------------------------finished uploading passport



        $model->save();

        // }
        $this->redirect("../../?yzxx" . md5(date("Y-m-d H:s")) . "//" . $model->pupil_id . "#/barcode");
    }

    public function actionDelete() {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];

            try {
                Pupils::model()->loadModel($id)->delete();
                echo json_encode(true);
            } catch (Exception $e) {
                echo json_encode(array('error' => Lang::t("Sorry, This record cannot be deleted, it is being used elsewhere ")));
            }
        }
    }

}
