<?php

class PupilsModule extends CWebModule
{
	public function init()
	{
		$this->setImport(array(
			'pupils.models.*',
			'pupils.components.*',
		));
	}

    public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			return true;
		}
		else
			return false;
	}
}
