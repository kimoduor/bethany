<?php

/**
 * This is the model class for table "pupils".
 *
 * The followings are the available columns in table 'pupils':
 * @property string $pupil_id
 * @property string $adm
 * @property string $class_id
 * @property string $stream_id
 * @property string $house_id
 * @property string $fname
 * @property string $lname
 * @property string $othername
 * @property string $parentname
 * @property string $parentmobile
 * @property string $parent_email
 * @property string $lat
 * @property string $long
 * @property integer $usebus
 * @property string $barcode
 * @property string $thumbcode
 * @property string $cardcode
 * @property string $passport
 * @property integer $sms_parent
 * @property integer $email_parent
 *
 * The followings are the available model relations:
 * @property Averagemarks[] $averagemarks
 * @property Marks[] $marks
 */
class Pupils extends ActiveRecord
{
    public $key=1;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pupils';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('adm, class_id, fname', 'required'),
			array('usebus, sms_parent, email_parent', 'numerical', 'integerOnly'=>true),
			array('adm, fname, lname, othername, parentmobile, parent_email', 'length', 'max'=>255),
			array('class_id, stream_id, house_id', 'length', 'max'=>11),
			array('parentname, lat, long, barcode, thumbcode, cardcode, passport', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('pupil_id, adm, class_id, stream_id, house_id, fname, lname, othername, parentname, parentmobile, parent_email, lat, long, usebus, barcode, thumbcode, cardcode, passport, sms_parent, email_parent', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'averagemarks' => array(self::HAS_MANY, 'Averagemarks', 'pupil_id'),
			'marks' => array(self::HAS_MANY, 'Marks', 'pupil_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'pupil_id' => 'Pupil',
			'adm' => 'Adm',
			'class_id' => 'Class',
			'stream_id' => 'Stream',
			'house_id' => 'House',
			'fname' => 'Fname',
			'lname' => 'Lname',
			'othername' => 'Othername',
			'parentname' => 'Parentname',
			'parentmobile' => 'Parentmobile',
			'parent_email' => 'Parent Email',
			'lat' => 'Lat',
			'long' => 'Long',
			'usebus' => '0-no,1-yes',
			'barcode' => 'Barcode',
			'thumbcode' => 'Thumbcode',
			'cardcode' => 'Cardcode',
			'passport' => 'Passport',
			'sms_parent' => '0-no,1-yes',
			'email_parent' => '0-no,1-yes',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('pupil_id',$this->pupil_id,true);
		$criteria->compare('adm',$this->adm,true);
		$criteria->compare('class_id',$this->class_id,true);
		$criteria->compare('stream_id',$this->stream_id,true);
		$criteria->compare('house_id',$this->house_id,true);
		$criteria->compare('fname',$this->fname,true);
		$criteria->compare('lname',$this->lname,true);
		$criteria->compare('othername',$this->othername,true);
		$criteria->compare('parentname',$this->parentname,true);
		$criteria->compare('parentmobile',$this->parentmobile,true);
		$criteria->compare('parent_email',$this->parent_email,true);
		$criteria->compare('lat',$this->lat,true);
		$criteria->compare('long',$this->long,true);
		$criteria->compare('usebus',$this->usebus);
		$criteria->compare('barcode',$this->barcode,true);
		$criteria->compare('thumbcode',$this->thumbcode,true);
		$criteria->compare('cardcode',$this->cardcode,true);
		$criteria->compare('passport',$this->passport,true);
		$criteria->compare('sms_parent',$this->sms_parent);
		$criteria->compare('email_parent',$this->email_parent);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Pupils the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
