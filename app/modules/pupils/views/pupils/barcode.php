<?php
$pupil_id = 0;
$pupil_idy = 0;
$pupil_idx = "";
$yz = 1;
$role_id = Users::model()->get(Yii::app()->user->id, "role_id");
$urlheader = @$_SERVER['HTTP_REFERER'];

if (strpos($urlheader, "?")) {
    $pupil_idx = substr($urlheader, strpos($urlheader, "?") + 1);
    $pupil_idy = substr($pupil_idx, strpos($pupil_idx, "//") + 2);
    $yz = substr($pupil_idx, strpos($pupil_idx, "yz") + 2, strpos($pupil_idx, "xx") - 2);
}
if ($pupil_idy > 0) {
    $pupil_id = $pupil_idy;
}
$model->key = $yz > 0 ? $yz : $model->key;

$model = Pupils::model()->loadModel($pupil_id);


//determine if teh pupil has a google location if not, your location is the default
?>
<style>
    .portlet-body{
        display: none;
    }
    #body<?php echo $model->key; ?>{
        display: block; 
    }
    .datepicker,.table-condensed { width: 30em; font-size: 14px; }
</style>


<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-map-marker fa-2x"></i>Google Location for <?php echo $model->fname . " " . $model->lname; ?>
                    &nbsp;&nbsp;&nbsp;
                    Class: <?php echo Classes::model()->get($model->class_id, "class"); ?>
                </div>
                <div class="tools">
                    <a href="javascript:;" class="expand" id="widget1" onclick="widgetclicked()">
                    </a>

                </div>
            </div>
            <div class="portlet-body form" id="body1">
                <form action="pupils/pupils/updateexternal" id="form-username" class="form-horizontal form-bordered" method="post">
                    <input type="hidden" name="Pupils[pupil_id]" value="<?php echo $pupil_id; ?>">
                    <div class="form-group">
                        <div class="col-sm-12">


                            <div class="form-group">
                                <label class="col-sm-2 control-label">Google Latitude</label>
                                <div class="col-sm-4">
                                    <input type="text" id="latitude" name="Pupils[lat]" value="<?php echo $model->lat; ?>" class="form-control"/>
                                </div>
                                <label class="col-sm-2 control-label">Google Longitude</label>
                                <div class="col-sm-4">
                                    <input type="text" id="longitude"  name="Pupils[long]" value="<?php echo $model->long; ?>" class="form-control"/>
                                </div>

                            </div>
                            <!------------------------start your map------------------------->
                            <div class="col-sm-14" id="map" style="height:400px"></div>
                            <!----------------------------end your map--------------------->
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button name="sub1" type="submit" class="btn green"><i class="fa fa-check"></i> Save</button>
                                        <button type="button" class="btn default">Cancel</button>
                                    </div>
                                </div>
                            </div>
                            </form>

                        </div>
                    </div>

            </div>
        </div>
        <div class="portlet box yellow-crusta">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-barcode"></i> Barcode</div>
                <div class="tools">
                    <a href="javascript:;" class="expand" id="widget1" onclick="widgetclicked()">
                    </a>

                </div>
            </div>
            <div class="portlet-body form" id="body2">
                <form action="pupils/pupils/updateexternal" id="form-username" class="form-horizontal form-bordered" method="post">
                    <input type="hidden" name="Pupils[pupil_id]" value="<?php echo $pupil_id; ?>">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Barcode<font color="red">This Barcode should only be edited under Very Precarious Conditions</font></label>
                                <div class="col-sm-6">
                                    <input type="text" id="barcode" name="Pupils[barcode]" value="<?php echo $model->barcode; ?>" class="form-control"/>
                                </div>
                            </div>
                            <center>
                                    <?php
                                    $generator = new BarcodeGeneratorHTML();
                                    echo $generated = $generator->getBarcode($model->barcode, $generator::TYPE_CODE_128);
                                    ?>
                               </center>
                        </div>
                    </div>

                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button name="sub1" type="submit" class="btn green"><i class="fa fa-check"></i> Save</button>
                                <button type="button" class="btn default">Cancel</button>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
        </div>
        <! ------------------------------------passport---->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet box yellow-gold">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>Passport size Photo
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse" onclick="widgetclicked()">
                    </a>

                </div>
            </div>
            <style type="text/css">
                .myimage {width:200px;
                          margin: 20px;
                          box-shadow:#ccc 1px;

                }
                legend{
                    font-size: 14px;
                }

            </style>
            <div class="portlet-body form" id="body2">       
                <fieldset>

                    <div id="front-end" style="float:left; padding: 40px;">
                        <form action="pupils/pupils/updateexternal" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="Pupils[pupil_id]" value="<?php echo $model->pupil_id; ?>">
                            Select Passport size Photo  
                            <br />
                            <img id="uploadPreview" src="<?php echo $model->passport ? "public/pupilsphotos/$model->passport" : "malt/img/default.jpg"; ?>" class="myimage" /><br />
                            <input type="file" name="uploadImage" id="uploadImage" onchange="PreviewImage()">
                            <br />  <input type="submit" class="btn blue-madison" value="Upload Passport" name="submit">
                        </form>
                    </div>

                </fieldset>

                <script type="text/javascript">
                    function PreviewImage() {
                        var oFReader = new FileReader();
                        oFReader.readAsDataURL(document.getElementById("uploadImage").files[0]);
                        oFReader.onload = function(oFREvent) {
                            document.getElementById("uploadPreview").src = oFREvent.target.result;
                        };
                    }
                </script>

                <!-- END FORM-->
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
</div>

    </div>
</div>
<!------------------------------------end passport-------------->

<script>
    function widgetclicked() {
        $('.portlet-body').hide();
        $('.collapse').addClass("expand");
        $('.expand').removeClass("collapse");
        $(this).addClass("collapse");
        $(this).removeClass("expand");
    }
    function initialize(lati, longi) {
        var $latitude = document.getElementById('latitude');
        var $longitude = document.getElementById('longitude');

        var latitude = lati;
        var longitude = longi;
        var zoom = 15;

        var LatLng = new google.maps.LatLng(latitude, longitude);

        var mapOptions = {
            zoom: zoom,
            center: LatLng,
            panControl: true,
            zoomControl: true,
            scaleControl: true,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        var map = new google.maps.Map(document.getElementById('map'), mapOptions);

        var marker = new google.maps.Marker({
            position: LatLng,
            map: map,
            title: 'Drag Me!',
            draggable: true
        });

        google.maps.event.addListener(marker, 'dragend', function(marker) {
            var latLng = marker.latLng;
            $latitude.value = latLng.lat();
            $longitude.value = latLng.lng();
        });

    }
<?php if (empty($model->lat) || empty($model->long)) { ?>
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        } else {
            $("#map").html("Geolocation is not supported by this browser.");
        }

        function showPosition(position) {
            $("#latitude").val(position.coords.latitude);
            $("#longitude").val(position.coords.longitude);
            initialize(position.coords.latitude, position.coords.longitude);
        }
<?php } else {
    ?>
        initialize(<?php echo $model->lat; ?>,<?php echo $model->long; ?>);
    <?php
}
?>


</script>
