<script>
    $.jgrid.defaults.width = 1200;
    $.jgrid.defaults.responsive = true;
    $.jgrid.defaults.styleUI = 'Bootstrap';</script>
<div style="margin-left:20px">
    <table id="jqGrid"></table>
    <div id="jqGridPager"></div>
</div>
<script type="text/javascript">

    $(document).ready(function() {
        $.jgrid.styleUI.Bootstrap.base.rowTable = "table table-bordered table-striped";
        var template = "<div style='margin-left:15px;'>";
        template += "<div> First Name: </div><div>{fname} </div>";
        template += "<hr style='width:100%;'/>";
        template += "<div> {sData} {cData}  </div></div>";
        $("#jqGrid").jqGrid({
            url: 'data/pupils',
            datatype: "local",
            rownumbers: true,
            toppager: true, cloneToTop: true,
            colModel: [
                {
                    label: '#',
                    name: 'pupil_id',
                    hidden: true,
                    width: 8,
                    key: true,
                    search: false,
                },
                {label: 'Class',
                    name: 'class_id',
                    editable: true,
                    edittype: 'select',
                    formatter: 'select',
                    editoptions: {value:<?php echo Details::classjson(); ?>},
                    stype: 'select',
                    searchoptions: {value:<?php echo Details::classjson(); ?>},
                    width: 200,
                },
                {label: 'Stream',
                    name: 'stream_id',
                    editable: true,
                    edittype: 'select',
                    formatter: 'select',
                    editoptions: {value:<?php echo Details::streamjson(); ?>},
                    stype: 'select',
                    searchoptions: {value:<?php echo Details::streamjson(); ?>},
                    width: 200,
                },
                {
                    label: 'First Name',
                    name: 'fname',
                    editable: true,
                    hidden: true,
                    editrules: {edithidden: true, required: true},
                    width: 200,
                },
                {
                    label: 'Last Name',
                    name: 'lname',
                    hidden: true,
                    editable: true,
                    editrules: {edithidden: true, required: true},
                    width: 200,
                },
                {
                    label: 'Other Name',
                    name: 'othername',
                    hidden: true,
                    editable: true,
                    editrules: {edithidden: true},
                    width: 200,
                },
                {label: 'Admission No',
                    name: 'adm',
                    editable: true,
                    width: 245,
                },
                {label: 'Name',
                    name: 'name',
                    editable: false,
                    width: 245,
                },
                {
                    label: 'Parent Name',
                    name: 'parentname',
                    editable: true,
                    width: 245,
                }, {
                    label: 'Parent Mobile',
                    name: 'parentmobile',
                    editable: true,
                    width: 200,
                },
                {
                    label: 'Uses Bus?',
                    name: 'usebus',
                    editable: true,
                    width: 170,
                    search: false,
                    edittype: 'checkbox',
                    formatter: 'checkbox',
                    editoptions: {value: "1:0"},
                },
                {
                    label: 'Boarder?',
                    name: 'boarding',
                    editable: true,
                    width: 170,
                    search: false,
                    edittype: 'checkbox',
                    formatter: 'checkbox',
                    editoptions: {value: "1:0"},
                },
                {label: 'House',
                    name: 'house_id',
                    editable: true,
                    edittype: 'select',
                    formatter: 'select',
                    editoptions: {value:<?php echo Details::housejson(); ?>},
                    stype: 'select',
                    searchoptions: {value:<?php echo Details::housejson(); ?>},
                    width: 200,
                },
                {
                    label: 'Bed Number',
                    name: 'bednumber',
                    editable: true,
                    width: 245,
                },
                {
                    label: 'Lunch?',
                    name: 'lunch',
                    editable: true,
                    width: 170,
                    search: false,
                    edittype: 'checkbox',
                    formatter: 'checkbox',
                    editoptions: {value: "1:0"},
                },
            ],
            sortname: 'pupil_id',
            sortorder: 'asc',
            loadonce: true,
            viewrecords: true,
            width: 1000,
            height: 400,
            rowNum: 20,
            pager: "#jqGridPager",
            subGrid: true,
            navOptions: {reloadGridOptions: {fromServer: true}},
            // define the icons in subgrid
            subGridRowExpanded: function(subgrid_id, row_id) {
                var subgrid_table_id, pager_id;
                subgrid_table_id = subgrid_id + "_t";
                pager_id = "p_" + subgrid_table_id;
                var rowData = $("#jqGrid").getRowData(row_id);
                var pupil_id = rowData['pupil_id'];
                $("#" + subgrid_id).html("<table id='" + subgrid_table_id + "' class='scroll'></table><div id='" + pager_id + "' class='scroll'></div>");
                jQuery("#" + subgrid_table_id).jqGrid({
                    url: 'data/pupils?subgridpupil=' + pupil_id,
                    datatype: "json",
                    colModel: [
                        {
                            label: '#',
                            name: 'pupil_id',
                            hidden: true,
                            width: 8,
                            key: true,
                            search: false,
                            editable: false,
                        },
                        {
                            label: 'Parent Email',
                            name: 'parent_email',
                            editable: true,
                            width: 200,
                        },
                        {label: 'Sms Parent?',
                            name: 'sms_parent',
                            editable: true,
                            width: 170,
                            search: false,
                            edittype: 'checkbox',
                            formatter: 'checkbox',
                            editoptions: {value: "1:0"},
                        }, {
                            label: 'Email Parent?',
                            name: 'email_parent',
                            editable: true,
                            width: 170,
                            search: false,
                            edittype: 'checkbox',
                            formatter: 'checkbox',
                            editoptions: {value: "1:0"},
                        },
                        {
                            label: 'Latitude',
                            name: 'lat',
                            width: 200,
                        }, {
                            label: 'Longitude',
                            name: 'long',
                            width: 200,
                        }
                        , {
                            label: 'Bar Code',
                            name: 'barcode',
                            width: 200,
                        }, {
                            label: 'Thumb Code',
                            name: 'thumbcode',
                            width: 200,
                        }, {
                            label: 'eCard Code',
                            name: 'cardcode',
                            width: 200,
                        },
                    ],
                    rowNum: 20,
                    pager: pager_id,
                    caption: "Other Details of the Pupil",
                    sortname: 'loan_id',
                    width: '900',
                    loadonce: true,
                    sortorder: "asc",
                    height: '100%'
                });
                jQuery("#" + subgrid_table_id).jqGrid('navGrid', "#" + pager_id, {edit: false, add: false, del: false})
            },
        }).setGridParam({datatype: 'json'}).trigger('reloadGrid');
        $('#jqGrid').navGrid('#jqGridPager',
                {edit: true, add: true, del: true, search: true, refresh: true, view: true, position: "left", cloneToTop: true},
        {url: "pupils/pupils/update", afterSubmit: processAddEdit, closeAfterEdit: true},
        {url: "pupils/pupils/create", afterSubmit: processAddEdit, closeAfterEdit: true},
        {url: "pupils/pupils/delete"},
        {// reload
            reloadAfterSubmit: true
        },
        // options for the Edit Dialog
        {
            editCaption: "The Edit Dialog",
            template: template,
            errorTextFormat: function(data) {
                return 'Error: ' + data.responseText
            }
        },
        // options for the Add Dialog
        {
            template: template,
            errorTextFormat: function(data) {
                return 'Error: ' + data.responseText
            }
        },
        // options for the Delete Dailog
        {
            errorTextFormat: function(data) {
                return 'Error: ' + data.responseText
            },
        }).navButtonAdd('#jqGridPager', {
            caption: "",
            toppager: true,
            cloneToTop: true,
            buttonicon: "glyphicon glyphicon-pencil",
            onClickButton: function() {
                try {
                    var rowId = $("#jqGrid").jqGrid('getGridParam', 'selrow');
                    var rowData = $("#jqGrid").getRowData(rowId);
                    var colData = rowData['pupil_id'];
                    if (colData == undefined) {
                        alert("Kindly select the Record to View");
                    }
                    else {
                        //  window.location.href = "#/requestloan?l=" + colData;
                        window.location.href = "?<?php echo md5(date("Y-m-d H:s")); ?>1ssd787hj//" + colData + "#/barcode";
                    }

                } catch (e) {

                }
            },
        });
        $('#jqGrid').jqGrid('filterToolbar', {
            // JSON stringify all data from search, including search toolbar operators
            stringResult: true,
            // instuct the grid toolbar to show the search options
            searchOperators: false,
        });



    });

</script>

