<?php

/**
 * Overrides the CUrlManager class
 */
class UrlManager extends CUrlManager {

    const GET_PARAM_RETURN_URL = 'r_url';

    public $showScriptName = false;
    public $appendParams = false;

    public function createUrl($route, $params = array(), $ampersand = '&') {
        $route = preg_replace_callback('/(?<![A-Z])[A-Z]/', function($matches) {
            return '-' . lcfirst($matches[0]);
        }, $route);
        return parent::createUrl($route, $params, $ampersand);
    }

    public function parseUrl($request) {
        $route = parent::parseUrl($request);
        return lcfirst(str_replace(' ', '', ucwords(str_replace('-', ' ', $route))));
    }

    /**
     * Get return link
     * @param type $url
     * @return type
     */
    public static function getReturnUrl($url = NULL) {
        $rl = filter_input(INPUT_GET, self::GET_PARAM_RETURN_URL);
        return !empty($rl) ? $rl : $url;
    }

}
