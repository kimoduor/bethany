<?php

class MyPHPExcelReadFilter implements PHPExcel_Reader_IReadFilter
{

    /**
     *
     * @var type
     */
    public $start_row;

    /**
     *
     * @var type
     */
    public $end_row;

    /**
     *
     * @var type
     */
    public $start_column;

    /**
     *
     * @var type
     */
    public $end_column;

    /**
     *
     * @param type $start_row
     * @param type $end_row
     * @param type $start_column
     * @param type $end_column
     */
    public function __construct($start_row, $end_row, $start_column, $end_column)
    {
        $this->start_row = $start_row;
        $this->end_row = $end_row;
        $this->start_column = $start_column;
        $this->end_column = $end_column;
    }

    /**
     *
     * @param type $column
     * @param type $row
     * @param type $worksheetName
     * @return boolean
     */
    public function readCell($column, $row, $worksheetName = '')
    {
        if ($row >= $this->start_row && $row <= $this->end_row) {
            if (in_array($column, range($this->start_column, $this->end_column))) {
                return true;
            }
        }
        return false;
    }

}
