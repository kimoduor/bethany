<?php

/**
 * Contains functions for manipulating files
  * @author JoAKIM <kimoduor@gmail.com>
 * Timestamp: 7th Aug 2014 22:13 EAT
 */
class FileHelper
{

    /**
     * Get file extension
     * @param string $path the full path of the file
     * @return string
     */
    public static function getFileExtension($path)
    {
        return pathinfo($path, PATHINFO_EXTENSION);
    }

}
