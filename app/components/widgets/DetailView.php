<?php

Yii::import('zii.widgets.CDetailView');

/**
 * Extends CDetailView
 * @author Joakim <kimoduor@gmail.com>
 */
class DetailView extends CDetailView
{

    public $nullDisplay = '&nbsp;';
    public $htmlOptions = array(
        'class' => 'table table-condensed table-striped',
    );

}
