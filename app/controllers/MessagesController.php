<?php

/**
 * Settings default controller
 * @author Joakim <kimoduor@gmail.com>
 */
class MessagesController extends Controller {

    public function init() {

        $this->resourceLabel = Lang::t('Messages');
        // $this->resource = DistributorsModuleConstants::RES_DISTIBUTORS;
        $this->activeTab = 'Messages';
        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index', 'read', 'delete', 'confirm'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
        $this->pageTitle = Lang::t('Messages');
        $model = new Messages();
        $model_class_name = get_class($model);
        $this->render('index');
    }

    public function actionConfirm() {
        $this->pageTitle = Lang::t('Messages');
        $member_id = Membership::model()->getScalar("member_id", "user_id=" . Yii::app()->user->id);
        $condition = "tomember_id=$member_id AND status_id=0"; //0->new,1 unread 2 read
        $models = Messages::model()->findAll(array("condition" => $condition));
        foreach ($models as $model) {
            $savemodel = Messages::model()->loadModel($model->message_id);
            $savemodel->status_id = 1;
            $savemodel->save();
        }
    }

    public function actionRead() {
        if (isset($_POST['message_id'])) {
            $id = $_POST['message_id'];
            $model = Messages::model()->loadModel($id);
            $model->status_id = 2;
            $model->save();
        }
    }

    public function actionDelete() {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];

            try {
                Messages::model()->loadModel($id)->delete();
                Yii::app()->user->setFlash('success', Lang::t("Successfully Deleted"));
            } catch (Exception $e) {
                Yii::app()->user->setFlash('error', Lang::t("Sorry, This record cannot be deleted, it is being used elsewhere "));
            }
        }
    }

}
