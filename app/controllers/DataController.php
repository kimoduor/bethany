<?php

/**
 * Settings default controller
 * @author Joakim <kimoduor@gmail.com>
 */
class DataController extends Controller {

    public function init() {

        $this->resourceLabel = Lang::t('Data');
// $this->resource = DistributorsModuleConstants::RES_DISTIBUTORS;
        $this->activeTab = 'data';
        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('classes', 'routes', 'years', 'buses', 'banks', 'terms',
                    'paymenttypes', 'emails', 'myloans', 'feesstructure', 'voteheads', 'voteheadyear',
                    'charges', 'streams', 'studentcharges', 'receipts', 'receiptdetails', 'exportexcel',
                    'entermarks', 'meritlists', 'tester', 'subjectteachers', 'closingopeningdate', 'reportcards',
                    'feesbalances', 'feesstatement', 'subjects', 'examinationtypes', 'subjectgrades',
                    'expenses', 'expensesincome', 'expensetypes', 'incometypes', 'income', 'subjectanalysis',
                    'reportcardsubjectanalysis',
                    'otherloans', 'guarantors', 'guarantorsx', 'messages', 'county', 'pupils'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionClasses() {
        $models = Classes::model()->findAll();
        $output = array();
        foreach ($models as $model) {
            $array = array();
            $array['class_id'] = $model->class_id;
            $array['class'] = $model->class;

            $output[] = $array;
        }
        $theoutput = json_encode($output);
        echo $theoutput;
    }

    public function actionRoutes() {
        $models = Route::model()->findAll();
        $output = array();
        foreach ($models as $model) {
            $array = array();
            $array['route_id'] = $model->route_id;
            $array['route'] = $model->route;

            $output[] = $array;
        }
        $theoutput = json_encode($output);
        echo $theoutput;
    }

    public function actionBuses() {
        $models = Bus::model()->findAll();
        $output = array();
        foreach ($models as $model) {
            $array = array();
            $array['bus_id'] = $model->bus_id;
            $array['plate_number'] = $model->plate_number;
            $array['tag_name'] = $model->tag_name;
            $array['main_driver_name'] = $model->main_driver_name;
            $array['route_id'] = $model->route_id;
            $array['phone_number'] = $model->phone_number;
            $array['mac_address'] = $model->mac_address;
            $array['driver_phone'] = $model->driver_phone;

            $output[] = $array;
        }
        $theoutput = json_encode($output);
        echo $theoutput;
    }

    public function actionEmails() {
        $models = Emails::model()->findAll();
        $output = array();
        foreach ($models as $model) {
            $array = array();
            $array['email_id'] = $model->email_id;
            $array['email'] = $model->email;
            $array['email_category'] = $model->email_category;
            $output[] = $array;
        }
        $theoutput = json_encode($output);
        echo $theoutput;
    }

    public function actionCounty() {
        $models = County::model()->findAll();
        $output = array();
        foreach ($models as $model) {
            $array = array();
            $array['county_id'] = $model->county_id;
            $array['county'] = $model->county;
            $output[] = $array;
        }
        $theoutput = json_encode($output);
        echo $theoutput;
    }

    public function actionPaymenttypes() {
        $models = Paymenttypes::model()->findAll();
        $output = array();
        foreach ($models as $model) {
            $array = array();
            $array['paymenttype_id'] = $model->paymenttype_id;
            $array['paymenttype'] = $model->paymenttype;
            $output[] = $array;
        }
        $theoutput = json_encode($output);
        echo $theoutput;
    }

    public function actionBanks() {
        $models = Banks::model()->findAll();
        $output = array();
        foreach ($models as $model) {
            $array = array();
            $array['bank_id'] = $model->bank_id;
            $array['bank_name'] = $model->bank_name;
            $output[] = $array;
        }
        $theoutput = json_encode($output);
        echo $theoutput;
    }

    public function actionYears() {
        $models = Years::model()->findAll();
        $output = array();
        foreach ($models as $model) {
            $array = array();
            $array['year_id'] = $model->year_id;
            $array['year'] = $model->year;
            $array['active'] = $model->active;
            $output[] = $array;
        }
        $theoutput = json_encode($output);
        echo $theoutput;
    }

    public function actionTerms() {
        $models = Terms::model()->findAll();
        $output = array();
        foreach ($models as $model) {
            $array = array();
            $array['term_id'] = $model->term_id;
            $array['term'] = $model->term;
            $array['active'] = $model->active;
            $output[] = $array;
        }
        $theoutput = json_encode($output);
        echo $theoutput;
    }

    public function actionPupils() {
        $condition = "";
        if (isset($_GET['subgridpupil'])) {
            $condition = array("condition" => "pupil_id=" . $_GET['subgridpupil']);
        }
        $models = Pupils::model()->findAll($condition);
        $output = array();
        foreach ($models as $model) {
            $array = array();
            $array['pupil_id'] = $model->pupil_id;
            $array['class_id'] = $model->class_id;
            $array['stream_id'] = $model->stream_id;
            $array['adm'] = $model->adm;
            $array['fname'] = $model->fname;
            $array['lname'] = $model->lname;
            $array['othername'] = $model->othername;
            $array['name'] = $model->fname . " " . $model->lname . " " . $model->othername;
            $array['parentname'] = $model->parentname;
            $array['parentmobile'] = $model->parentmobile;
            $array['parent_email'] = $model->parent_email;
            $array['lat'] = $model->lat;
            $array['long'] = $model->long;
            $array['barcode'] = $model->barcode;
            $array['thumbcode'] = $model->thumbcode;
            $array['cardcode'] = $model->cardcode;
            $array['passport'] = $model->passport;
            $array['boarding'] = $model->boarding;
            $array['house_id'] = $model->house_id;
            $array['bednumber'] = $model->bednumber;
            $array['lunch'] = $model->lunch;
            $array['usebus'] = $model->usebus;
            $array['sms_parent'] = $model->sms_parent;
            $array['email_parent'] = $model->email_parent;
            $output[] = $array;
        }
        $theoutput = json_encode($output);
        echo $theoutput;
    }

    public function actionRepaymentmodes() {
        $models = Repaymentmodes::model()->findAll();
        $output = array();
        foreach ($models as $model) {
            $array = array();
            $array['repaymentmode_id'] = $model->repaymentmode_id;
            $array['repaymentmode'] = $model->repaymentmode;
            $output[] = $array;
        }
        $theoutput = json_encode($output);
        echo $theoutput;
    }

    public function actionFeesstructure() {
        $models = FeesStructure::model()->findAll();
        $output = array();
        foreach ($models as $model) {
            $array = array();
            $array['feesstructure_id'] = $model->feesstructure_id;
            $array['class_id'] = $model->class_id;
            $array['year_id'] = $model->year_id;
            $array['term_id'] = $model->term_id;
            $array['amount'] = $model->amount;
            $output[] = $array;
        }
        $theoutput = json_encode($output);
        echo $theoutput;
    }

    public function actionMessages() {
        $member_id = Membership::model()->getScalar("member_id", "user_id=" . Yii::app()->user->id);
        $condition = "tomember_id=$member_id"; //0->unconfirmed,1 confirmed 2 rejected
        $models = Messages::model()->findAll(array("condition" => $condition . ' order by date desc'));
        $output = array();
        foreach ($models as $model) {
            $array = array();
            $array['message_id'] = $model->message_id;
            $array['sendername'] = Membership::model()->get($member_id, "CONCAT(fname,' ',lname)");
            $array['date'] = $model->date;
            $array['subject'] = $model->subject;
            $array['body'] = $model->body;
            $array['status_id'] = $model->status_id;

            $output[] = $array;
        }
        $theoutput = json_encode($output);
        echo $theoutput;
    }

    public function actionVoteheads() {
        $models = Voteheads::model()->findAll();
        $output = array();
        foreach ($models as $model) {
            $array = array();
            $array['votehead_id'] = $model->votehead_id;
            $array['votehead_name'] = $model->votehead_name;
            $output[] = $array;
        }
        $theoutput = json_encode($output);
        echo $theoutput;
    }

    public function actionVoteheadyear() {
        $models = Voteheadyear::model()->findAll(array('condition' => 'votehead_id NOT IN (1)'));
        $output = array();
        foreach ($models as $model) {
            $array = array();
            $array['voteheadyear_id'] = $model->voteheadyear_id;
            $array['votehead_id'] = $model->votehead_id;
            $array['year_id'] = $model->year_id;
            $array['class_id'] = $model->class_id;
            $array['priority'] = $model->priority;
            $array['amount'] = $model->amount;
            $output[] = $array;
        }
        $theoutput = json_encode($output);
        echo $theoutput;
    }

    public function actionCharges() {
        $models = Otherfeescharges::model()->findAll();
        $output = array();
        foreach ($models as $model) {
            $array = array();
            $array['charge_id'] = $model->charge_id;
            $array['chargeparticulars'] = $model->chargeparticulars;
            $array['datecreated'] = $model->datecreated;
            $output[] = $array;
        }
        $theoutput = json_encode($output);
        echo $theoutput;
    }

    public function actionStreams() {
        $models = Streams::model()->findAll();
        $output = array();
        foreach ($models as $model) {
            $array = array();
            $array['stream_id'] = $model->stream_id;
            $array['stream'] = $model->stream;
            $output[] = $array;
        }
        $theoutput = json_encode($output);
        echo $theoutput;
    }

    public function actionStudentcharges() {
        $condition = "";
        if (isset($_GET['subgridpupil'])) {
            $condition = array("condition" => "pupil_id=" . $_GET['subgridpupil']);
        }
        $models = Studentcharges::model()->findAll($condition);
        $output = array();
        foreach ($models as $model) {
            $array = array();
            $array['studentcharge_id'] = $model->studentcharge_id;
            $array['pupil_id'] = $model->pupil_id;
            $array['charge_id'] = $model->charge_id;
            $array['amount'] = $model->amount;
            $array['amount_charged'] = $model->amount_charged;
            $array['balance'] = $model->balance;
            $array['datecreated'] = $model->datecreated;
            $output[] = $array;
        }
        $theoutput = json_encode($output);
        echo $theoutput;
    }

    public function actionReceipts() {
        $condition = " 1=1 ORDER BY date DESC";
        $models = FeesTransactions::model()->findAll($condition);
        $output = array();
        foreach ($models as $model) {
            $array = array();
            $array['receipt_id'] = $model->transaction_id;
            $array['pupil_id'] = $model->pupil_id;
            $pupilmodel = Pupils::model()->loadModel($model->pupil_id);
            $array['name'] = $pupilmodel->fname . " " . $pupilmodel->lname . " " . $pupilmodel->othername;
            $array['date'] = $model->date;
            $array['adm'] = $pupilmodel->adm;
            $array['transaction_details'] = $model->transaction_details;
            $array['receipt_no'] = Details::receiptfromtransaction($model->transaction_id);
            $array['bank_id'] = $model->bank_id;
            $array['cheque_no'] = $model->cheque_no;
            $array['paymenttype_id'] = $model->paymenttype_id;
            $array['amount'] = $model->amount;
            $array['class_id'] = $pupilmodel->class_id;
            $array['stream_id'] = $pupilmodel->stream_id;

            $output[] = $array;
        }
        $theoutput = json_encode($output);
        echo $theoutput;
    }

    public function actionReceiptdetails() {
        $output = array();
        if (isset($_GET['subgridtransaction'])) {
            $transaction_id = $_GET['subgridtransaction'];
            $feesvoteheads = Feesvoteheads::model()->findAll(array("condition" => "fees_id IN "
                . "(SELECT fees_id FROM fees_payment WHERE transaction_id = $transaction_id)"));
            foreach ($feesvoteheads as $feesvotehead) {
                $array = array();
                $array['voteheadyear_id'] = $feesvotehead->voteheadyear_id;
                $array['votehead'] = Details::getvoteheadyear($feesvotehead->voteheadyear_id);
                $array['amount_charged'] = $feesvotehead->amount_charged;
                $output[] = $array;
            }
            //start looking for any charges//make sure all the voteheads rhyme
            $studentcharges = Studentchargestransactions::model()->findAll(array("condition" => "transaction_id= $transaction_id"));
            foreach ($studentcharges as $studentcharge) {
                $array = array();
                $array['voteheadyear_id'] = $studentcharge->studentchargetransactions_id;
                $array['votehead'] = Details::getvoteheadyear($studentcharge->studentcharge_id) . "[Charges]";
                $array['amount_charged'] = $studentcharge->amount;
                $output[] = $array;
            }
        }
        $theoutput = json_encode($output);
        echo $theoutput;
    }

    public function actionExportexcel() {
        $output = "";
        if (isset($_GET['data'])) {
            $output = $_GET['data'];
            $junks = explode("___", $output);

            $newjunk = "";
            foreach ($junks as $junk) {
                if ($junk) {
                    $newjunk .="\n";
                    $pieces = explode("__", $junk);
                    foreach ($pieces as $piece) {
                        if ($piece)
                            $newjunk.=$piece . ",";
                    }
                }
            }
        }

        mb_convert_encoding($newjunk, 'UCS-2LE', 'UTF-8');

        $filename = "Report" . date('Y-m-d') . ".csv";
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . $filename . '.csv');
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        echo "\xEF\xBB\xBF"; // UTF-8 BOM
        echo $newjunk;
    }

    public function actionFeesbalances() {
        $models = Pupils::model()->findAll("1=1 ORDER BY class_id ASC");
        //var_dump($models);
        //  exit();
        $output = array();
        foreach ($models as $model) {
            $transaction_id = FeesTransactions::model()->getScalar("transaction_id", "pupil_id=$model->pupil_id ORDER BY transaction_id DESC ");
            if ($transaction_id > 0) {
                $array = array();
                $array['pupil_id'] = $model->pupil_id;
                $array['adm'] = $model->adm;
                $array['class_id'] = $model->class_id;
                $array['stream_id'] = $model->stream_id;
                $array['name'] = $model->fname . " " . $model->lname . " " . $model->othername;
                $array['lastpaid'] = FeesTransactions::model()->get($transaction_id, 'date');
                $array['actualbalance'] = FeesPayment::model()->getScalar("term_balance", "transaction_id=$transaction_id");
                $charges = Studentcharges::model()->getSum("balance", "pupil_id=$model->pupil_id");
                $array['othercharges'] = $charges;
                $array['total'] = $array['actualbalance'] + $charges;
                $output[] = $array;
            }
        }
        $theoutput = json_encode($output);
        echo $theoutput;
    }

    public function actionFeesstatement() {//get fees payment statement
        if (isset($_GET['pupil_id'])) {
            $pupil_id = $_GET['pupil_id'];
            $output = array();
            //get all fees that the student has ever paid
            $models = FeesTransactions::model()->findAll("pupil_id='$pupil_id' ORDER BY transaction_id");
            foreach ($models as $model) {
                $array = array();
                $array['transaction_id'] = $model->transaction_id;
                $array['amount'] = $model->amount;
                $array['datecreated'] = $model->date;
                $array['paymenttype_id'] = $model->paymenttype_id;
                $array['bank_id'] = $model->bank_id;
                $array['year_id'] = $model->year_id;
                $array['term_id'] = $model->term_id;
                $array['term_balance'] = number_format(FeesPayment::model()->getScalar("term_balance", "transaction_id='$model->transaction_id'"), 2);
                $array['year_balance'] = number_format(FeesPayment::model()->getScalar("year_balance", "transaction_id='$model->transaction_id'"), 2);
                //on the charges
                $anycharges = Studentchargestransactions::model()->getSum("amount", "transaction_id='$model->transaction_id'");
                $array['voteheadsfees'] = number_format($model->amount - $anycharges, 2);
                $array['deductions'] = number_format($anycharges, 2);
                $output[] = $array;
            }
            //charges made without paying fees
            $output2 = array();
            $models2 = Studentchargestransactions::model()->findAll("studentcharge_id IN (SELECT studentcharge_id FROM studentcharges WHERE pupil_id='$pupil_id') AND transaction_id IS NULL");
            foreach ($models2 as $model2) {
                $array2 = array();
                $array2['datecreated'] = $model2->datecreated;
                $array2['deductions'] = $model2->amount;
                $output2[] = $array2;
            }
            $newarray = array_merge($output, $output2);
            foreach ($newarray as $key => $row) {
                $records[$key] = $row['datecreated'];
            }
            array_multisort($records, SORT_DESC, $newarray);

            echo json_encode($newarray);
        }
    }

    public function actionSubjects() {
        $models = Subjects::model()->findAll();
        $output = array();
        foreach ($models as $model) {
            $array = array();
            $array['subject_id'] = $model->subject_id;
            $array['subject'] = $model->subject;
            $output[] = $array;
        }
        $theoutput = json_encode($output);
        echo $theoutput;
    }

    public function actionExaminationtypes() {
        $models = Examinationtypes::model()->findAll();
        $output = array();
        foreach ($models as $model) {
            $array = array();
            $array['examinationtype_id'] = $model->examinationtype_id;
            $array['examinationtype'] = $model->examinationtype;
            $output[] = $array;
        }
        $theoutput = json_encode($output);
        echo $theoutput;
    }

    public function actionSubjectgrades() {
        if (isset($_GET['subject_id'])) {
            $subject_id = $_GET['subject_id'];
            $models = Subjectgrades::model()->findAll(array('condition' => "subject_id=$subject_id"));
            $output = array();
            foreach ($models as $model) {
                $array = array();
                $array['subjectgrade_id'] = $model->subjectgrade_id;
                $array['subject_id'] = $model->subject_id;
                $array['frommark'] = $model->frommark;
                $array['tomark'] = $model->tomark;
                $array['grade'] = $model->grade;
                $array['comment'] = $model->comment;
                $output[] = $array;
            }
            $theoutput = json_encode($output);
            echo $theoutput;
        }
    }

    public function actionEntermarks() {
        $condition = "1=2";
        $subject_id = '';
        $term_id = '';
        $year_id = '';
        $class_id = '';
        if (isset($_GET['class_id']) && isset($_GET['stream_id'])) {
            $stream_id = $_GET['stream_id'];
            $term_id = $_GET['term_id'];
            $subject_id = $_GET['subject_id'];
            $year_id = $_GET['year_id'];
            $class_id = $_GET['class_id'];
            $condition = "subject_id='$subject_id' AND term_id='$term_id' AND year_id='$year_id' AND class_id='$class_id' AND "
                    . "pupil_id IN (SELECT pupil_id FROM pupils WHERE stream_id=$stream_id)";
        }
        $modelsx = Marks::model()->getDistinct("pupil_id", $condition);
        $output = array();
        foreach ($modelsx as $modelx) {
            $model = Pupils::model()->loadModel($modelx['pupil_id']);
            $array = array();

            //get the marks-----------------------------------------
            $allexams = Examinationtypes::model()->findAll();

            foreach ($allexams as $exam) {
                $marks = '';
                $grade = '';
                $checkexammarks_id = Marks::model()->getScalar("exammarks_id", "subject_id='$subject_id' AND "
                        . "examinationtype_id='$exam->examinationtype_id' AND pupil_id=$model->pupil_id AND term_id='$term_id' "
                        . " AND year_id='$year_id' AND class_id='$class_id'");
                if ($checkexammarks_id > 0) {//meaning there is stuff
                    $marksmodel = Marks::model()->loadModel($checkexammarks_id);
                    $marks = $marksmodel->marks;
                    $grade = $marksmodel->grade;
                }
                $array['marks' . $exam->examinationtype_id] = $marks;
                $array['grade' . $exam->examinationtype_id] = $grade;
            }

            //  $average = number_format(($sum / $counter),2);
            $average = Marks::model()->getAverage("marks", "subject_id='$subject_id' AND "
                    . " pupil_id=$model->pupil_id AND term_id='$term_id' "
                    . " AND year_id='$year_id' AND class_id='$class_id'");
            //end marks story----------------------------------------
            $array['avgrade'] = $average > 0 ? Details::getgrade($subject_id, $average) : "";
            $array['average'] = number_format($average, 2);
            $array['pupil_id'] = $model->pupil_id;
            $array['class_id'] = $model->class_id;
            $array['stream_id'] = $model->stream_id;
            $array['adm'] = $model->adm;
            $array['fname'] = $model->fname;
            $array['lname'] = $model->lname;
            $array['othername'] = $model->othername;
            $array['name'] = $model->fname . " " . $model->lname . " " . $model->othername;
            $output[] = $array;
        }
        $theoutput = json_encode($output);
        echo $theoutput;
    }

    //-----------------------------------merit lists --------------------------------------------------------------------
    public function actionMeritlists() {
        $condition = "1=2";
        $streamcondition = "1=1";
        $subject_id = '';
        $term_id = '';
        $year_id = '';
        $class_id = '';
        $examinationtype_id = '';
        if (isset($_GET['class_id']) && isset($_GET['year_id'])) {
            $term_id = $_GET['term_id'];
            $year_id = $_GET['year_id'];
            $class_id = $_GET['class_id'];
            $examinationtype_id = $_GET['examinationtype_id'];
            $condition = " examinationtype_id='$examinationtype_id' AND term_id='$term_id' AND year_id='$year_id' AND "
                    . " class_id='$class_id'";
        }


        $models = Marks::model()->getDistinct("pupil_id", "$condition");
        $output = array();
        foreach ($models as $model) {
            $array = array();
            $array['adm'] = Pupils::model()->get($model['pupil_id'], 'adm');
            $array['stream_id'] = Pupils::model()->get($model['pupil_id'], 'stream_id');
            $array['name'] = Pupils::model()->get($model['pupil_id'], "CONCAT(fname, ' ',lname)");
            $allsubjects = Subjects::model()->findAll();
            $totalmarks = 0;
            //marks nd position for class
            foreach ($allsubjects as $subject) {
                $marks = '';
                $grade = '';
                $exammarks_id = Marks::model()->getScalar("exammarks_id", "subject_id='$subject->subject_id' AND "
                        . "examinationtype_id=' $examinationtype_id ' AND pupil_id=" . $model['pupil_id']
                        . " AND term_id='$term_id' AND year_id='$year_id' AND class_id='$class_id'");
                if ($exammarks_id > 0) {//meaning there is stuff
                    $marksmodel = Marks::model()->loadModel($exammarks_id);
                    $marks = $marksmodel->marks;
                    $grade = $marksmodel->grade;
                    $array['marks' . $subject->subject_id] = $marks;
                    $totalmarks = (int) $totalmarks + (int) $marks;
                    $array['grade' . $subject->subject_id] = $grade;
                }
            }
            $array['tmarks'] = $totalmarks;
            //----------------------------------look for stream marks---------------------------------------
            $streamofstudent = Pupils::model()->get($model['pupil_id'], "stream_id");
            $models2 = Marks::model()->getDistinct("pupil_id", "$condition  AND pupil_id IN (SELECT pupil_id FROM pupils WHERE stream_id=$streamofstudent)");
            $markscollector = array();
            foreach ($models2 as $model2) {
                $streamtotalmarks = 0;
                foreach ($allsubjects as $subject) {//all subjects for a particular pupil 23 marks,45 marks
                    $exammarks_id = Marks::model()->getScalar("exammarks_id", "subject_id='$subject->subject_id' AND "
                            . "examinationtype_id=' $examinationtype_id ' AND pupil_id=" . $model2['pupil_id']
                            . " AND term_id='$term_id' AND year_id='$year_id' AND class_id='$class_id'");
                    if ($exammarks_id > 0) {//meaning there is stuff
                        $marksmodel = Marks::model()->loadModel($exammarks_id);
                        $streamsubjectmarks = $marksmodel->marks;
                        $streamtotalmarks = (int) $streamtotalmarks + (int) $streamsubjectmarks;
                    }
                }
                $markscollector[$model2['pupil_id']] = $streamtotalmarks;
            }

            arsort($markscollector);
            $prevposition2 = 0;
            $prevmark2 = -1;
            $mypos = -1;
            $pos2 = 1;
            foreach ($markscollector as $key2 => $value2) {
                $currentmark2 = $value2;
                if ($currentmark2 == $prevmark2) {
                    $mypos = $prevposition2;
                } else {
                    $mypos = $pos2;
                    $prevposition2 = $pos2;
                }
                if ($key2 == $model['pupil_id']) {
                    $array['stream_position'] = $mypos;
                    break;
                }
                $prevmark2 = $currentmark2;
                $pos2 = $pos2 + 1;
            }
//---------------------finished stream marks and position------------------------------------------
            $output[] = $array;
        }
        $finalarray = array();
        if ($output) {
            //arrange the array and start looking for positions
            foreach ($output as $key => $row) {
                $ttmarks[$key] = $row['tmarks'];
            }
            array_multisort($ttmarks, SORT_DESC, $output);
            //now look for positions
            $prevposition = 0;
            $prevmark = -1;
            foreach ($output as $key => $value) {
                $pos = $key + 1;
                $currentmark = $value['tmarks'];
//find out if the curent mark is equal to the previous
                if ($currentmark == $prevmark) {
                    $finalarray[] = array_merge($value, array("class_position" => $prevposition));
                } else {
                    $finalarray[] = array_merge($value, array("class_position" => $pos));
                    $prevposition = $pos;
                }
                $prevmark = $currentmark;
            }
        }
        echo json_encode($finalarray);
    }

    public function actionSubjectteachers() {
        $models = Subjectteachers::model()->findAll();
        $output = array();
        foreach ($models as $model) {
            $array = array();
            $array['subjectteacher_id'] = $model->subjectteacher_id;
            $array['subject_id'] = $model->subject_id;
            $array['user_id'] = $model->user_id;
            $array['class_id'] = $model->class_id;
            $array['stream_id'] = $model->stream_id;
            $output[] = $array;
        }
        $theoutput = json_encode($output);
        echo $theoutput;
    }

    public function actionClosingopeningdate() {
        $models = Closingopeningdate::model()->findAll();
        $output = array();
        foreach ($models as $model) {
            $array = array();
            $array['closingopening_id'] = $model->closingopening_id;
            $array['closingdate'] = $model->closingdate;
            $array['openingdate'] = $model->openingdate;
            $output[] = $array;
        }
        $theoutput = json_encode($output);
        echo $theoutput;
    }

    public function actionReportcards() {
        $condition = "1=2";
        $term_id = '';
        $stream_id = '';
        $year_id = '';
        $class_id = '';
        $output = array();
        if (isset($_GET['class_id']) && isset($_GET['year_id'])) {
            $term_id = $_GET['term_id'];
            $year_id = $_GET['year_id'];
            $class_id = $_GET['class_id'];
            //  $stream_id = $_GET['stream_id'];
            //  $streamcond = $stream_id > 0 ? "pupil_id IN (SELECT pupil_id FROM pupils WHERE stream_id='$stream_id') AND " : "";
            $streamcond = "";
            $condition = "$streamcond   term_id='$term_id' AND year_id='$year_id' AND class_id='$class_id' ORDER BY class_position";
            $condition2 = "$streamcond   term_id='$term_id' AND year_id='$year_id' AND class_id='$class_id'";
            $allpupils = Termlyexam::model()->getDistinct("pupil_id", $condition);
            foreach ($allpupils as $pupil) {
                $array = array();
                $pupil_id = $pupil['pupil_id'];
                $pupilmodel = Pupils::model()->loadModel($pupil_id);
                $adm = $pupilmodel->adm;
                $name = $pupilmodel->fname . " " . $pupilmodel->lname . " " . $pupilmodel->othername;
                $array['adm'] = $adm;
                $array['pupil_id'] = $pupil_id;
                $array['name'] = $name;
                $array['stream_id'] = $pupilmodel->stream_id;

                $allsubjects = Subjects::model()->findAll();
                $myarray = array();
                foreach ($allsubjects as $subject) {

                    $markcondition = "pupil_id='$pupil_id' AND subject_id='$subject->subject_id' AND " . $condition2;
                    $marks = Averagemarks::model()->getScalar("marks", $markcondition);
                    $grade = Averagemarks::model()->getScalar("grade", $markcondition);
                    $newmarks = $marks > 0 ? (int) $marks : "";
                    $newgrades = $grade ? $grade : "";
                    $array["marks$subject->subject_id"] = $newmarks;
                    $array["grade$subject->subject_id"] = $newgrades;
                }

                //total marks grades and positions
                $totalmarks = Termlyexam::model()->getScalar("totalmarks", "pupil_id=$pupil_id AND " . $condition2);
                $classposition = Termlyexam::model()->getScalar("class_position", "pupil_id=$pupil_id AND " . $condition2);
                $streamposition = Termlyexam::model()->getScalar("stream_position", "pupil_id=$pupil_id AND " . $condition2);
                $array["tmarks"] = (int) $totalmarks;
                $array["class_position"] = $classposition;
                $array["stream_position"] = $streamposition;

                $output[] = $array;
            }
        }
        echo json_encode($output);
    }

    public function actionExpenses() {
        $models = Expenses::model()->findAll();
        $output = array();
        foreach ($models as $model) {
            $array = array();
            $array['expense_id'] = $model->expense_id;
            $array['details'] = $model->details;
            $array['date'] = $model->date;
            $array['amount'] = $model->amount;
            $array['toname'] = $model->toname;
            $array['expensetype_id'] = $model->expensetype_id;
            $array['bank_id'] = $model->bank_id;
            $array['user_id'] = $model->user_id;
            $array['paymenttype_id'] = $model->paymenttype_id;
            $array['cheque_no'] = $model->cheque_no;
            $output[] = $array;
        }
        $theoutput = json_encode($output);
        echo $theoutput;
    }

    public function actionExpensesincome() {
        $models = Expenses::model()->findAll();
        $output = array();
        foreach ($models as $model) {
            $array = array();
            $expense = Expensetypes::model()->get($model->expensetype_id, "expensetype");
            $array['details'] = $model->toname . "-" . $expense;
            $array['date'] = $model->date;
            $array['credit_amount'] = $model->amount;
            $array['expense_id'] = $model->expense_id;
            $array['bank_id'] = $model->bank_id;
            $array['paymenttype_id'] = $model->paymenttype_id;
            $output[] = $array;
        }
        $feespayments = FeesTransactions::model()->findAll();
        foreach ($feespayments as $feespayment) {
            $array2 = array();
            $array2['details'] = "Fees Payment";
            $array2['debit_amount'] = $feespayment->amount;
            $array2['date'] = $feespayment->date;
            $array2['bank_id'] = $feespayment->bank_id;
            $array2['paymenttype_id'] = $feespayment->paymenttype_id;
            $output[] = $array2;
        }
        //start income
        $incomes = Income::model()->findAll();
        foreach ($incomes as $income) {
            $array3 = array();
            $incometype = Incometypes::model()->get($income->incometype_id, "incometype");
            $array3['details'] = $income->fromname . "-" . $incometype;
            $array3['date'] = $income->date;
            $array3['debit_amount'] = $income->amount;
            $array3['bank_id'] = $income->bank_id;
            $array3['paymenttype_id'] = $income->paymenttype_id;
            $output[] = $array3;
        }

        //sort the output based on date i descending order
        foreach ($output as $key => $row) {
            $date[$key] = $row['date'];
        }

// Sort the data with volume descending, edition ascending
// Add $data as the last parameter, to sort by the common key
        array_multisort($date, SORT_DESC, $output);

        $theoutput = json_encode($output);
        echo $theoutput;
    }

    public function actionExpensetypes() {
        $models = Expensetypes::model()->findAll();
        $output = array();
        foreach ($models as $model) {
            $array = array();
            $array['expensetype_id'] = $model->expensetype_id;
            $array['expensetype'] = $model->expensetype;
            $output[] = $array;
        }
        $theoutput = json_encode($output);
        echo $theoutput;
    }

    public function actionIncometypes() {
        $models = Incometypes::model()->findAll();
        $output = array();
        foreach ($models as $model) {
            $array = array();
            $array['incometype_id'] = $model->incometype_id;
            $array['incometype'] = $model->incometype;
            $output[] = $array;
        }
        $theoutput = json_encode($output);
        echo $theoutput;
    }

    public function actionIncome() {
        $models = Income::model()->findAll();
        $output = array();
        foreach ($models as $model) {
            $array = array();
            $array['income_id'] = $model->income_id;
            $array['details'] = $model->details;
            $array['incometype_id'] = $model->incometype_id;
            $array['date'] = $model->date;
            $array['amount'] = $model->amount;
            $array['fromname'] = $model->fromname;
            $array['bank_id'] = $model->bank_id;
            $array['user_id'] = $model->user_id;
            $array['paymenttype_id'] = $model->paymenttype_id;
            $array['cheque_no'] = $model->cheque_no;
            $output[] = $array;
        }
        $theoutput = json_encode($output);
        echo $theoutput;
    }

    public function actionSubjectanalysis() {
        $condition = "1=2";
        $streamcondition = "1=1";
        $term_id = '';
        $year_id = '';
        $class_id = '';
        $output = array();
        $examinationtype_id = '';
        if (isset($_GET['class_id']) && isset($_GET['year_id'])) {
            $term_id = $_GET['term_id'];
            $year_id = $_GET['year_id'];
            $class_id = $_GET['class_id'];
            $examinationtype_id = $_GET['examinationtype_id'];
            $condition = " examinationtype_id='$examinationtype_id' AND term_id='$term_id'"
                    . " AND year_id='$year_id' AND  class_id='$class_id'";


            //-------------------------Entry------------------------------------------
            $array = array();
            $array['analysis'] = 'Entry';
            $subjects = Subjects::model()->findAll();
            foreach ($subjects as $subject) {
                $subject_id = $subject->subject_id;
                $totalmarks = Marks::model()->count("marks", $condition . " AND subject_id=$subject_id AND marks IS NOT NULL");
                $array['subject' . $subject->subject_id] = $totalmarks;
            }
            $array['ttmarks'] = ' ';
            $array['mscore'] = ' ';

            $output[] = $array;
            //--------------------------------total marks-------------------------------
            $array = array();
            $array['analysis'] = 'Total Marks';
            $themarks = 0;
            foreach ($subjects as $subject) {
                $subject_id = $subject->subject_id;
                $totalmarks = Marks::model()->getSum("marks", $condition . " AND subject_id=$subject_id");
                $array['subject' . $subject->subject_id] = $totalmarks;
                $themarks = $themarks + $totalmarks;
            }
            $array['ttmarks'] = $themarks>0?$themarks:" ";
            $array['mscore'] = ' ';
            $output[] = $array;
            //-----------------------------Mean Score-------------------------------
            $array = array();
            $array['analysis'] = 'Mean Score';
            $themarks = 0;
            foreach ($subjects as $subject) {
                $subject_id = $subject->subject_id;
                $entry = Marks::model()->count("marks", $condition . " AND subject_id=$subject_id AND marks IS NOT NULL");
                $entry = $entry ? $entry : 1;
                $totalmarks = Marks::model()->getSum("marks", $condition . " AND subject_id=$subject_id");
                $array['subject' . $subject->subject_id] =$totalmarks>0? Round(($totalmarks / $entry), 2):" ";
                $themarks = $themarks + $totalmarks;
            }
            $array['ttmarks'] = " ";
            $entrymain = Marks::model()->count("marks", $condition . " AND subject_id=1 AND marks IS NOT NULL");
            $array['mscore'] = Round(($themarks / $entrymain), 2);
            $output[] = $array;
            //-----------------------------Deviation------------------------------
            $array = array();
            if ($examinationtype_id == 1 && $term_id == 1) {//meaning first examination in that year
                $examinationtype_id = 4;
                $currentyear = Years::model()->get($year_id, "year");
                $oldyear = $currentyear - 1;
                $year_id = Years::model()->getScalar("year_id", "year=$oldyear");
                $class_id = $class_id - 1;
                 $term_id=3;
            } else {
                $examinationtype_id = $examinationtype_id == 1 ? $examinationtype_id = 4 : $examinationtype_id - 1;
            }
            $array['analysis'] = 'Mean Score Deviation';
            $themarks = 0;
               $prevthemarks = 0;
             $prevcondition = " examinationtype_id='$examinationtype_id' AND term_id='$term_id' AND year_id='$year_id' AND  class_id='$class_id'";
            
             foreach ($subjects as $subject) {
                $subject_id = $subject->subject_id;
                $entry = Marks::model()->count("marks", $condition . " AND subject_id=$subject_id AND marks IS NOT NULL");
                $entry = $entry ? $entry : 1;
                $totalmarks = Marks::model()->getSum("marks", $condition . " AND subject_id=$subject_id");
                
                $preventry = Marks::model()->count("marks", $prevcondition . " AND subject_id=$subject_id AND marks IS NOT NULL");
                $preventry = $entry ? $entry : 1;
                $prevtotalmarks = Marks::model()->getSum("marks", $prevcondition . " AND subject_id=$subject_id");
                $meanscore= Round(($totalmarks / $entry), 2);
                $prevmeanscore= Round(($prevtotalmarks / $preventry), 2);
                $deviation=    $meanscore-$prevmeanscore;
                $deviation=$deviation>0?"".$deviation:$deviation;
                $array['subject' . $subject->subject_id] =$deviation?$deviation:" ";
                $themarks = $themarks + $totalmarks;
                $prevthemarks=$prevthemarks+$prevtotalmarks;
            }
            $lastcondition="";
            $array['ttmarks'] = " ";
            $entrymain = Marks::model()->count("marks", $condition . " AND subject_id=1 AND marks IS NOT NULL");
            $meanscore= Round(($themarks / $entrymain), 2);
              
            $preventrymain = Marks::model()->count("marks", $prevcondition . " AND subject_id=1 AND marks IS NOT NULL");
            $prevthemeanscore = Round(($prevthemarks / $preventrymain), 2);
            $prevmeanscore=$meanscore-$prevthemeanscore; 
            $array['mscore'] = $prevmeanscore>0?"".$prevmeanscore:$prevmeanscore; 
            
            $output[] = $array;
        }

        echo json_encode($output);
    }
       public function actionReportcardsubjectanalysis() {
        $condition = "1=2";
        $streamcondition = "1=1";
        $term_id = '';
        $year_id = '';
        $class_id = '';
        $output = array();
     //   $examinationtype_id = '';
        if (isset($_GET['class_id']) && isset($_GET['year_id'])) {
            $term_id = $_GET['term_id'];
            $year_id = $_GET['year_id'];
            $class_id = $_GET['class_id'];
            //$examinationtype_id = $_GET['examinationtype_id'];
            $condition = " term_id='$term_id'"
                    . " AND year_id='$year_id' AND  class_id='$class_id'";


            //-------------------------Entry------------------------------------------
            $array = array();
            $array['analysis'] = 'Entry';
            $subjects = Subjects::model()->findAll();
            foreach ($subjects as $subject) {
                $subject_id = $subject->subject_id;
                $totalmarks = Averagemarks::model()->count("marks", $condition . " AND subject_id=$subject_id AND marks IS NOT NULL");
                $array['subject' . $subject->subject_id] = $totalmarks;
            }
            $array['ttmarks'] = ' ';
            $array['mscore'] = ' ';

            $output[] = $array;
            //--------------------------------total marks-------------------------------
            $array = array();
            $array['analysis'] = 'Total Marks';
            $themarks = 0;
            foreach ($subjects as $subject) {
                $subject_id = $subject->subject_id;
                $totalmarks = Averagemarks::model()->getSum("marks", $condition . " AND subject_id=$subject_id");
                $array['subject' . $subject->subject_id] = $totalmarks;
                $themarks = $themarks + $totalmarks;
            }
            $array['ttmarks'] = $themarks>0?$themarks:" ";
            $array['mscore'] = ' ';
            $output[] = $array;
            //-----------------------------Mean Score-------------------------------
            $array = array();
            $array['analysis'] = 'Mean Score';
            $themarks = 0;
            foreach ($subjects as $subject) {
                $subject_id = $subject->subject_id;
                $entry = Averagemarks::model()->count("marks", $condition . " AND subject_id=$subject_id AND marks IS NOT NULL");
                $entry = $entry ? $entry : 1;
                $totalmarks = Averagemarks::model()->getSum("marks", $condition . " AND subject_id=$subject_id");
                $array['subject' . $subject->subject_id] =$totalmarks>0? Round(($totalmarks / $entry), 2):" ";
                $themarks = $themarks + $totalmarks;
            }
            $array['ttmarks'] = " ";
            $entrymain = Averagemarks::model()->count("marks", $condition . " AND subject_id=1 AND marks IS NOT NULL");
            $array['mscore'] = Round(($themarks / $entrymain), 2);
            $output[] = $array;
            //-----------------------------Deviation------------------------------
            $array = array();
            if ($term_id == 1) {//meaning first examination in that year
                $currentyear = Years::model()->get($year_id, "year");
                $oldyear = $currentyear - 1;
                $year_id = Years::model()->getScalar("year_id", "year=$oldyear");
                $class_id = $class_id - 1;
                $term_id=3;
            }
            $array['analysis'] = 'Mean Score Deviation';
            $themarks = 0;
               $prevthemarks = 0;
             $prevcondition = " term_id='$term_id' AND year_id='$year_id' AND  class_id='$class_id'";
            
             foreach ($subjects as $subject) {
                $subject_id = $subject->subject_id;
                $entry = Averagemarks::model()->count("marks", $condition . " AND subject_id=$subject_id AND marks IS NOT NULL");
                $entry = $entry ? $entry : 1;
                $totalmarks = Averagemarks::model()->getSum("marks", $condition . " AND subject_id=$subject_id");
                
                $preventry = Averagemarks::model()->count("marks", $prevcondition . " AND subject_id=$subject_id AND marks IS NOT NULL");
                $preventry = $entry ? $entry : 1;
                $prevtotalmarks = Averagemarks::model()->getSum("marks", $prevcondition . " AND subject_id=$subject_id");
                $meanscore= Round(($totalmarks / $entry), 2);
                $prevmeanscore= Round(($prevtotalmarks / $preventry), 2);
                $deviation=    $meanscore-$prevmeanscore;
                $deviation=$deviation>0?"".$deviation:$deviation;
                $array['subject' . $subject->subject_id] =$deviation?$deviation:" ";
                $themarks = $themarks + $totalmarks;
                $prevthemarks=$prevthemarks+$prevtotalmarks;
            }
            $lastcondition="";
            $array['ttmarks'] = " ";
            $entrymain = Averagemarks::model()->count("marks", $condition . " AND subject_id=1 AND marks IS NOT NULL");
            $meanscore= Round(($themarks / $entrymain), 2);
              
            $preventrymain = Averagemarks::model()->count("marks", $prevcondition . " AND subject_id=1 AND marks IS NOT NULL");
            $prevthemeanscore = Round(($prevthemarks / $preventrymain), 2);
            $prevmeanscore=$meanscore-$prevthemeanscore; 
            $array['mscore'] = $prevmeanscore>0?"".$prevmeanscore:$prevmeanscore; 
            
            $output[] = $array;
        }

        echo json_encode($output);
    }

}
