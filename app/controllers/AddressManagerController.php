<?php

class AddressManagerController extends Controller
{

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl',
            'postOnly + delete',
            'ajaxOnly + loadAddress',
        );
    }
    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'actions' => array('loadAddress', 'create', 'update', 'delete', 'getCities'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     *
     * @param type $type : Model_class_name
     * @param type $parent_id
     * @param type $id
     */
    public function actionLoadAddress($type, $parent_id, $route, $id = null)
    {
        if (!empty($id)) {
            $model = $type::model()->loadModel($id);
        } else if (!empty($parent_id)) {
            $model = $type::model()->getPreferredAddress($parent_id);
        } else {
            Yii::app()->end();
        }
        $this->renderPartial('_view', array('model' => $model, 'route' => $route), false, true);
    }

    /**
     * Add address
     * @param type $type
     * @param type $parent_id
     */
    public function actionCreate($type, $parent_id = null)
    {

        $this->pageTitle = Lang::t(Constants::LABEL_CREATE . ' address');

        $model = new $type();
        if (!empty($model->parent_id_field))
            $model->{$model->parent_id_field} = $parent_id;
        $model_class_name = $model->getClassName();

        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            $error_message = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error_message);
            if (!empty($error_message_decoded)) {
                echo CJSON::encode(array('success' => false, 'message' => $error_message));
            } else {
                $model->save(FALSE);
                echo CJSON::encode(array('success' => true, 'message' => Lang::t('SUCCESS_MESSAGE'), 'redirectUrl' => UrlManager::getReturnUrl(Yii::app()->createUrl($this->getAfterSaveRoute(), array('id' => $parent_id, AddressManager::ADDRESS_ID_GET_PARAM => $model->id)))));
            }
            Yii::app()->end();
        }

        $this->renderPartial('_form', array('model' => $model), FALSE, TRUE);
    }

    /**
     *
     * @param string $type model_class_name
     * @param type $id
     */
    public function actionUpdate($type, $id)
    {
        $this->pageTitle = Lang::t(Constants::LABEL_UPDATE . ' address');

        $model = $type::model()->loadModel($id);
        $model_class_name = $model->getClassName();

        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            $error_message = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error_message);
            if (!empty($error_message_decoded)) {
                echo CJSON::encode(array('success' => false, 'message' => $error_message));
            } else {
                $model->save(FALSE);
                $parent_id = !empty($model->parent_id_field) ? $model->{$model->parent_id_field} : null;
                echo CJSON::encode(array('success' => true, 'message' => Lang::t('SUCCESS_MESSAGE'), 'redirectUrl' => UrlManager::getReturnUrl(Yii::app()->createUrl($this->getAfterSaveRoute(), array('id' => $parent_id, AddressManager::ADDRESS_ID_GET_PARAM => $model->id)))));
            }
            Yii::app()->end();
        }

        $this->renderPartial('_form', array('model' => $model), FALSE, TRUE);
    }
    /**
     *
     * @param string $type model_class_name
     * @param type $id
     */
    public function actionDelete($type, $id)
    {
        //redirectUrl
        if (Yii::app()->request->isPostRequest) {
            $model = $type::model()->loadModel($id);
            $parent_id = !empty($model->parent_id_field) ? $model->{$model->parent_id_field} : null;
            $model->delete();
            echo CJSON::encode(array('redirectUrl' => Yii::app()->createUrl($this->getAfterSaveRoute(), array('id' => $parent_id))));
        }
    }

    /**
     * Get cities
     * @param type $country_id
     */
    public function actionGetCities($country_id)
    {
        $citites = SettingsCity::model()->getData('id,name', '`country_id`=:t1', array(':t1' => $country_id));
        echo CJSON::encode($citites);
    }

    protected function getAfterSaveRoute()
    {
        $route = filter_input(INPUT_GET, AddressManager::AFTERSAVE_GO_TO_ROUTE_GET_PARAM);
        if (empty($route))
            throw new CHttpException(400, Lang::t('400_error'));
        return $route;
    }

}
