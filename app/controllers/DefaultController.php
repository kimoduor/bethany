<?php

/**
 * Home controller
 * @author Fred<mconyango@gmail.com>
 */
class DefaultController extends Controller {

    public function init() {
        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users
                'actions' => array('firststep', 'header', 'sidebar', 'footer', 'pagehead', 'registration', 'createmember'),
                'users' => array('*'),
            ),
            array('allow',
                'actions' => array('index', 'uploads', 'verification', '_reportCSV', 'summaryreport', 'supplierreport', 'redeemed', 'paymentapproval', 'supplierinvoices', 'supplier', 'query', 'invoicevalidation', 'invoicevalidationforagrodealer', 'agrodealerinputs', 'confirm', 'supplierapprovals', 'suppliersearch', 'inputsummaryreport',
                    'messages', 'messagesreplied', 'messagereplydelete', 
                    'messagesreply', 'messagedelete', 'chat', 'chart', 
                    'viewredemptions', 'evoucherinputs', 'evouchersupplierperformance', 'dashboard',
                    'evoucheragrodealerinputs'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    public function actionHeader() {
        $this->renderPartial('tpl/header');
    }
    public function actionDashboard() {
        $this->renderPartial('dashboard');
    }


    public function actionCreatemember() {
$usermodel = new Users(ActiveRecord::SCENARIO_CREATE);
        if (isset($_POST['fname']) && isset($_POST['lname']) && isset($_POST['email']) && isset($_POST['mobile'])) {
                $usermodel->fname = $_POST['fname'] ;
                $usermodel->lname =  $_POST['lname'];
                $usermodel->username =$_POST['email'];
                $usermodel->email = $_POST['email'];
                $usermodel->phone = $_POST['mobile'];
                $salt = Common::generateSalt();
                // $usermodel->setScenario(Users::SCENARIO_UPDATE);
                $usermodel->password = $salt . md5($_POST['password']);
                // var_dump($usermodel->password);
                $usermodel->salt = $salt;
                $usermodel->user_level = 3;
                $usermodel->role_id = 3;
                $error_message = CActiveForm::validate($usermodel);
                $error_message_decoded = json_decode($error_message);
                if (empty($error_message_decoded)) {
                    $usermodel->save();
                    $usermodel->password = $salt . md5($_POST['password']);
                    $usermodel->salt = $salt;
                    $usermodel->save();
                    echo json_encode(array('operation' => "success"));
                } else {
                    $errorlist = "<br />";
                    foreach ($error_message_decoded as $error) {
                        $errorlist.=$error[0] . '<br />';
                    }
                    echo json_encode(array("operation" => "error", "the_errors" => $errorlist));
                }

            }
        
        else {
            $this->render('registration/index', array('model' => $usermodel));
        }
    }

    public function actionSidebar() {
        $this->renderPartial('tpl/sidebar');
    }

    public function actionFooter() {
        $this->renderPartial('tpl/footer');
    }

    public function actionPagehead() {
        $this->renderPartial('tpl/pagehead');
    }

    public function actionRegistration() {

        $this->render('registration/index');
    }

    public function actionViewredemptions() {
        $condition = ' agrodealer_id=' . Yii::app()->user->agrodealer_id;
        $this->render('viewredemptions', array(
            'model' => Postransactions::model()->searchModel(array(), $this->settings[SettingsModuleConstants::SETTINGS_ITEMS_PER_PAGE], '', $condition),
        ));
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionChart() {

        if (Yii::app()->user->user_level === UserLevels::LEVEL_AGRODEALER) {
            $this->redirect(array('verification'));
        } elseif (Yii::app()->user->user_level === UserLevels::LEVEL_DISTRICTME) {
            //  echo "I ak he";
            $this->redirect(array('verification'));
        } elseif (Yii::app()->user->user_level === UserLevels::LEVEL_PROVINCEME) {
            //  echo "I ak he";
            $this->redirect(array('verification'));
        }

        $this->pageTitle = Lang::t('Dashboard');
        $this->activeMenu = 1;

        //Get data from model
        $season_id = null;
        if (isset($_POST['Vwfarmerredemption']['season_id']) && $_POST['Vwfarmerredemption']['season_id'] != '') {
            $season_id = $_POST['Vwfarmerredemption']['season_id'];
        } else {
            $season_id = Agriseason::model()->getScalar('season_id', 'active=1');
        }
        $province = Vwfarmerredemption::getRedeemedVoucherByProvince($season_id);
        $national = Vwfarmerredemption::getNationalRedemption($season_id);
        $type = Vwfarmerredemption::getRedeemedVoucherType($season_id);
        $gender = Vwfarmerredemption::getRedeemedVoucherGender($season_id);

        $this->render('chart', array(
            'province' => $province,
            'national' => $national,
            'type' => $type,
            'gender' => $gender
        ));
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        $this->pageTitle = Lang::t('Dashboard');
        $this->activeMenu = 1;


        $this->render('index', array(
        ));
    }

    public function actionFirststep() {
        
    }

}
