<?php

/**
 * This is the model class for table "examaveragemarks".
 *
 * The followings are the available columns in table 'examaveragemarks':
 * @property string $avexammarks_id
 * @property string $subject_id
 * @property string $pupil_id
 * @property string $term_id
 * @property string $year_id
 * @property string $class_id
 * @property double $marks
 * @property string $grade
 * @property integer $subjposition
 *
 * The followings are the available model relations:
 * @property Classes $class
 * @property Pupils $pupil
 * @property Subjects $subject
 * @property Terms $term
 * @property Years $year
 */
class Averagemarks extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'examaveragemarks';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('subject_id, pupil_id, term_id, year_id, class_id', 'required'),
			array('subjposition', 'numerical', 'integerOnly'=>true),
			array('marks', 'numerical'),
			array('subject_id, pupil_id, term_id, year_id, class_id', 'length', 'max'=>11),
			array('grade', 'length', 'max'=>4),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('avexammarks_id, subject_id, pupil_id, term_id, year_id, class_id, marks, grade, subjposition', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'class' => array(self::BELONGS_TO, 'Classes', 'class_id'),
			'pupil' => array(self::BELONGS_TO, 'Pupils', 'pupil_id'),
			'subject' => array(self::BELONGS_TO, 'Subjects', 'subject_id'),
			'term' => array(self::BELONGS_TO, 'Terms', 'term_id'),
			'year' => array(self::BELONGS_TO, 'Years', 'year_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'avexammarks_id' => 'Avexammarks',
			'subject_id' => 'Subject',
			'pupil_id' => 'Pupil',
			'term_id' => 'Term',
			'year_id' => 'Year',
			'class_id' => 'Class',
			'marks' => 'Marks',
			'grade' => 'Grade',
			'subjposition' => 'Subjposition',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('avexammarks_id',$this->avexammarks_id,true);
		$criteria->compare('subject_id',$this->subject_id,true);
		$criteria->compare('pupil_id',$this->pupil_id,true);
		$criteria->compare('term_id',$this->term_id,true);
		$criteria->compare('year_id',$this->year_id,true);
		$criteria->compare('class_id',$this->class_id,true);
		$criteria->compare('marks',$this->marks);
		$criteria->compare('grade',$this->grade,true);
		$criteria->compare('subjposition',$this->subjposition);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Averagemarks the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
