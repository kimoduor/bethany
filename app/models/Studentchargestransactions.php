<?php

/**
 * This is the model class for table "studentchargestransactions".
 *
 * The followings are the available columns in table 'studentchargestransactions':
 * @property string $studentchargetransactions_id
 * @property string $studentcharge_id
 * @property string $transaction_id
 * @property double $amount
 * @property string $datecreated
 */
class Studentchargestransactions extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'studentchargestransactions';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('studentcharge_id, amount', 'required'),
			array('amount', 'numerical'),
			array('studentcharge_id, transaction_id', 'length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('studentchargetransactions_id, studentcharge_id, transaction_id, amount, datecreated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'studentchargetransactions_id' => 'Studentchargetransactions',
			'studentcharge_id' => 'Studentcharge',
			'transaction_id' => 'Transaction',
			'amount' => 'Amount',
			'datecreated' => 'Datecreated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('studentchargetransactions_id',$this->studentchargetransactions_id,true);
		$criteria->compare('studentcharge_id',$this->studentcharge_id,true);
		$criteria->compare('transaction_id',$this->transaction_id,true);
		$criteria->compare('amount',$this->amount);
		$criteria->compare('datecreated',$this->datecreated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Studentchargestransactions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
