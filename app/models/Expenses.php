<?php

/**
 * This is the model class for table "expenses".
 *
 * The followings are the available columns in table 'expenses':
 * @property string $expense_id
 * @property string $expensetype_id
 * @property string $details
 * @property double $amount
 * @property string $user_id
 * @property string $paymenttype_id
 * @property string $bank_id
 * @property string $cheque_no
 * @property string $toname
 * @property string $date
 */
class Expenses extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'expenses';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('expensetype_id,amount, user_id', 'required'),
			array('amount', 'numerical'),
			array('expensetype_id, user_id, paymenttype_id, bank_id', 'length', 'max'=>11),
			array('cheque_no, toname', 'length', 'max'=>255),
			array('date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('expense_id, expensetype_id, details, amount, user_id, paymenttype_id, bank_id, cheque_no, toname, date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'expense_id' => 'Expense',
			'expensetype_id' => 'Expensetype',
			'details' => 'Details',
			'amount' => 'Amount',
			'user_id' => 'User',
			'paymenttype_id' => 'Paymenttype',
			'bank_id' => 'Bank',
			'cheque_no' => 'Cheque No',
			'toname' => 'Toname',
			'date' => 'Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('expense_id',$this->expense_id,true);
		$criteria->compare('expensetype_id',$this->expensetype_id,true);
		$criteria->compare('details',$this->details,true);
		$criteria->compare('amount',$this->amount);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('paymenttype_id',$this->paymenttype_id,true);
		$criteria->compare('bank_id',$this->bank_id,true);
		$criteria->compare('cheque_no',$this->cheque_no,true);
		$criteria->compare('toname',$this->toname,true);
		$criteria->compare('date',$this->date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Expenses the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
