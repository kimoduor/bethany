<?php

class Details {

    public static function loanstatus($status_id) {
        switch ($status_id) {
            case 0:return "Application";
                break;
            case 1:return "Verification";
                break;
            case 2:return "Rejected";
                break;
            case 3:return "Aproved";
                break;
        }
    }

    public static function classjson() {
//class
        $modelx = Classes::model()->findAll();
        $output = array();
        $output[""] = "[Select One]";
        foreach ($modelx as $modely) {
            $output[$modely->class_id] = $modely->class;
        }
        return json_encode($output);
    }

    public static function streamjson() {
//class
        $modelx = Streams::model()->findAll();
        $output = array();
        $output[""] = "[Select One]";
        foreach ($modelx as $modely) {
            $output[$modely->stream_id] = $modely->stream;
        }
        return json_encode($output);
    }

    public static function termjson() {
//term
        $modelx = Terms::model()->findAll();
        $output = array();
        $output[""] = "[Select One]";
        foreach ($modelx as $modely) {
            $output[$modely->term_id] = $modely->term;
        }
        return json_encode($output);
    }

    public static function yearjson() {
        //year
        $modelxx = Years::model()->findAll();
        $output = array();
        $output[""] = "[Select One]";
        foreach ($modelxx as $modelyy) {
            $output[$modelyy->year_id] = $modelyy->year;
        }
        return json_encode($output);
    }

    public static function voteheadjson() {
        //year
        $modelxx = Voteheads::model()->findAll(array('condition' => 'votehead_id NOT IN (1)'));
        $output = array();
        $output[""] = "[Select One]";
        foreach ($modelxx as $modelyy) {
            $output[$modelyy->votehead_id] = $modelyy->votehead_name;
        }
        return json_encode($output);
    }

    public static function chargesjson() {
        //year
        $modelxx = Otherfeescharges::model()->findAll();
        $output = array();
        $output[""] = "[Select One]";
        foreach ($modelxx as $modelyy) {
            $output[$modelyy->charge_id] = $modelyy->chargeparticulars;
        }
        return json_encode($output);
    }

    public static function paymenttypesjson() {
        //year
        $modelxx = Paymenttypes::model()->findAll();
        $output = array();
        $output[""] = "[Select One]";
        foreach ($modelxx as $modelyy) {
            $output[$modelyy->paymenttype_id] = $modelyy->paymenttype;
        }
        return json_encode($output);
    }

    public static function banksjson() {
        //year
        $modelxx = Banks::model()->findAll();
        $output = array();
        $output[""] = "[Select One]";
        foreach ($modelxx as $modelyy) {
            $output[$modelyy->bank_id] = $modelyy->bank_name;
        }
        return json_encode($output);
    }

    public static function getbalancevoteheadsandstructure($pupil_id) {//this method checks if all the voteheads in the year rhyme with
        //the fees structure
        //year
        $error = "";
        $activeyear = Years::model()->getScalar("year_id", "active=1");
        $class_id = Pupils::model()->get($pupil_id, "class_id");
        $activeyearname = Years::model()->getScalar("year", "active=1");
        $class = Classes::model()->get($class_id, "class");
        $totalfeesstructureamountforyear = FeesStructure::model()->getSum("amount", "class_id=$class_id AND year_id=$activeyear");
        $totalofvoteheads = Voteheadyear::model()->getSum("amount", "class_id=$class_id AND year_id=$activeyear");
        if ($totalfeesstructureamountforyear == 0 || $totalofvoteheads == 0) {
            $error = "No Fees Structure or Voteheads have been Set";
        } elseif ($totalofvoteheads > $totalfeesstructureamountforyear && $totalofvoteheads > 0) {
            $difference = $totalofvoteheads - $totalfeesstructureamountforyear;
            $error = "Sorry, the Value of Voteheads is greater than the Fees structure by $difference, "
                    . "You cannot Continue Until you Balance the fees structure of Class- $class for Year- $activeyearname";
        } elseif ($totalofvoteheads < $totalfeesstructureamountforyear && $totalofvoteheads > 0) {
            $difference = $totalfeesstructureamountforyear - $totalofvoteheads;
            $error = "Sorry, the Value of Feesstructure is greater than Voteheads by $difference, "
                    . "You cannot Continue Until you Balance the fees structure of Class- $class for Year- $activeyearname";
        }
        if ($error != "") {
            return $error;
        }
    }

    public static function getvotehead($votehead_id) {
        return Voteheads::model()->get($votehead_id, "votehead_name");
    }

    public static function chargedetails($charge_id) {
        return Otherfeescharges::model()->get($charge_id, "chargeparticulars");
    }

    public static function getvoteheadyear($voteheadyear_id) {
        $votehead_id = Voteheadyear::model()->get($voteheadyear_id, "votehead_id");
        return self::getvotehead($votehead_id);
    }

    public static function studentchargedetails($studentcharge_id) {
        $charge_id = Studentcharges::model()->get($studentcharge_id, "charge_id");
        return self::chargedetails($charge_id);
    }

    public static function receiptfromtransaction($transaction) {
        if ($transaction < 99) {
            return "00" . $transaction;
        } elseif ($transaction < 999) {
            return "0" . $transaction;
        } else {
            return $transaction;
        }
    }

    public static function getgrade($subject_id, $marks) {
        $grade = "";
        $grade = Subjectgrades::model()->getScalar("grade", "`subject_id`=$subject_id AND (`frommark`<=$marks AND `tomark`>=$marks)");
        return $grade;
    }

    public static function subjectjson() {
          $modelxx = Subjects::model()->findAll();
        $output = array();
        $output[""] = "[Select One]";
        foreach ($modelxx as $modelyy) {
            $output[$modelyy->subject_id] = $modelyy->subject;
        }
        return json_encode($output);
    }
        public static function teachersjson() {
          $modelxx = Users::model()->findAll();
        $output = array();
        $output[""] = "[Select One]";
        foreach ($modelxx as $modelyy) {
            $output[$modelyy->id] = $modelyy->fname." ". $modelyy->lname;
        }
        return json_encode($output);
    }
      public static function getsubjectcomment($subject_id, $marks) {
        $comment = "";
        $comment = Subjectgrades::model()->getScalar("comment", "`subject_id`=$subject_id AND (`frommark`<='$marks' AND `tomark`>='$marks')");
        return $comment;
    }
     public static function expensetypejson() {
        //year
        $modelxx = Expensetypes::model()->findAll();
        $output = array();
        $output[""] = "[Select One]";
        foreach ($modelxx as $modelyy) {
            $output[$modelyy->expensetype_id] = $modelyy->expensetype;
        }
        return json_encode($output);
    }
      public static function incometypejson() {
        //year
        $modelxx = Incometypes::model()->findAll();
        $output = array();
        $output[""] = "[Select One]";
        foreach ($modelxx as $modelyy) {
            $output[$modelyy->incometype_id] = $modelyy->incometype;
        }
        return json_encode($output);
    }
      public static function housejson() {
        //year
        $modelxx = Houses::model()->findAll();
        $output = array();
        $output[""] = "[Select One]";
        foreach ($modelxx as $modelyy) {
            $output[$modelyy->house_id] = $modelyy->house;
        }
        return json_encode($output);
    }
  
}
