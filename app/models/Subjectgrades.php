<?php

/**
 * This is the model class for table "examsubjectgrades".
 *
 * The followings are the available columns in table 'examsubjectgrades':
 * @property string $subjectgrade_id
 * @property string $subject_id
 * @property double $frommark
 * @property double $tomark
 * @property string $grade
 * @property string $comment
 *
 * The followings are the available model relations:
 * @property Subjects $subject
 */
class Subjectgrades extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'examsubjectgrades';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('subject_id', 'required'),
			array('frommark, tomark', 'numerical'),
			array('subject_id', 'length', 'max'=>11),
			array('grade', 'length', 'max'=>4),
			array('comment', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('subjectgrade_id, subject_id, frommark, tomark, grade, comment', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'subject' => array(self::BELONGS_TO, 'Subjects', 'subject_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'subjectgrade_id' => 'Subjectgrade',
			'subject_id' => 'Subject',
			'frommark' => 'Frommark',
			'tomark' => 'Tomark',
			'grade' => 'Grade',
			'comment' => 'Comment',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('subjectgrade_id',$this->subjectgrade_id,true);
		$criteria->compare('subject_id',$this->subject_id,true);
		$criteria->compare('frommark',$this->frommark);
		$criteria->compare('tomark',$this->tomark);
		$criteria->compare('grade',$this->grade,true);
		$criteria->compare('comment',$this->comment,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Subjectgrades the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
