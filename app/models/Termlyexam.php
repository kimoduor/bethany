<?php

/**
 * This is the model class for table "examtermlyexam".
 *
 * The followings are the available columns in table 'examtermlyexam':
 * @property string $termlyexam_id
 * @property string $year_id
 * @property string $class_id
 * @property string $pupil_id
 * @property string $term_id
 * @property string $totalmarks
 * @property integer $numberofclasspupils
 * @property integer $numberofstreampupils
 * @property integer $numberofsubjects
 * @property integer $class_position
 * @property integer $stream_position
 *
 * The followings are the available model relations:
 * @property Classes $class
 * @property Pupils $pupil
 * @property Terms $term
 * @property Years $year
 */
class Termlyexam extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'examtermlyexam';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('year_id, class_id, pupil_id, term_id', 'required'),
			array('numberofclasspupils, numberofstreampupils, numberofsubjects, class_position, stream_position', 'numerical', 'integerOnly'=>true),
			array('year_id, class_id, pupil_id, term_id', 'length', 'max'=>11),
			array('totalmarks', 'length', 'max'=>30),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('termlyexam_id, year_id, class_id, pupil_id, term_id, totalmarks, numberofclasspupils, numberofstreampupils, numberofsubjects, class_position, stream_position', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'class' => array(self::BELONGS_TO, 'Classes', 'class_id'),
			'pupil' => array(self::BELONGS_TO, 'Pupils', 'pupil_id'),
			'term' => array(self::BELONGS_TO, 'Terms', 'term_id'),
			'year' => array(self::BELONGS_TO, 'Years', 'year_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'termlyexam_id' => 'Termlyexam',
			'year_id' => 'Year',
			'class_id' => 'Class',
			'pupil_id' => 'Pupil',
			'term_id' => 'Term',
			'totalmarks' => 'Totalmarks',
			'numberofclasspupils' => 'Numberofclasspupils',
			'numberofstreampupils' => 'Numberofstreampupils',
			'numberofsubjects' => 'Numberofsubjects',
			'class_position' => 'Class Position',
			'stream_position' => 'Stream Position',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('termlyexam_id',$this->termlyexam_id,true);
		$criteria->compare('year_id',$this->year_id,true);
		$criteria->compare('class_id',$this->class_id,true);
		$criteria->compare('pupil_id',$this->pupil_id,true);
		$criteria->compare('term_id',$this->term_id,true);
		$criteria->compare('totalmarks',$this->totalmarks,true);
		$criteria->compare('numberofclasspupils',$this->numberofclasspupils);
		$criteria->compare('numberofstreampupils',$this->numberofstreampupils);
		$criteria->compare('numberofsubjects',$this->numberofsubjects);
		$criteria->compare('class_position',$this->class_position);
		$criteria->compare('stream_position',$this->stream_position);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Termlyexam the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
