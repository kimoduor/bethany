<?php

/**
 * This is the model class for table "examsubjectteachers".
 *
 * The followings are the available columns in table 'examsubjectteachers':
 * @property string $subjectteacher_id
 * @property string $subject_id
 * @property string $user_id
 * @property string $class_id
 * @property string $stream_id
 *
 * The followings are the available model relations:
 * @property Classes $class
 * @property Streams $stream
 * @property Subjects $subject
 * @property Users $user
 */
class Subjectteachers extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'examsubjectteachers';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('subject_id, user_id, class_id, stream_id', 'required'),
			array('subject_id, user_id, class_id, stream_id', 'length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('subjectteacher_id, subject_id, user_id, class_id, stream_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'class' => array(self::BELONGS_TO, 'Classes', 'class_id'),
			'stream' => array(self::BELONGS_TO, 'Streams', 'stream_id'),
			'subject' => array(self::BELONGS_TO, 'Subjects', 'subject_id'),
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'subjectteacher_id' => 'Subjectteacher',
			'subject_id' => 'Subject',
			'user_id' => 'User',
			'class_id' => 'Class',
			'stream_id' => 'Stream',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('subjectteacher_id',$this->subjectteacher_id,true);
		$criteria->compare('subject_id',$this->subject_id,true);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('class_id',$this->class_id,true);
		$criteria->compare('stream_id',$this->stream_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Subjectteachers the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
