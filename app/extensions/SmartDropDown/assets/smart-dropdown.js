/*
 * Smart DropDown
 * @author Joakim <kimoduor@gmail.com>
 */

var MySmartDropDown = {
    optionsDefault: {
        select_id: undefined,
        link_wrapper_id: undefined,
        input_wrapper_id: undefined,
        input_id: undefined,
        beforeSave: function (settings) {
        },
        afterSave: function (response, options) {
        },
    },
    options: {},
    init: function (options) {
        'use strict';
        var settings = $.extend({}, this.optionsDefault, options || {});
        this.run(settings);
    },
    run: function (options) {
        var selector = '#' + options.link_wrapper_id + ' a'
                , field_wrapper = $('#' + options.input_wrapper_id)
                , field = $('#' + options.input_id)
                , toggle_field = function () {
                    field_wrapper.toggle();
                },
                submit_form = function () {
                    //beforeSave event callback
                    options.beforeSave(options);
                    var url = $(selector).attr('data-ajax-url')
                            , form = $('#' + options.select_id).closest('form')
                            , data = form.serializeArray()
                            , error_wrapper_selector = '#' + options.input_wrapper_id + ' .errorMessage'
                            , error_wrapper = $(error_wrapper_selector);
                    $.ajax({
                        type: 'POST',
                        url: url,
                        data: data,
                        dataType: 'json',
                        success: function (response) {
                            if (response.success) {
                                //afterSave event callback
                                options.afterSave(response, options);

                                error_wrapper.hide();
                                MyUtils.populateDropDownList('#' + options.select_id, response.data, response.selected_id);
                                field.val('');
                                field_wrapper.hide();
                            }
                            else {
                                MyUtils.display_model_errors(response.message, error_wrapper_selector);
                                error_wrapper.show();
                            }
                        },
                        beforeSend: function () {
                            $('#' + options.link_wrapper_id + ' i.fa-spin').show();
                            field.attr('readonly', 'readonly');
                        },
                        complete: function () {
                            $('#' + options.link_wrapper_id + ' i.fa-spin').hide();
                            field.removeAttr('readonly');
                        },
                        error: function (XHR) {
                            var message = XHR.responseText;
                            MyUtils.display_model_errors(message, error_wrapper_selector);
                            error_wrapper.show();
                        }
                    });
                };
        //click event
        $(selector).on('click', function () {
            toggle_field();
        });
        //on blur
        field.on('blur', function () {
            submit_form();
        });
    },
}