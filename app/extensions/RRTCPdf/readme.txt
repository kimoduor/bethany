Yii extension to add Pdf generation capabilities to an application using the TCPDF library.

/**
 * RRTCPdf extension to use TCPdf in a Yii application
 * @author Rupert, Red Rabbit Studio, 2011
 * @link http://www.rabbitstyle.com
 * @copyright Red Rabbit Studio, 2011
 * @license http://www.opensource.org/licenses/mit-license.php MIT License 
 */

To use this, all you should need to do is copy the RRTCPdf.php file (and optionally the RRYiiTCPdf.php file) into your extensions folder (or anywhere else that you can reference with an alias); copy the TCPdf folder also (you can download the zip from http://sourceforge.net/projects/tcpdf/files/).

In your main.php config file, add the following in the components section:

'pdf' => array(
	'class' => 'ext.RRTCPdf',
	'mainPath' => 'ext.tcpdf',
	'wrapperAlias' => 'ext.RRYiiTCPdf',
),

Obviously "class" should give an alias to where you have put the class file, "mainPath" should be an alias for the TCPdf folder and "wrapperAlias" should be an alias for the class file that extends the TCPdf class (use the default RRYiiTCPdf or use your own class that extends TCPDF).
You can also set any default options here (see the public properties in RRTCPdf for what you can set and what the defaults are).

You should then be able to call $pdf = Yii::app()->getComponent('pdf')->create(); anywhere you want in your application, secure in the knowledge that the very bulky TCPDF class file will only ever be loaded if you need it!
