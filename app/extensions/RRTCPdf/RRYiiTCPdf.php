<?php

/**
 * RRYiiTCPdf extension to use TCPdf in a Yii application
 * @author Rupert, Red Rabbit Studio, 2011
 * @link http://www.rabbitstyle.com
 * @copyright Red Rabbit Studio, 2011
 * @license http://www.opensource.org/licenses/mit-license.php MIT License 
 */

class RRYiiTCPdf extends TCPDF {
	
	/**
	 * The header and footer methods should be modified depending on how you want your
	 * headers and footer to appear.
	 */
	public function header() {
		parent::Header();
	}
	
	public function footer() {
		parent::Footer();
	}
	
	/* 
	 * The following are simply getter methods for the setter methods in the main
	 * TCPDF class which do not have their own getter methods.
	 * In some cases the variables did not exist as simple private variables,
	 * so a calculation inverse to the setter method had to be made.
	 */
	public function getPrintHeader() {
		return $this->print_header;
	}
	
	public function getPrintFooter() {
		return $this->print_footer;
	}
	
	public function getAutoHeader() {
		return $this->AutoPageBreak;
	}
	
	public function getAutoHeaderBreak() {
		return ($this->h - $this->PageBreakTrigger);
	}
	
	public function getPdfUnit() {
		return $this->pdfunit;
	}
	
	public function getK() {
		return $this->k;
	}
	
	public function getImgScale() {
		return $this->imgscale;
	}
	
	
	/**
	 * Method to calculate a length (width or height) for an image given its pixel value,
	 * and using the appropriate DPI variables set in the object.
	 * @param integer the length in pixels
	 * @return float the length calculated in the page unit
	 */
	public function calculateImageSize($length) {
		return $this->getHTMLUnitToUnits($length, 0, $this->pdfunit, true) * $this->imgscale * $this->k;
	}
}