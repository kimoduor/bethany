<?php
/**
 * RRTCPdf extension to use TCPdf in a Yii application
 * @author Rupert, Red Rabbit Studio, 2011
 * @link http://www.rabbitstyle.com
 * @copyright Red Rabbit Studio, 2011
 * @license http://www.opensource.org/licenses/mit-license.php MIT License 
 */

class RRTCPdf extends CApplicationComponent {
	/**
	 * 
	 * Below are all the constants that are normally defined by the tcppdf_config.php
	 * file. They are included here so that they can be set in the main config array
	 * if desired, they are then parsed and all defined, unless we set 
	 * @link $useConfigFile to true, in which case these settings will be ignored and
	 * the usual config file will be loaded (it should be noted that these are the 
	 * default settings provided in the config file).
	 * The path variables should be set as Yii aliases. Any that are not set are 
	 * automatically calculated from the @link $mainPath in the doPathDefinitions()
	 * method. 
	 */
	public $mainPath = 'ext.tcpdf';
	public $urlPath = '';
	public $fontsPath = '';
	public $cachePath = '';
	public $urlCachePath = '';
	public $imagesPath = '';
	public $blankImage = '';
	public $cellHeightRatio = 1.25;
	public $titleMagnification = 1.3;
	public $smallRatio = null;
	public $thaiTopChars = false;
	public $allowTcpdfCallsInHtml = false;
	
	/* default parameters for constructing the TCPDF object */
	public $pageFormat = 'A4';
	public $pageOrientation = 'P';
	public $unit = 'mm';
	public $unicode = true;
	public $encoding = 'UTF-8';
	public $diskCache = false;
	
	/**
	 * 
	 * Set this to true if you wish to ignore the above settings and 
	 * load the default config file
	 * @var boolean whether to use TCPDF config file or not
	 */
	public $useConfigFile = false;
	
	/* array mapping the class variables to the definitions needed
	 * for TCPDF
	 */
	protected static $definitions = array(
		'K_PATH_MAIN' => 'mainPath',
		'K_PATH_URL' => 'urlPath',
		'K_PATH_FONTS' => 'fontsPath',
		'K_PATH_CACHE' => 'cachePath',
		'K_PATH_URL_CACHE' => 'urlCachePath',
		'K_PATH_IMAGES' => 'imagesPath',
		'K_BLANK_IMAGE' => 'blankImage',
		'K_CELL_HEIGHT_RATIO' => 'cellHeightRatio',
		'K_TITLE_MAGNIFICATION' => 'titleMagnification',
		'K_SMALL_RATIO' => 'smallRatio',
		'K_THAI_TOPCHARS' => 'thaiTopChars',
		'K_TCPDF_CALLS_IN_HTML' => 'allowTcpdfCallsInHtml',
		'K_TCPDF_EXTERNAL_CONFIG' => 'useConfigFile',
	);
	
	/* the class to be used as the TCPdf wrapper
	 * defaults to RRYiiTCPdf
	 * present to allow people to use their own wrappers with this component if they so wish
	 */
	public $wrapperAlias = 'ext.tcpdf.RRYiiTCPdf';
	
	/* with Yii's auto-loading feature, this file should not actually be included until
	 * this component is called (which will currently only happen when calling the
	 * create method), so we can safely include all our initialisation here. 
	 */
	public function init() {
		parent::init();
		Yii::trace(Yii::t('RRTCPdf', 'Initialising RRTCPdf'), 'ext.RRTCPdf');
		
		/* resolve the main include path */
		$this->mainPath = Yii::getPathOfAlias($this->mainPath).DIRECTORY_SEPARATOR;
		
		/* do any necessary definitions before including the TCPdf class file */
		$this->doDefinitions();
		
		/* include the TCPdf class file (we cannot Yii autoload it because the file
		 * name is not the same as the class name) */
		require_once($this->mainPath.'tcpdf.php');
		
		/* import the wrapper class file */
		Yii::import($this->wrapperAlias);
	}
	
	public function doDefinitions() {
		if (!$this->useConfigFile) {
			$this->doPathDefinitions();
			$this->doOtherDefinitions();
			/* loop through the definitions array, giving each definition
			 * its corresponding value from the class variable
			 */
			foreach(self::$definitions as $name => $value) {
				define($name, $this->$value);
			}
		}
	}
	
	/**
	 * 
	 * For each path variable, get its path from Yii alias if it is defined,
	 * if not calculate a default.
	 * The Yii runtime path is used for the cache directories (to avoid any
	 * write problems).
	 * This can be overridden if the paths should be calculated in a 
	 * different way.
	 */
	public function doPathDefinitions() {
		if ($this->urlPath === '') {
			$this->urlPath = $this->mainPath;
		}
		else {
			$this->urlPath = Yii::getPathOfAlias($this->fontsPath).DIRECTORY_SEPARATOR;
		}
		if ($this->fontsPath === '') {
			$this->fontsPath = $this->mainPath.'fonts'.DIRECTORY_SEPARATOR;
		}
		else {
			$this->fontsPath = Yii::getPathOfAlias($this->fontsPath).DIRECTORY_SEPARATOR;
		}
		if ($this->cachePath === '') {
			$this->cachePath = Yii::app()->getRuntimePath().DIRECTORY_SEPARATOR;
		}
		else {
			$this->cachePath = Yii::getPathOfAlias($this->cachePath).DIRECTORY_SEPARATOR;
		}
		if ($this->urlCachePath === '') {
			$this->urlCachePath = $this->urlPath.'cache'.DIRECTORY_SEPARATOR;
		}
		else {
			$this->urlCachePath = Yii::app()->getRuntimePath().DIRECTORY_SEPARATOR;
		}
		if ($this->imagesPath === '') {
			$this->imagesPath = $this->mainPath.'images'.DIRECTORY_SEPARATOR;
		}
		else {
			$this->imagesPath = Yii::getPathOfAlias($this->imagesPath).DIRECTORY_SEPARATOR;
		}
		if ($this->blankImage === '') {
			$this->blankImage = $this->imagesPath.'_blank.png';
		}
		else {
			$this->blankImage = Yii::getPathOfAlias($this->blankImage).DIRECTORY_SEPARATOR;
		}
	}
	
	/* calculate any other definitions that cannot be calculated in
	 * their declaration
	 */
	public function doOtherDefinitions() {
		if ($this->smallRatio === null) {
			$this->smallRatio = 2/3;
		}	
	}
	
	/* create a new PhpThumb wrapper object */
	public function create($orientation = null, $unit = null, $format = null, $unicode = null, $encoding = null, $diskcache = null) {
		/* use default parameters set in this class rather than have them overridden by
		 * the TCPDF object's default construction parameters
		 */
		if ($orientation === null) {
			$orientation = $this->pageOrientation;
		}
		if ($unit === null) {
			$unit = $this->unit;
		}
		if ($format === null) {
			$format = $this->pageFormat;
		}
		if ($unicode === null) {
			$unicode = $this->unicode;
		}
		if ($encoding === null) {
			$encoding = $this->encoding;
		}
		if ($diskcache === null) {
			$diskcache = $this->diskCache;
		}
		// get the wrapper class from its alias
		$wrapper = '';
		$pos = strrpos($this->wrapperAlias, '.');
		if ($pos === false) {
			$wrapper = $this->wrapperAlias;
		}
		else {
			$wrapper = substr($this->wrapperAlias, $pos + 1);
		}
		return new $wrapper($orientation, $unit, $format, $unicode, $encoding, $diskcache);
	}
}