<style>div.ui-jqdialog-content td.form-view-data {
        white-space: normal !important;
        height: auto;
        vertical-align: middle;
        padding-top: 3px; padding-bottom: 3px
    }
        .newmsg{
        background-color:#ffc !important;
    }
    .unreadmsg{
        background-color:#feefcd !important;
    }
</style>
<?php
$member_id = Membership::model()->getScalar("member_id", "user_id=" . Yii::app()->user->id);
$url = Yii::app()->baseUrl;
?>
<script>
    $.jgrid.defaults.width = 1200;
    $.jgrid.defaults.responsive = true;
    $.jgrid.defaults.styleUI = 'Bootstrap';</script>
<div style="margin-left:20px">
    <table id="guarantorsGrid"></table>
    <div id="guarantorsGridPager"></div>
</div>
<script type="text/javascript">

    $(document).ready(function() {
        $.jgrid.styleUI.Bootstrap.base.rowTable = "table table-bordered table-striped";
        var template = "<div style='margin-left:15px;'>";
        template += "<div> Other Loans: </div><div>{otherloan_id} </div>";
        template += "<hr style='width:100%;'/>";
        template += "<div> {sData} {cData}  </div></div>";
        var rowsToColor = [];
        $("#guarantorsGrid").jqGrid({
            url: 'data/messages',
            datatype: "json",
            rownumbers: true,
            toppager: true,
            colModel: [
                {name: 'message_id', hidden: true, editable: true, search: false},
                {label: '#',
                    name: 'message_id',
                    hidden: true,
                    width: 8,
                    key: true,
                    search: false,
                    editable: false,
                },
                {
                    label: 'Date Recieved',
                    name: 'date',
                    search: false,
                    editable: false,
                },
                {
                    label: 'FROM',
                    name: 'sendername',
                    search: false,
                    editable: false,
                },
                {
                    label: 'Subject',
                    name: 'subject',
                    width: 200,
                    search: false,
                    editable: false,
                },
                {
                    label: 'Message',
                    name: 'body',
                    width: 200,
                    search: false,
                    editable: false,
                },
                {
                    label: 'Status',
                    name: 'status_id',
                    width: 200,
                    search: false,
                    editable: true,
                    edittype: 'select',
                    formatter: 'select',
                    editoptions: {value: {0: "New", 1: "Unread", 2: 'Read'}},
                }

            ],
            sortname: 'guarantor_id',
            sortorder: 'asc',
            loadonce: true,
            toolbar: true,
            viewrecords: true,
            width: 1000,
            height: 400,
            rowNum: 20,
            onSelectRow: function(rowid) {
                $(this).jqGrid('viewGridRow', rowid);
                var rowData = $("#guarantorsGrid").getRowData(rowid);
                var message_id = rowData['message_id'];
                $.ajax({
                    'type': 'POST',
                    'url': '<?php echo $url; ?>/messages/read',
                    'data': {'message_id': message_id},
                    'success': function() {
                        $('#guarantorsGrid').trigger("reloadGrid");
                    }});

            },
            loadComplete: function() {
                var rowIds = $('#guarantorsGrid').jqGrid('getDataIDs');
                for (i = 0; i < rowIds.length; i++) {//iterate over each row
                    rowData = $('#guarantorsGrid').jqGrid('getRowData', rowIds[i]);
                    //set background style if ColValue === true\
                    if (rowData['status_id'] == "0") {
                        $('#guarantorsGrid').jqGrid('setRowData', rowIds[i], false, "newmsg");
                    }
                    if (rowData['status_id'] == "1") {
                        $('#guarantorsGrid').jqGrid('setRowData', rowIds[i], false, "unreadmsg");
                    }
                } //for
            },
            pager: "#guarantorsGridPager"
        }).trigger('reloadGrid');
        $('#guarantorsGrid').navGrid('#guarantorsGridPager',
                {edit: false, add: false, del: false, search: false, refresh: true, view: false, position: "left", cloneToTop: true},
        {url: "loans/loans/confirmguarantors", closeAfterEdit: true},
        {url: "loans/loans/confirmguarantors", closeAfterEdit: true},
        {url: "messages/delete", closeAfterEdit: true},
        {// reload
            reloadAfterSubmit: true
        },
        // options for the Edit Dialog
        {
            editCaption: "The Edit Dialog",
            template: template,
            errorTextFormat: function(data) {
                return 'Error: ' + data.responseText
            }
        },
        // options for the Add Dialog
        {
            template: template,
            errorTextFormat: function(data) {
                return 'Error: ' + data.responseText
            }
        },
        // options for the Delete Dailog
        {
            errorTextFormat: function(data) {
                return 'Error: ' + data.responseText
            }

        });
        $('#guarantorsGrid').jqGrid('filterToolbar', {
            // JSON stringify all data from search, including search toolbar operators
            stringResult: true,
            // instuct the grid toolbar to show the search options
            searchOperators: false,
        });

        setInterval(function() {
            $.ajax({
                'type': 'POST',
                'url': '<?php echo $url; ?>/messages/confirm',
                'data': {'id': 1},
                'success': function(result) {
                    //$("#guarants").html(result);
                }});


        }, 5000);




    });


</script>

