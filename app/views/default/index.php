<!DOCTYPE html>
<html lang="en" data-ng-app="MuskeyApp">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <title data-ng-bind="'Bethany Brains| ' + $state.current.data.pageTitle"></title>

        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta content="" name="description"/>
        <meta content="" name="author"/>

        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="malt/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="malt/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
        <link href="malt/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="malt/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" media="screen"  src="malt/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css"/>
        <link rel='stylesheet' href='malt/assets/global/plugins/fullcalendar/css/jquery-ui.min.css' />
        <link href='malt/assets/global/plugins/fullcalendar/css/fullcalendar.css' rel='stylesheet' />
        <link href='malt/assets/global/plugins/fullcalendar/css/fullcalendar.print.css' rel='stylesheet' media='print' />
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <!-- END GLOBAL MANDATORY STYLES -->

        <!-- BEGIN DYMANICLY LOADED CSS FILES(all plugin and page related styles must be loaded between GLOBAL and THEME css files ) -->
        <link id="ng_load_plugins_before"/>
        <!-- END DYMANICLY LOADED CSS FILES -->

        <!-- BEGIN THEME STYLES -->
        <!-- DOC: To use 'rounded corners' style just load 'components-rounded.css' stylesheet instead of 'components.css' in the below style tag -->
        <link href="malt/assets/global/css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css"/>
        <link href="malt/assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
        <link href="malt/assets/admin/layout4/css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="malt/assets/admin/layout4/css/themes/light.css" rel="stylesheet" type="text/css" id="style_color"/>
        <link href="malt/assets/admin/layout4/css/custom.css" rel="stylesheet" type="text/css"/>
        <script src="malt/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script type="text/ecmascript" src="malt/js/jquery-2.0.2.min.js"></script> 
        <script type="text/ecmascript" src="malt/jqgrid/js/grid.locale-en.js"></script> 
        <script type="text/ecmascript" src="malt/jqgrid/js/jquery.jqGrid.js"></script>
        <link rel="stylesheet" type="text/css" media="screen" href="malt/jqgrid/css/ui.jqgrid-bootstrap.css" />



        <!-- END THEME STYLES -->

        <link rel="shortcut icon" href="favicon.ico"/>
    </head>
    <body ng-controller="AppController" class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-sidebar-closed-hide-logo page-on-load" ng-class="{'page-sidebar-closed': settings.layout.pageSidebarClosed}">

        <!-- BEGIN PAGE SPINNER -->
        <div ng-spinner-bar class="page-spinner-bar">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
        <!-- END PAGE SPINNER -->

        <!-- BEGIN HEADER -->
        <div data-ng-include="'default/header'" data-ng-controller="HeaderController" class="page-header navbar navbar-fixed-top">
        </div>
        <!-- END HEADER -->

        <div class="clearfix">
        </div>

        <!-- BEGIN CONTAINER -->  <div class="page-container">
            <?php if (isset(Yii::app()->session['success'])) { ?>

                <div class="datamessages note note-success note-bordered">
                    <i class="fa fa-check-circle-o"></i>  <?php echo Yii::app()->session['success']; ?><span class="close" data-close="note"></span>
                </div>
                <?php
                unset(Yii::app()->session['success']);
            } elseif (isset(Yii::app()->session['error'])) {
                ?> <div class="datamessages note note-danger note-bordered">
                    <i class="fa fa-close"></i> Error:<?php echo Yii::app()->session['error']; ?><span class="close" data-close="note"></span>
                </div>
                <?php
                unset(Yii::app()->session['error']);
            }
            ?>

            <!-- BEGIN SIDEBAR -->
            <div data-ng-include="'default/sidebar'" data-ng-controller="SidebarController" class="page-sidebar-wrapper">			
            </div>
            <!-- END SIDEBAR -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD -->
                    <div data-ng-include="'default/pagehead'" data-ng-controller="PageHeadController" class="page-head">			
                    </div>
                    <!-- END PAGE HEAD -->
                    <!-- BEGIN ACTUAL CONTENT -->
                    <div ui-view class="fade-in-up">
                    </div> 
                    <!-- END ACTUAL CONTENT -->
                </div>
            </div>
        </div>
        <!-- END CONTAINER -->

        <!-- BEGIN FOOTER -->
        <div data-ng-include="'default/footer'" data-ng-controller="FooterController" class="page-footer">
        </div>


        <script src="malt/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
        <script src="malt/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="malt/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="malt/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="malt/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="malt/assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
        <script src="malt/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="malt/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <!-- END CORE JQUERY PLUGINS -->

        <!-- BEGIN CORE ANGULARJS PLUGINS -->
        <script src="malt/assets/global/plugins/angularjs/angular.min.js" type="text/javascript"></script>	
        <script src="malt/assets/global/plugins/angularjs/angular-sanitize.min.js" type="text/javascript"></script>
        <script src="malt/assets/global/plugins/angularjs/angular-touch.min.js" type="text/javascript"></script>	
        <script src="malt/assets/global/plugins/angularjs/plugins/angular-ui-router.min.js" type="text/javascript"></script>
        <script src="malt/assets/global/plugins/angularjs/plugins/ocLazyLoad.min.js" type="text/javascript"></script>
        <script src="malt/assets/global/plugins/angularjs/plugins/ui-bootstrap-tpls.min.js" type="text/javascript"></script>

        <script src='malt/assets/global/plugins/fullcalendar/js/moment.min.js'></script>
        <script src='malt/assets/global/plugins/fullcalendar/js/fullcalendar.js'></script>
        <script src='malt/assets/global/plugins/barcode/jquery-barcode.min.js'></script>
        <!-- END CORE ANGULARJS PLUGINS -->

        <!-- BEGIN APP LEVEL ANGULARJS SCRIPTS --> 

        <script src="malt/js/app.js" type="text/javascript"></script>
        <script src="malt/js/controllers.js"></script> 
        <script src="malt/js/directives.js" type="text/javascript"></script>

        <!-- END APP LEVEL ANGULARJS SCRIPTS -->

        <!-- BEGIN APP LEVEL JQUERY SCRIPTS -->
        <script src="malt/assets/global/scripts/metronic.js" type="text/javascript"></script>
        <script src="malt/assets/admin/layout4/scripts/layout.js" type="text/javascript"></script>
        <script src="malt/assets/admin/layout4/scripts/demo.js" type="text/javascript"></script>  
        <!-- END APP LEVEL JQUERY SCRIPTS -->

        <script type="text/javascript">
                    /* Init Muskey's core jquery plugins and layout scripts */
                    $(document).ready(function() {
                        Muskey.init(); // Run metronic theme
                        Muskey.setAssetsPath('malt/assets/'); // Set the assets folder path			
                    });
                    function processAddEdit(response) {
                        var resp = response.responseText;
                        var sub = resp.substring(1, 3);
                        if (sub == "qq") {
                            var receipt = parseInt(resp.substring(3));
                            window.open('fees/receipts/printreceipt/?receipt_id=' + receipt, '_blank');
                            $("#jqGrid").setGridParam({datatype: 'json'}).trigger('reloadGrid');
                            $('.ui-jqdialog-titlebar-close').trigger("click");
                            //return [true, "Data Successfully Saved", "1"];

                        }
                        else if (response.responseText != "true") {
                            var json = eval('(' + response.responseText + ')');
                            //each error
                            var errorstring = "Errors:";
                            for (var key in json) {
                                if (json.hasOwnProperty(key)) {
                                    errorstring = errorstring + "<br/>" + json[key];
                                }
                            }
                            var new_id = "1";

                            return [false, errorstring, new_id];
                        } else
                        {
                            $("#jqGrid").setGridParam({datatype: 'json'}).trigger('reloadGrid');
                            // jQuery("#jqGrid").setGridParam({datatype:'local'}).trigger('reloadGrid');
                            //$('#jqGrid').trigger("reloadGrid", { fromServer: true, page: 1 });
                            $('.ui-jqdialog-titlebar-close').trigger("click");
                            return [true, "Data Successfully Saved", "1"];


                        }
                    }
                    function exportData(gridID, header) {
                        var grid = $('#' + gridID);
                        var rowIDList = grid.getDataIDs();
                        var row = grid.getRowData(rowIDList[0]);
                        var colNames = [];
                        var i = 0;
                        var html = header + "___";
                        for (var cName in row) {
                            colNames[i++] = cName; // Capture Column Names
                            html += $("#jqgh_" + gridID + "_" + cName).text() + '__';//for comma resp

                        }

                        html = html + "___";


                        for (var j = 0; j < rowIDList.length; j++) {
                            for (var i = 0; i < (colNames.length); i++) {
                                var value = $("tr[id='" + rowIDList[j] + "'] td[aria-describedby='" + gridID + "_" + colNames[i] + "']").text();
                                if (value) {
                                    html += value + '__';//for comma
                                }
                                else {
                                    html += "-" + '__';//for comma
                                }
                            }
                            html = html + "___";
                        }
                        window.open('data/exportexcel/?data=' + html);
                    }
                    //
                    function exportdataaggregate(gridID, header) {
                        var grid = $('#' + gridID);
                        var rowIDList = grid.getDataIDs();
                        var row = grid.getRowData(rowIDList[0]);
                        var colNames = [];
                        var i = 0;
                        var html = header + "___";
                        for (var cName in row) {
                            colNames[i++] = cName; // Capture Column Names
                            html += $("#jqgh_" + gridID + "_" + cName).text() + '__';//for comma resp

                        }

                        html = html + "___";

                        //----------------------------
                        $(".jqfoot").each(function(index) {
                            var val = $('#jqGridghead_0_' + index).text();
                            html += val + '___';//for new line
                            var counter = 0;
                            for (var j = counter; j < rowIDList.length; j++) {
                                for (var i = 0; i < (colNames.length); i++) {
                                    var value = $("tr[id='" + rowIDList[j] + "'] td[aria-describedby='" + gridID + "_" + colNames[i] + "']").text();
                                    if (value) {
                                        html += value + '__';//for comma
                                    }
                                    else {
                                        html += "-" + '__';//for comma
                                    }
                                }
                                html = html + "___";
                                counter = j;
                            }
                            //start the total footers

//                            for (var i = 0; i < (colNames.length); i++) {
//                                var tr=$(this);
//                                var value = $("td[aria-describedby='" + gridID + "_" + colNames[i] + "']").text();
//                                if (value) {
//                                    html += value + '__';//for comma
//                                }
//                                else {
//                                    html += "-" + '__';//for comma
//                                }
//                            }
//                            html = html + "___";


                        });

                        //----------------------------


                        window.open('data/exportexcel/?data=' + html);
                    }
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>
<script>
    setTimeout(function() {
        $('.datamessages').fadeOut(1000);
    }, 12000);
</script>
