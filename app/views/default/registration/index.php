
<!DOCTYPE html> 
<html>
    <head>
        <title>Bethany Brains|Registration</title>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
        <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl . '/malt/assets/registration/css/main.css'; ?>">
        <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl . '/malt/assets/registration/css/sky-forms.css'; ?>">
        <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl . '/malt/assets/registration/css/jquery-ui.css'; ?>">
        <script src="<?php echo Yii::app()->baseUrl . '/malt/assets/registration/js/jquery-1.10.2.js'; ?>"></script>
        <script src="<?php echo Yii::app()->baseUrl . '/malt/assets/registration/js/jquery-ui.js'; ?>"></script>

    </head>
    <style>
        .errordisplay{
            background-color:#fef4ef;
            border:1px solid #f56709;
            position:absolute;
            width:28.2%;
            padding: 10px;
            top: 250px;
            color:red;
            z-index:100;

        }.error{
            background-color:#fef4ef;
            border:1px solid #f56709;
            position:absolute;
            width:28.2%;
            padding: 10px;
            top: 90px;
            color:red;
            font-size: 13px;
            z-index:100;
            box-shadow: 1px #fef4ef;
            border-radius: 3px;

        }
        .success{
            background-color:#ccffcc;
            border:1px solid green;
            position:absolute;
            width:28.2%;
            padding: 10px;
            top: 90px;
            font-size: 13px;
            z-index:100;
            box-shadow: 1px #ccffcc;
            border-radius: 3px;
        }
        .loading{
            opacity:0.4;
        }

    </style>
    <body class="bg-cyan">        
    <center><span style="font-size: 28px;  color: #fff;">Bethany Brains Academy</span></center> 
             <hr />
        <div class="body body-s">

            <form action="<?php echo Yii::app()->baseUrl . '/default/createmember'; ?>" id="sky-form" class="sky-form" method="post">
                <div class="errordisplay" style="display:none;"></div>
                <header>
                    <div id="forminfo"></div>

                    <img style=" float: left ; margin-right:50px;"  src="<?php echo Yii::app()->baseUrl ?>/malt/img/newlogo.jpeg" alt="logo" height="50" width="50" />

                    Registration form  </header>

                <fieldset>
                    <section>
                        <label class="input">
                            <i class="icon-append icon-user"></i>
                            <input type="text" id="fname" name="fname" placeholder="First Name">
                            <b class="tooltip tooltip-bottom-right">Enter First Name</b>
                        </label>
                    </section>
                    <section>
                        <label class="input">
                            <i class="icon-append icon-user"></i>
                            <input type="text" id="lname" name="lname" placeholder="Last Name">
                            <b class="tooltip tooltip-bottom-right">Enter Last Name</b>
                        </label>
                    </section>

                    <section>
                        <label class="input">
                            <i class="icon-append icon-envelope-alt"></i>
                            <input type="email" name="email" id="email" placeholder="Email address">
                            <b class="tooltip tooltip-bottom-right">Enter Email to verify your account</b>
                        </label>
                    </section>
                    <section>
                        <label class="input">
                            <i class="icon-append icon-calendar"></i>
                            <input type="text" name="dob" placeholder="Date of Birth" id="datepicker">
                            <b class="tooltip tooltip-bottom-right">Select the Date of Birth</b>
                        </label>
                    </section>
                    <section>
                        <label class="input">
                            <i class="icon-append icon-phone"></i>
                            <input type="text" name="mobile" placeholder="Mobile" id="mobile">
                            <b class="tooltip tooltip-bottom-right">Enter Mobile Number</b>
                        </label>
                    </section>
                </fieldset>

                <fieldset>
                    <section>
                        <label class="input">
                            <i class="icon-append icon-lock"></i>
                            <input type="password" id="password" name="password" placeholder="Password">
                            <b class="tooltip tooltip-bottom-right">Enter Password</b>
                        </label>
                    </section>

                    <section>
                        <label class="input">
                            <i class="icon-append icon-lock"></i>
                            <input type="password" id="password2" name="password2" placeholder="Confirm password">
                            <b class="tooltip tooltip-bottom-right">Confirm Password</b>
                        </label>
                    </section>


                </fieldset>
                <footer>
                    <a href="<?php echo Yii::app()->baseUrl . '/auth/default/login'; ?>">Login</a>
                    <button type="submit" class="button" id="submit">Submit</button>
                    <img id="loader" style="display: none;" src="<?php echo Yii::app()->baseUrl ?>/malt/img/loader.gif">
                </footer>
            </form>

        </div>
    </body>
    <script>
        $(function() {
            $("#datepicker").datepicker();
            $("#datepicker").datepicker("option", "dateFormat", "yy-mm-dd");
        });
        $('#submit').click(function(event) {
            event.preventDefault();

            $('#loader').show();
            $('.body').addClass("loading");
            var errorp = false;
            $(":text,#email,:password, select").each(function() {
                if ($(this).val() === "") {
                    $(this).css("background-color", "#fef4ef");
                    $(this).css("border", "1px solid #f56709");
                    $(".errordisplay").show();
                    $(".errordisplay").html('<span class="icon-warning-sign"></span> Kindly Fill In the Highlighted Field(s)');
                    errorp = true;
                    setTimeout(function() {
                        $(".errordisplay").fadeOut().empty();

                    }, 3000);
                }

                else {
                    $(this).css("background-color", "#fff");
                    $(this).css("border", "2px solid #e5e5e5");
                }

            });
            if ($('#password').val() != $('#password2').val()) {
                $('#password2').css("border", "1px solid #f56709");
                $('#forminfo').show();
                $('#forminfo').removeClass("success");
                $('#forminfo').addClass("error");
                $('#forminfo').html("The Passwords do not match");
                errorp = true;

            }
            setTimeout(function() {
                $("#forminfo").fadeOut().empty();
            }, 5000);
            //
            if (errorp == false) {//send via ajax
                $.ajax({
                    'type': 'POST',
                    'url': '<?php echo Yii::app()->baseUrl . '/default/createmember'; ?>',
                    'cache': false,
                    'data': {'fname': $('#fname').val(), 'lname': $('#lname').val(),
                        'dob': $('#datepicker').val(), 'email': $('#email').val(),
                        'mobile': $('#mobile').val(), 'password': $(':password').val()},
                    'success': function(html) {

                        obj = JSON.parse(html);
                        if (obj.operation == 'error') {
                            $('#forminfo').removeClass("success");
                            $('#forminfo').addClass("error");
                            $('#forminfo').show();
                            $('#forminfo').html("Your Details have Errors:" + obj.the_errors);

                        } else {
                            $('#forminfo').removeClass("error");
                            $('#forminfo').addClass("success");
                            $('#forminfo').show();
                            $('#forminfo').html("Your Data has been Successfully Saved, \n\
            Your login Details have been Sent to your Email, Kindly check your Email. Thank you.\n\
You shall be Redirected in <span id='redirect'>7</span> seconds");


                        }
                        setTimeout(function() {
                            $("#forminfo").fadeOut().empty();
                            if (obj.operation != 'error') {
                                window.location = "<?php echo Yii::app()->baseUrl . "/auth/default/login"; ?>";
                            }

                        }, 7000);
                    }
                });
            }
            $('.body').removeClass("loading");
            $('#loader').hide();

        });

    </script>

</html>
