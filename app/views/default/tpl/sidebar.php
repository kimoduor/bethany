<div class="page-sidebar navbar-collapse collapse">
    <ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" ng-class="{'page-sidebar-menu-closed': settings.layout.pageSidebarClosed}">
        <li class="heading">
            <h3>The Bar</h3> 
        </li>
        <li class="start">
            <a href="#/dashboard">
                <i class="icon-home"></i>
                <span class="title">Home</span>
            </a>
        </li>
        <?php
        $role_id = Users::model()->get(Yii::app()->user->id, "role_id");
        if ($role_id == 1 || $role_id == 2) {
            ?>
            <li>
                <a href="javascript:;">
                    <i class="icon-wrench"></i>
                    <span class="title">Setup</span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="#/buses">
                            <i class="fa fa-angle-right"></i><span> School Bus</span>
                        </a>
                    </li>
                    <li>
                        <a href="#/classes">
                            <i class="fa fa-angle-right"></i><span>Classes</span>
                        </a>
                    </li>
                    <li>
                        <a href="#/streams">
                            <i class="fa fa-angle-right"></i><span>Streams</span>
                        </a>
                    </li>
                    <li>
                        <a href="#/route">
                            <i class="fa fa-angle-right"></i><span>Routes</span>
                        </a>
                    </li>
                    <li>
                        <a href="#/emails">
                            <i class="fa fa-angle-right"></i><span class="title">Email Addresses</span>
                        </a>
                    </li>    
                    <li>
                        <a href="#/banks">
                            <i class="fa fa-angle-right"></i><span>Banks</span>
                        </a>
                    </li>
                    <li>
                        <a href="#/paymenttypes">
                            <i class="fa fa-angle-right"></i><span>Payment Types</span>
                        </a>
                    </li>
                    <li>
                        <a href="#/years">
                            <i class="fa fa-angle-right"></i><span>Years</span>
                        </a>
                    </li>
                    <li>
                        <a href="#/terms">
                            <i class="fa fa-angle-right"></i><span>Terms</span>
                        </a>
                    </li>
                                        <li>
                        <a href="#/closingopeningdate">
                            <i class="fa fa-angle-right"></i><span>Set Close and Open Date</span>
                        </a>
                    </li>
                    <li>
                        <a href="#/major">
                            <i class="fa fa-angle-right"></i><span>Termly Update</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="javascript:;">
                    <i class="icon-user-following"></i>
                    <span class="title">Users</span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="#/newroles">
                            <i class="icon-user"></i> Manage User Roles
                        </a>
                    </li>

                </ul>
            </li>
            <li>
                <a href="javascript:;">
                    <i class="fa fa-cog"></i>
                    <span class="title">Fees Setup</span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="#/voteheads">
                            <i class="fa fa-angle-right"></i> Voteheads Setup
                        </a>
                    </li>
                    <li>
                        <a href="#/voteheadyear">
                            <i class="fa fa-angle-right"></i> Yearly Voteheads
                        </a>
                    </li>
                    <li>
                        <a href="#/charges">
                            <i class="fa fa-angle-right"></i> Other Charges
                        </a>
                    </li>

                </ul>
            </li>
        <?php }?>
            <li>
                <a href="#/pupils">
                    <i class="icon-plus"></i>
                    <span class="title">Pupils/Students</span>
                </a>
            </li>
            <li>
                <a href="javascript:;">
                    <i class="fa fa-xing"></i>
                    <span class="title">Examination</span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="#/subjects">
                            <i class="fa fa-angle-right"></i> Manage Subjects
                        </a>
                    </li>
                    <li>
                        <a href="#/examinationtypes">
                            <i class="fa fa-angle-right"></i> Manage Examinations
                        </a>
                    </li>
                     <li>
                        <a href="#/subjectteachers">
                            <i class="fa fa-angle-right"></i> Manage Subject Teachers
                        </a>
                    </li>

                </ul>
            </li>


    </ul>
    <!-- END SIDEBAR MENU -->
</div>	
