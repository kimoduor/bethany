<!-- BEGIN HEADER INNER -->

<div class="page-header-inner">
    <!-- BEGIN LOGO -->
    <div class="page-logo">
        <a href="#/dashboard">
            <img src="malt/img/newlogo.jpeg" alt="logo" class="logo-default" height="60" width="80"/>
        </a>
        <div class="menu-toggler sidebar-toggler">
            <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
        </div>
    </div>
    <!-- END LOGO -->
    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
    </a>
    <!-- END RESPONSIVE MENU TOGGLER -->
    <!-- BEGIN PAGE ACTIONS -->
    <!-- DOC: Remove "hide" class to enable the page header actions -->
    <?php
        $role_id = Users::model()->get(Yii::app()->user->id, "role_id");
        if ($role_id == 1 || $role_id == 2) {
            ?>
    <div class="page-actions">
        <div class="btn-group">
            <button type="button" class="btn  ui-button ui-state-default  ui-state-active btn-sm dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                <span class="hidden-sm hidden-xs"><i class="fa fa-university"></i> Fees Operations&nbsp;</span><i class="fa fa-angle-down"></i>
            </button>
            <ul class="dropdown-menu" role="menu">
                <li>
                    <a href="#pay">
                        <i class="fa fa-money"></i>Pay Fees </a>
                </li>
                <li>
                    <a href="#feesstructure">
                        <i class="fa fa-cubes"></i> Fees Structure </a>
                </li>
                

                <li>
                    <a href="#chargestudents">
                        <i class="fa fa-money"></i> Charge Students</a>
                </li>
                <li>
                    <a href="#receipts">
                        <i class="fa fa-bookmark-o"></i> Fees transactions & Receipts</a>
                </li>
                <li>
                    <a href="#feesbalances">
                        <i class="fa fa-tags"></i> Fees Balances </a>
                </li>

            </ul>
        </div>
    </div>
    
    <div class="page-actions">
        <div class="btn-group">
            <button type="button" class="btn  ui-button ui-state-default  ui-state-active btn-sm dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                <span class="hidden-sm hidden-xs"><i class="fa fa-cc-mastercard"></i> Accounting Operations&nbsp;</span><i class="fa fa-angle-down"></i>
            </button>
            <ul class="dropdown-menu" role="menu">
                <li>
                    <a href="#incometypes"><i class="fa fa-cog"></i>Income Types </a>
                </li>
                <li>
                    <a href="#income"><i class="fa fa-cny"></i> Enter Income </a>
                </li>
                <li>
                    <a href="#expensetypes"><i class="fa fa-cog"></i>Expense Types </a>
                </li>
                <li>
                    <a href="#expenses"><i class="fa fa-cny"></i> Enter Expenses </a>
                </li>
                <li>
                    <a href="#expensesincome"><i class="fa fa-asterisk"></i> Daily Expenses & Income </a>
                </li>
            </ul>
        </div>
    </div>
        <?php }?>
    <div class="page-actions">
        <div class="btn-group">
            <button type="button" class="btn  ui-button ui-state-default  ui-state-active btn-sm dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                <span class="hidden-sm hidden-xs"><i class="fa fa-graduation-cap"></i> Examination Operations&nbsp;</span><i class="fa fa-angle-down"></i>
            </button>
            <ul class="dropdown-menu" role="menu">
                <li>
                    <a href="#entermarks"><i class="fa fa-pencil-square"></i> Enter Marks </a>
                </li>
                <li>
                    <a href="#meritlists"><i class="fa fa-list"></i> Tests Merit Lists </a>
                </li>
                <li>
                    <a href="#reportcards"><i class="fa fa-tags"></i>Termly Merit List & Report Cards </a>
                </li>

            </ul>
        </div>
    </div>


    <!-- END PAGE ACTIONS -->
    <!-- BEGIN PAGE TOP -->
    <div class="page-top">
        <!-- END HEADER SEARCH BOX -->
        <!-- BEGIN TOP NAVIGATION MENU -->
        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">
                <li class="separator hide"></li>
                <!-- BEGIN NOTIFICATION DROPDOWN -->
                <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                
                <!-- END NOTIFICATION DROPDOWN -->
                <li class="separator hide"></li>
                <!-- BEGIN INBOX DROPDOWN -->
                <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
              
                <!-- END INBOX DROPDOWN -->
                <li class="separator hide"></li>
                <!-- BEGIN TODO DROPDOWN -->
                <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                <li class="dropdown dropdown-user dropdown-dark">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <span class="username username-hide-on-mobile">

                            <?php
                            echo Yii::app()->user->name;
                            ?>
                        </span>
                        <!-- DOC: Do not remove below empty space(&nbsp;) as its purposely used -->
                        <?php
                        $user_id = Yii::app()->user->id;
                        $imageurl = Users::model()->getScalar('photo_url', "id=$user_id");
                        ?>
                        <img id="uploadPreview3"  alt="" class="img-circle"  src="<?php echo $imageurl ? "public/idfore/$imageurl" : "malt/img/default.jpg"; ?>" class="myimage" />

                    </a>
                    <ul class="dropdown-menu dropdown-menu-default">
                        <li>
                            <a href="#/profile">
                                <i class="icon-user"></i> My Profile </a>
                        </li>

                        <li>
                            <a href="#/messages">
                                <i class="icon-envelope-open" ></i> My Inbox <span class="badge badge-danger" id="myinbox">
                                    0 </span>
                            </a>
                        </li>

                        <li class="divider">
                        </li>
                        <li>
                            <a href="auth/default/logout">
                                <i class="icon-key"></i> Log Out </a>
                        </li>
                    </ul>
                </li>
                <!-- END USER LOGIN DROPDOWN -->
            </ul>
        </div>
        <!-- END TOP NAVIGATION MENU -->
    </div>
    <!-- END PAGE TOP -->
</div>
<!-- END HEADER INNER -->
<?php $url = Yii::app()->baseUrl; ?>

<!-- to be changed for school environment -->
<script>
    $(document).ready(function() {
        setInterval(function() {
            $.ajax({
                'type': 'POST',
                'url': '<?php echo $url; ?>/loans/loans/guarantstoconfirm',
                'data': {'id': 1},
                'success': function(result) {
                    $("#guarants").html(result);
                }});
            $.ajax({
                'type': 'POST',
                'url': '<?php echo $url; ?>/loans/loans/guarantstoconfirm?m=1',
                'data': {'id': 1},
                'success': function(result) {
                    $("#mymessages").html(result);
                    $("#myinbox").html(result);
                }});
            $.ajax({
                'type': 'POST',
                'url': '<?php echo $url; ?>/loans/loans/guarantstoconfirm?n=1',
                'data': {'id': 1},
                'success': function(result) {
                    $("#msg").html(result);

                }});

        }, 5000000000000000000);
    });
    $("#reqloans").click(function(event) {
        event.preventDefault();
        window.location.href = "<?php echo Yii::app()->baseUrl . ''; ?>/?chin#requestloan";
    });


</script>