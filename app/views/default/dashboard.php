<script>

    $(document).ready(function() {

        $('#calendar').fullCalendar({
            theme: true,
            header: {
                left: ' prevYear,nextYear,today,prev,next',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            defaultDate: new Date(),
            editable: true,
            eventLimit: true, // allow "more" link when too many events
            events: [
           
            ]
        });

    });

</script>
<div class="row">
    <div class="col-md-12">
        <div class="col-md-6">
			<div class="dashboard-stat blue-madison">
				<div class="visual">
					<i class="fa fa-comments"></i>
				</div>
				<div class="details">
					<div class="number">
                                           School Motto
					
					</div>
					<div class="desc">
						Knowledge Empowers
					</div>
				</div>
				<a class="more" href="#">
				Motto <i class="m-icon-swapright m-icon-white"></i>
				</a>
			</div>
            <div class="dashboard-stat red-intense">
				<div class="visual">
					<i class="fa fa-bar-chart-o"></i>
				</div>
				<div class="details">
					<div class="number">
						Mission
					</div>
					<div class="desc">
					To encourage Self-Confidence and nurture individual Potential
					</div>
				</div>
				<a class="more" href="#">
				Mission <i class="m-icon-swapright m-icon-white"></i>
				</a>
			</div>
            <div class="dashboard-stat blue-sharp">
				<div class="visual">
					<i class="fa fa-cubes"></i>
				</div>
				<div class="details">
					<div class="number">
						Vision
					</div>
					<div class="desc">
					To be the best Center for a holistic Childhood Growth and Devpt
					</div>
				</div>
				<a class="more" href="#">
				Vision <i class="m-icon-swapright m-icon-white"></i>
				</a>
			</div>
		</div>
        <div class="col-md-6" id="calendar"></div>

    </div>
</div>