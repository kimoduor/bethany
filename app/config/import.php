<?php

/**
 * stores the import configurations
 * @author Joakim <kimoduor@gmail.com>
 */
return array(
    'application.extensions.*',
    'application.models.*',
    'application.models.forms.*',
    'application.components.*',
    'application.components.widgets.*',
    'ext.easyimage.EasyImage',
    //user module
    'application.modules.users.models.*',
    'application.modules.users.components.*',
    'application.modules.users.components.behaviors.*',
    //settings module
    'application.modules.settings.models.*',
    'application.modules.settings.components.*',
    //auth module
    'application.modules.auth.models.*',
    'application.modules.auth.components.*',
    //members module
    'application.modules.pupils.models.*',
    'application.modules.pupils.components.*',
        //fees module
    'application.modules.fees.models.*',
    'application.modules.fees.components.*',
            //exam module
    'application.modules.exam.models.*',
    'application.modules.exam.components.*',

    //tcpf module
    'application.modules.tcpf.*',
//
);
