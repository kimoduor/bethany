/***
 Muskey AngularJS App Main Script
 ***/

/* Muskey App */
var MuskeyApp = angular.module("MuskeyApp", [
    "ui.router",
    "ui.bootstrap",
    "oc.lazyLoad",
    "ngSanitize"
]);

/* Configure ocLazyLoader(refer: https://github.com/ocombe/ocLazyLoad) */
MuskeyApp.config(['$ocLazyLoadProvider', function($ocLazyLoadProvider) {
        $ocLazyLoadProvider.config({
            cssFilesInsertBefore: 'ng_load_plugins_before' // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
        });
    }]);

/********************************************
 BEGIN: BREAKING CHANGE in AngularJS v1.3.x:
 *********************************************/
//AngularJS v1.3.x workaround for old style controller declarition in HTML
MuskeyApp.config(['$controllerProvider', function($controllerProvider) {
        // this option might be handy for migrating old apps, but please don't use it
        // in new ones!
        $controllerProvider.allowGlobals();
    }]);

/* Setup global settings */
MuskeyApp.factory('settings', ['$rootScope', function($rootScope) {
        // supported languages
        var settings = {
            layout: {
                pageSidebarClosed: false, // sidebar state
                pageAutoScrollOnLoad: 1000 // auto scroll to top on page load
            },
            layoutImgPath: Muskey.getAssetsPath() + 'admin/layout/img/',
            layoutCssPath: Muskey.getAssetsPath() + 'admin/layout/css/'
        };

        $rootScope.settings = settings;

        return settings;
    }]);

/* Setup App Main Controller */
MuskeyApp.controller('AppController', ['$scope', '$rootScope', function($scope, $rootScope) {
        $scope.$on('$viewContentLoaded', function() {
            Muskey.initComponents(); // init core components
            //Layout.init(); //  Init entire layout(header, footer, sidebar, etc) on page load if the partials included in server side instead of loading with ng-include directive 
        });
    }]);

/***
 Layout Partials.
 By default the partials are loaded through AngularJS ng-include directive. In case they loaded in server side(e.g: PHP include function) then below partial 
 initialization can be disabled and Layout.init() should be called on page load complete as explained above.
 ***/

/* Setup Layout Part - Header */
MuskeyApp.controller('HeaderController', ['$scope', function($scope) {
        $scope.$on('$includeContentLoaded', function() {
            Layout.initHeader(); // init header
        });
    }]);

/* Setup Layout Part - Sidebar */
MuskeyApp.controller('SidebarController', ['$scope', function($scope) {
        $scope.$on('$includeContentLoaded', function() {
            Layout.initSidebar(); // init sidebar
        });
    }]);

/* Setup Layout Part - Sidebar */
MuskeyApp.controller('PageHeadController', ['$scope', function($scope) {
        $scope.$on('$includeContentLoaded', function() {
            Demo.init(); // init theme panel
        });
    }]);

/* Setup Layout Part - Footer */
MuskeyApp.controller('FooterController', ['$scope', function($scope) {
        $scope.$on('$includeContentLoaded', function() {
            Layout.initFooter(); // init footer
        });
    }]);

/* Setup Rounting For All Pages */
MuskeyApp.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

        // Redirect any unmatched url
        $urlRouterProvider.otherwise("dashboard");

        $stateProvider

                // Dashboard

                .state("dashboard", {
                    url: "/dashboard",
                    templateUrl: "default/dashboard",
                    data: {pageTitle: 'Welcome', pageSubTitle: ''}
                })
                .state("classes", {
                    url: "/classes",
                    templateUrl: "settings/classes",
                    data: {pageTitle: 'Manage Classes', pageSubTitle: ''}
                })
                .state("route", {
                    url: "/route",
                    templateUrl: "settings/route",
                    data: {pageTitle: 'Manage Routes', pageSubTitle: ''}
                })

                .state("buses", {
                    url: "/buses",
                    templateUrl: "settings/buses",
                    data: {pageTitle: 'School Buses', pageSubTitle: ''}
                })
                .state('emails', {
                    url: '/emails',
                    templateUrl: "settings/emails",
                    data: {pageTitle: 'Email Addresses', pageSubTitle: ''},
                })

                .state("profile", {
                    url: "/profile",
                    templateUrl: "auth/profile",
                    data: {pageTitle: 'My Profile', pageSubTitle: ''}
                })
                .state("county", {
                    url: "/county",
                    templateUrl: "settings/county",
                    data: {pageTitle: 'Counties', pageSubTitle: 'Manage Counties'}
                })
                .state("pupils", {
                    url: "/pupils",
                    templateUrl: "pupils/pupils",
                    data: {pageTitle: 'Pupils/Students', pageSubTitle: ''}
                })

                .state("myloans", {
                    url: "/myloans",
                    templateUrl: "loans/loans/myloans",
                    data: {pageTitle: 'Requested Loans', pageSubTitle: ''}
                })
                .state("messages", {
                    url: "/messages",
                    templateUrl: "messages",
                    data: {pageTitle: 'Messages', pageSubTitle: ''}
                })

                .state("barcode-qs", {
                    url: "/barcode?pupil_id",
                    templateUrl: "pupils/pupils/barcode",
                    controller: function($scope, $stateParams) {
                        $scope.pupil_id = $stateParams.loanpupil_id_id;
                    },
                    data: {pageTitle: 'Google Map,Barcode,Thumb Print and e-cards', pageSubTitle: ''}
                })


                .state('newroles', {
                    url: '/newroles',
                    templateUrl: "users/newroles",
                    data: {pageTitle: 'Assign User Roles', pageSubTitle: ''},
                })
                .state('users', {
                    url: '/users',
                    templateUrl: "users",
                    data: {pageTitle: 'users', pageSubTitle: 'Manage Users'},
                })
                .state('banks', {
                    url: '/banks',
                    templateUrl: "settings/banks",
                    data: {pageTitle: ' Manage Banks', pageSubTitle: ''},
                })
                .state('paymenttypes', {
                    url: '/paymenttypes',
                    templateUrl: "settings/paymenttypes",
                    data: {pageTitle: ' Manage Payment Types', pageSubTitle: ''},
                })
                .state('years', {
                    url: '/years',
                    templateUrl: "settings/years",
                    data: {pageTitle: ' Manage Years', pageSubTitle: ''},
                })
                .state('terms', {
                    url: '/terms',
                    templateUrl: "settings/terms",
                    data: {pageTitle: ' Manage Terms', pageSubTitle: ''},
                })
                .state('feesstructure', {
                    url: '/feesstructure',
                    templateUrl: "fees/feesstructure",
                    data: {pageTitle: ' Manage Fees Structure', pageSubTitle: ''},
                })
                .state('voteheads', {
                    url: '/voteheads',
                    templateUrl: "settings/voteheads",
                    data: {pageTitle: ' Manage Voteheads', pageSubTitle: ''},
                })
                .state('voteheadyear', {
                    url: '/voteheadyear',
                    templateUrl: "settings/voteheadyear",
                    data: {pageTitle: ' Manage Yearly Voteheads', pageSubTitle: ''},
                })
                .state('charges', {
                    url: '/charges',
                    templateUrl: "settings/charges",
                    data: {pageTitle: 'Other Charges', pageSubTitle: ''},
                })
                .state('chargestudents', {
                    url: '/chargestudents',
                    templateUrl: "fees/chargestudents",
                    data: {pageTitle: 'Manage Other Charges to Students', pageSubTitle: ''},
                })
                .state('streams', {
                    url: '/streams',
                    templateUrl: "settings/streams",
                    data: {pageTitle: 'Manage Streams', pageSubTitle: ''},
                })
                .state('pay', {
                    url: '/pay',
                    templateUrl: "fees/pay",
                    data: {pageTitle: 'Make Fees Payment', pageSubTitle: ''},
                })
                .state('receipts', {
                    url: '/receipts',
                    templateUrl: "fees/receipts",
                    data: {pageTitle: 'Fees Transactions and Receipts', pageSubTitle: ''},
                })
                .state('feesbalances', {
                    url: '/feesbalances',
                    templateUrl: "fees/feesbalances",
                    data: {pageTitle: 'Fees Balances for Students', pageSubTitle: ''},
                })
                .state('subjects', {
                    url: '/subjects',
                    templateUrl: "exam/subjects",
                    data: {pageTitle: 'Manage Subjects', pageSubTitle: ''},
                })
                .state('examinationtypes', {
                    url: '/examinationtypes',
                    templateUrl: "exam/examinationtypes",
                    data: {pageTitle: 'Manage Examinations', pageSubTitle: ''},
                })
                .state('entermarks', {
                    url: '/entermarks',
                    templateUrl: "exam/entermarks",
                    data: {pageTitle: 'Modify Marks', pageSubTitle: ''},
                })
                .state('major', {
                    url: '/major',
                    templateUrl: "settings/major",
                    data: {pageTitle: 'Make Major Changes', pageSubTitle: ''},
                })
                .state('meritlists', {
                    url: '/meritlists',
                    templateUrl: "exam/meritlists",
                    data: {pageTitle: 'Prepare Merit Lists', pageSubTitle: ''},
                })
                .state('subjectteachers', {
                    url: '/subjectteachers',
                    templateUrl: "exam/subjectteachers",
                    data: {pageTitle: 'Manage Subject Teachers', pageSubTitle: ''},
                })
                .state('houses', {
                    url: '/houses',
                    templateUrl: "settings/houses",
                    data: {pageTitle: 'Manage Houses', pageSubTitle: ''},
                })
                .state('closingopeningdate', {
                    url: '/closingopeningdate',
                    templateUrl: "settings/closingopeningdate",
                    data: {pageTitle: 'Set Closing and Opening Date', pageSubTitle: ''},
                })
                .state('reportcards', {
                    url: '/reportcards',
                    templateUrl: "exam/reportcards",
                    data: {pageTitle: 'Termly Merit Lists and Reports', pageSubTitle: ''},
                })
                .state('expenses', {
                    url: '/expenses',
                    templateUrl: "fees/expenses",
                    data: {pageTitle: 'Manage Expenses', pageSubTitle: ''},
                })
                .state('expensesincome', {
                    url: '/expensesincome',
                    templateUrl: "fees/expensesincome",
                    data: {pageTitle: 'Report for Expenses and Income', pageSubTitle: ''},
                })
                .state('expensetypes', {
                    url: '/expensetypes',
                    templateUrl: "fees/expensetypes",
                    data: {pageTitle: 'Expense Types', pageSubTitle: ''},
                })
                .state('incometypes', {
                    url: '/incometypes',
                    templateUrl: "fees/incometypes",
                    data: {pageTitle: 'Income Types', pageSubTitle: ''},
                })
                .state('income', {
                    url: '/income',
                    templateUrl: "fees/income",
                    data: {pageTitle: 'Manage Income', pageSubTitle: ''},
                })




    }]);

/* Init global settings and run the app */
MuskeyApp.run(["$rootScope", "settings", "$state", function($rootScope, settings, $state) {
        $rootScope.$state = $state; // state to be accessed from view
    }]);